<?php

 if(!defined('BASEPATH')) exit('No direct script access allowed');
 require APPPATH . '/libraries/BaseController.php';
class Admin extends BaseController {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('common_data');
        $this->load->library('dashboard_data');
		$this->load->helper('utility');
        $this->load->model('Admin_model', 'admin');
		$this->load->model('Common_model','common');
		
    }

    public function login(){
    	$this->global['pageTitle'] = 'Bank : Login';
        $this->loadNormal('admin/page-login');
    }
	
	/* Set api tokens View */
	public function set_api_tokens() {
		$data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering
		if(isset($data['map_key'])) {
			$map_key=$data['map_key'];
			$this->admin->updateAPItoken($map_key);
		}
		$data['GMAP_API_KEY'] = $this->map_key;
		$this->global['pageTitle'] = 'KTP : API Tokens';
		$this->loadViews("admin/set_tokens", $this->global, $data, NULL);
	}
	
	/* Set create users View */
	public function set_create_users() {
		$data['1'] = 1;
		$this->global['pageTitle'] = 'KTP : Create Users';
		$this->loadViews("admin/create_users", $this->global, $data, NULL);
	}
	
	/* Set user roles View */
	public function set_user_roles() {
		$data['1'] = 1;
		$this->global['pageTitle'] = 'KTP : User Roles';
		$this->loadViews("admin/set_roles", $this->global, $data, NULL);
	}
	
	/* Set controller scopes View */
	public function set_controller_scopes() {
		$data['1'] = 1;
		$this->global['pageTitle'] = 'KTP : Controller Scopes';
		$this->loadViews("admin/set_scopes", $this->global, $data, NULL);
	}

	/* Set user permissions View */
	public function set_user_permissions() {
		$data['1'] = 1;
		$this->global['pageTitle'] = 'KTP : User Permissions';
		$this->loadViews("admin/set_permission", $this->global, $data, NULL);
	}

	/* Set ROLES names */
	public function set_rolename() {
		$data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering
		$data=$this->admin->set_rolename($data);
		echo json_encode($data);
	}
	/* Get permissions for all users */
	public function get_userRecords() {
		$post=$this->input->post();//Get All Post Data
        $post=html_escape($post);//HTML Escape string
        $post=$this->security->xss_clean($post);//XSS Filtering

        // $post['from_date']='01/01/2019';
        // $post['to_date']='01/01/2020';
        $post['length']=5;
        $post['start']=0;
        $post['search']['value']='';
        $post['draw']=1;

		$paginate_config = array(
            'table'=>array('TBL_USERS',
							array('TBL_ROLES', 'TBL_ROLES.ROLEID = TBL_USERS.ROLEID','left'),
							array('TBL_USER_PERMISSION', 'TBL_USER_PERMISSION.USERID = TBL_USERS.USERID','left')
						),
            'condition'=>array('TBL_USERS.ISDELETED'=> 0,'TBL_USERS.ROLEID !='=> 1),
            'column'=>array('TBL_USERS.USERID', 'TBL_USERS.EMAIL', 'TBL_USERS.NAME', 'TBL_ROLES.ROLE', 'TBL_USER_PERMISSION.EDIT', 'TBL_USER_PERMISSION.SHOW', 'TBL_USER_PERMISSION.REMOVE', 'TBL_USER_PERMISSION.MAKE'),
            'column_search'=>array('ROLE','NAME'),
            'from_date'=>$post['from_date'],
            'to_date'=>$post['to_date'],
            'search_value'=>$post['search']['value'],
            'date_range_column'=>'',
            'column_order'=>array(''),
            'length'=>$post['length'],
            'start'=>$post['start']
        );
        $output=$this->common_data->get_paginate_data($paginate_config);   

        $recordsTotal=$output['recordsTotal'];
        $recordsFiltered=$output['recordsFiltered'];
        $data_list= $output['data'];

          $output = array(
            "draw" => $post['draw'],
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data_list,
			"test" => $post,
		);  
		// pre($this->db->last_query());exit();
        echo json_encode($output);
	}
	
	/* Get permissions by a selected id */
	public function get_userRecords_id() {
		$data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering
		$condition = $data['USERID'];
		$data=$this->common->get_data(
									array('TBL_USERS',
										array('TBL_ROLES', 'TBL_ROLES.ROLEID = TBL_USERS.ROLEID', 'left'),
										array('TBL_USER_PERMISSION', 'TBL_USER_PERMISSION.USERID = TBL_USERS.USERID', 'left')
										),
									array('TBL_USERS.USERID', 'TBL_USERS.NAME', 'TBL_ROLES.ROLE', 'TBL_USER_PERMISSION.EDIT', 'TBL_USER_PERMISSION.SHOW', 'TBL_USER_PERMISSION.REMOVE', 'TBL_USER_PERMISSION.MAKE'),
									array('TBL_USERS.USERID'=>$condition));
		// pre($this->db->last_query());exit();
		echo json_encode($data);
	}
	
	/* Set permissions for a selected id */
	public function set_permission_id() {
		$data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering
		$uid=$data['USERID'];
		unset($data['USERID']);
		$data=$this->admin->set_permission_id($uid, $data);
		
		echo json_encode($data);
	}

	public function get_all_role_controller_scope(){
		$post=$this->input->post();//Get All Post Data
        $post=html_escape($post);//HTML Escape string
        $post=$this->security->xss_clean($post);//XSS Filtering

        // $post['from_date']='01/01/2019';
        // $post['to_date']='01/01/2020';
        $post['length']=5;
        $post['start']=0;
        $post['search']['value']='';
        $post['draw']=1;

		$paginate_config = array(
            'table'=>array('TBL_GROUP_PERMISSION',
							array('TBL_ROLES','TBL_GROUP_PERMISSION.ROLEID=TBL_ROLES.ROLEID','inner')
						),
            'condition'=>"",
            'column'=>array('TBL_GROUP_PERMISSION.ROLEID','TBL_ROLES.ROLE','TBL_GROUP_PERMISSION.CONTROLLER','TBL_GROUP_PERMISSION.METHOD'),
            'column_search'=>array('ROLE','CONTROLLER', 'METHOD'),
            'from_date'=>$post['from_date'],
            'to_date'=>$post['to_date'],
            'search_value'=>$post['search']['value'],
            'date_range_column'=>'TBL_GROUP_PERMISSION.CREATED_ON',
            'column_order'=>array("TBL_GROUP_PERMISSION.CREATED_ON DESC"),
            'length'=>$post['length'],
            'start'=>$post['start']
        );
        $output=$this->common_data->get_paginate_data($paginate_config);   

        $recordsTotal=$output['recordsTotal'];
        $recordsFiltered=$output['recordsFiltered'];
        $data_list= $output['data'];

        //$data=$this->task->process_task_table_data($data_list);

          $output = array(
            "draw" => $post['draw'],
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data_list,
			"test" => $post,
        );  
        echo json_encode($output);
	}

	public function get_all_controller(){
		$controllers = array();
	    $this->load->helper('file');

	    // Scan files in the /application/controllers directory
	    // Set the second param to TRUE or remove it if you 
	    // don't have controllers in sub directories
	    $files = get_dir_file_info(APPPATH.'controllers', FALSE);
	    // Loop through file names removing .php extension
	    foreach ( $files as $key => $file ) {
	    	//pre($file);
	        if ( strpos($file['name'], 'index.html') === false )
	            $controllers[] = str_replace('.php', '', str_replace( '\\\\' , '\\' , str_replace(APPPATH.'controllers\\', '', $file['relative_path'].'\\'.$key)))  ; //str_replace(APPPATH,'', str_replace('.php', '', $file['relative_path']));
	    }
	    echo json_encode($controllers); // Array with all our controllers
	}
	public function get_all_methods(){
		$post=$this->input->post();//Get All Post Data
        $post=html_escape($post);//HTML Escape string
        $post=$this->security->xss_clean($post);//XSS Filtering
        $methods=array();
        if(!empty($post['controller_name'])){
        	$methods=$this->common->get_data('TBL_CONTROLLER_METHODS',array('METHOD_NAME'),array('CONTROLLER_NAME'=>$post['controller_name']),'result_array');
        }
		echo json_encode($methods);
	}

	public function get_roles(){
		$data=$this->common_model->get_data('TBL_ROLES','','','','result_array');
		echo json_encode($data);
	}

	public function save_ControllerMethod(){
		$data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering
		
		$created_date=date('d/m/Y H:i:s');
        $data = array (
        	'ID'=>get_new_id(),
        	'ROLEID'=>$data['ROLEID'],
        	'CONTROLLER'=>$data['CONTROLLER'],
        	'METHOD'=>$data['METHOD'],
			'IS_DELETE'=>'0',
			'CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
			'CREATED_BY'=>$this->vendorId
        	);
			
        $status=$this->admin->save_ControllerMethod($data);
        echo json_encode($status);
	}
	public function delete_ControllerMethod(){
		$post=$this->input->post();//Get All Post Data
        $post=html_escape($post);//HTML Escape string
        $post=$this->security->xss_clean($post);//XSS Filtering
        if(!empty($post['id'])){
        	$status=$this->admin->delete_data('TBL_GROUP_PERMISSION','ID',$post['id']);
        	echo json_encode($status);	
        }
	}
	
}

?>