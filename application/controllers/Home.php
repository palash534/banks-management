<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Home (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.

 */
require APPPATH . '/libraries/BaseController.php';


class Home extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }
/**
     * This is methods are menu links
     */
    public function index(){
        $this->global['pageTitle'] = ' News society : Home';
        $data['news']=$this->Common_model->get_data('admin_news',array('TITLE'),array('IS_DELETE'=>0),'','row_array','',array(
               array('column'=>'NEWS_DATE','mode'=>'DESC')
           ));
        $this->global['news']=!empty($data['news']['TITLE'])?$data['news']['TITLE']:'';
        $this->loadViews("index", $this->global, NULL , NULL);
    }
   public function aboutus(){
        $this->global['pageTitle'] = ' about : about pcics  society';
        $this->loadSubViews("aboutus", $this->global, NULL , NULL);
    }
    public function committe(){
        $this->global['pageTitle'] = ' committee : committee of pcics  society';
        $this->loadSubViews("committe", $this->global, NULL , NULL);
    }
    public function service(){
        $this->global['pageTitle'] = ' service  : service of pcics  society';
        $this->loadSubViews("service", $this->global, NULL , NULL);
    }

    public function products(){
        $this->global['pageTitle'] = ' products : products of pcics  society';
        $this->loadSubViews("products", $this->global, NULL , NULL);
    }
     public function shop(){
        $this->global['pageTitle'] = ' society : shop of pcics  society';
        $this->loadSubViews("shop", $this->global, NULL , NULL);
    }
    public function contact(){
        $this->global['pageTitle'] = ' contact : contact of pcics  society';
        $this->loadSubViews("contact", $this->global, NULL , NULL);
    }
    public function gallery(){
        $this->global['pageTitle'] = ' gallery : contact of pcics  society';
        $this->loadSubViews("gallery", $this->global, NULL , NULL);
    }
    /**
     * This is methods are branch list
     */
    public function branches($id)
    {
        $this->global['pageTitle'] = 'society : branch of pcics society';
         if($id=='9')
         {
            $this->global['pageTitle'] = 'B.C Road - pottery society Puttur';
            $this->global['title'] = 'B.C Road';
            $data['address']='BRANCH: BC ROAD <BR> ACHUTHA COMPLEX,OPP MINI VIDHANA SOWDHA, B MOODA VILLAGE, JODUMARGA  POST,BANTWAL, D.K. 574219';
            $data['img']='bcroad';
            $data['contact']='08255-233274 <br>pcicsbrbc@gmail.com';
            $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d972.354283681893!2d75.0305536!3d12.8808775!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4a6595cdd71f9%3A0xb61958539900c2c7!2sMini%20Vidhana%20Soudha%2C%20Bantwal!5e0!3m2!1sen!2sin!4v1568310811280!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }elseif ($id=='2') {
           $this->global['title'] = 'BELLARE';
           $this->global['pageTitle'] = 'BELLARE - pottery society Puttur';
            $data['address']='BRANCH: BELLARE <BR> SRI RAMA COMPLEX, 1ST FLOOR, KELAGINAPETE,BELLARE VILLAGE AND POST,SULLIA, D.K. 574212';
            $data['img']='bellare';
            $data['contact']='08257-272534 <br>pcicsbrbe@gmail.com';
            $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d973.184165752929!2d75.37089872918085!3d12.665263599441088!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4ed28e1de154d%3A0xcfc9429e2d0e414a!2sHotel%20Shree%20Ram!5e0!3m2!1sen!2sin!4v1568310879284!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }elseif ($id=='3') {
              $this->global['title'] = 'GURUVAYANAKERE';
              $this->global['pageTitle'] = 'GURUVAYANAKERE - pottery society Puttur';
              $data['address']='BRANCH: GURUVAYANAKERE KULALA MANDIRA   GURUVAYANAKERE VILLAGE AND POST BELTHANGADY, D.K. 574217
';
             $data['img']='guruvainkere';   
             $data['contact']='08256-234508 <br>pcicsbrgu@gmail.com';
             $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15551.46729817584!2d75.2433197!3d12.9803686!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4b6d43c9ebc31%3A0x1f9edac5da296a38!2sKulala%20Mandira!5e0!3m2!1sen!2sin!4v1568311171794!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }elseif ($id=='4') {
              $this->global['title'] = 'KAWDICHARU';
               $this->global['pageTitle'] = 'GURUVAYANAKERE - pottery society Puttur';
            $data['address']='BRANCH: KAWDICHARU <BR> ARIYADKA VILLAGE & POST PUTTUR, D.K. 574223
';
            $data['img']='kawdicharu';  
            $data['contact']='08251-283297 <br>pcicsbrka@gmail.com';
            $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d3314.8206453168677!2d75.26053621433161!3d12.672016541052905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m5!1s0x3ba4940f0365e677%3A0x216307de649038f1!2sKaudicharu%2C%20Karnataka%20574223!3m2!1d12.6719723!2d75.2622719!4m3!3m2 !1d12.671572!2d75.26308879999999!5e1!3m2!1sen!2sin!4v1568312056736!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }elseif ($id=='5') {
              $this->global['title'] = 'MANI';
               $this->global['pageTitle'] = 'MANI - pottery society Puttur';
            $data['address']='BRANCH: MANI <BR> NEAR VILLAGE PANCHAYATH MANI VILLAGE AND POST BANTWAL, D.K.574253
';
            $data['img']='mani';  
            $data['contact']='08255-235585 <br>pcicsbrma@gmail.com';
            $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3890.125693010922!2d75.11879931482059!3d12.835153390945504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4a3629559e733%3A0x435f9836ae3938d6!2sPost%20Office%20Mani.!5e0!3m2!1sen!2sin!4v1568312269897!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }elseif ($id=='6') {
             $this->global['title'] = 'UPPINANGADY';
             $this->global['pageTitle'] = 'UPPINANGADY - pottery society Puttur';

            $data['address']='BRANCH: UPPINANGADY <BR> LAXMI ARCADE, 2ND FLOOR, NEAR KARNATAKA BANK, UPPINANGADY VILLAGE AND POST PUTTUR, D.K. 574241
'; 
            $data['img']='uppinangady'; 
            $data['contact']='08251-251274 <br>pcicsbrup@gmail.com';
            $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d439.48402562299844!2d75.25060793692748!3d12.83793058483478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4beabc753bdad%3A0x81338e924d1141d!2sKarnataka%20 society!5e1!3m2!1sen!2sin!4v1568312448177!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }elseif ($id=='7') {
              $this->global['title'] = 'VITTAL';
              $this->global['pageTitle'] = 'VITTAL - pottery society Puttur';
            $data['address']='BRANCH:VITTAL <BR>
            EMPIRE MALL, FIRST FLOOR, NEAR VITTAL BUS STAND, VITTAL VILLAGE AND POST,
                  BANTWAL, D.K. 574243';
             $data['img']='vitla';     
            $data['contact']='08255-238374 <br>pcicsbrvt@gmail.com';
            $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3891.2320671872044!2d75.0986606!3d12.7634357!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4a26144dc0135%3A0x2e6356ee0871dbcb!2sempire%20mall!5e0!3m2!1sen!2sin!4v1568312600934!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }elseif ($id=='8') {
              $this->global['title'] ='KUDTHAMUGERU';
              $this->global['pageTitle'] = 'KUDTHAMUGERU - pottery society Puttur';
            $data['address']='BRANCH: KUDTHAMAGERU <BR> KOLNADU VILLAGE AND KUDTHAMAGERU POST BANTWAL, 
                  D.K. 574323 ';
            $data['img']='kudthamugere';      
            $data['contact']='08255-263026 <br>pcicsbrku@gmail.com';
            $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1945.5280759604798!2d75.04813685803383!3d12.774865397746314!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4a19021abc25b%3A0xf8c76640b069537d!2sVijayashree%20Kalyana%20Mantapa!5e0!3m2!1sen!2sin!4v1568312774476!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }elseif ($id=='1') {
              $this->global['title'] ='PUTTUR';
               $this->global['pageTitle'] = 'KUDTHAMUGERU - pottery society Puttur';
            $data['address']='Head OFFICE: PUTTUR <BR> KULALA SAHAKARI BHAVANA SAINIKA BHAVAN ROAD, opp. BEO OFFICE, Puttur, Karnataka 574201 ';
            $data['img']='Puttur';      
            $data['contact']=' 08251-236274 / 08251-233274  <br> pcicsputtur@gmail.com';
            $data['link']='<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15565.189925039971!2d75.200116!3d12.759183!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe69abe4267e9666!2sPOTTERS%20COTTAGE%20INDUSTRIAL%20CO%20OPERATIVE%20SOCIETY%20LTD%20PUTTUR!5e0!3m2!1sen!2sin!4v1568458960668!5m2!1sen!2sin" width="580" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
         }
         $this->loadSubViews("branch", $this->global, $data , NULL);
     }

     /**
     * accounts methods
     */

     public function saving(){
        $this->global['pageTitle'] = 'SAVINGS ACCOUNT  ';
         $this->global['img'] = '10';
        $this->loadServiceViews("saving", $this->global, NULL , NULL);
    }
    public function currentAc(){
        $this->global['pageTitle'] = 'CURRENT ACCOUNT';
        $this->global['img'] = '1';
        $this->loadServiceViews("currentAc", $this->global, NULL , NULL);
    }
    public function fixedAc(){
        $this->global['pageTitle'] = 'FIXED DEPOSIT';
        $this->global['img'] = '2';
        $this->loadServiceViews("fixedAc", $this->global, NULL , NULL);
    }
    public function reccuringAc(){
        $this->global['pageTitle'] = 'RECCURING DEPOSIT ';
        $this->global['img'] = '3';
        $this->loadServiceViews("reccuringAc", $this->global, NULL , NULL);
    }
    public function compulsaryAc(){
        $this->global['pageTitle'] = 'COMPULSARY DEPOSIT';
        $this->global['img'] = '5';
        $this->loadServiceViews("compulsaryAc", $this->global, NULL , NULL);
    }
    public function dailyAc(){
        $this->global['pageTitle'] = 'DAILY DEPOSITES';
        $this->global['img'] = '5';
        $this->loadServiceViews("dailyAc", $this->global, NULL , NULL);
    }

    public function loan($id){

         
         if($id=='1')
         {
            $this->global['pageTitle'] = 'SURITY LOAN ';
             $this->global['address']='';
             $this->global['details']='<b>Interest rate - 16%</b><BR>
                                         For more details visit your nearest branch ';
             $this->global['image']=$id;

         }elseif ($id=='2') {
           $this->global['pageTitle'] = 'JEWEL LOAN';
             $this->global['address']='';
             $this->global['details']='<b>Interest of Jewel loan 13%<b>
                                       <BR>
                                       For more details visit your nearest branch';
             $this->global['image']=$id;

         }elseif ($id=='3') {
              $this->global['pageTitle'] = 'DEPOSIT LOAN ';
             $this->global['address']='';
             $this->global['details']='Deposit loan no longer much  concerning the deposits may also lie issued towards the safety over deposits in imitation of the depositors. stability Interest concerning certain loans shall be charged at certain rates determined by way of the Board beyond age in accordance with time.<br>
                <ul> <li> • PIGMY LOANS - 10% </li><li>• FIXED DEOISIT -  +2% OF THE RATE OF INTEREST ON DEPOSIT </li>
             
              ';
             $this->global['image']=$id;

         }elseif ($id=='4') {
             $this->global['pageTitle'] = 'CASH CREDIT LOAN';
             $this->global['address']='';
             $this->global['details']=' A Cash Credit (CC) is a short-term source of financing for a company. In other words, a cash credit is a short-term loan extended to a company by a society. It enables a company to withdraw money from a bank account without keeping a credit balance. The account is limited to only borrowing up to the borrowing limit. Also, interest is charged on the amount borrowed and not the borrowing limit.</li> 
More details contact our nearest branch.';
             $this->global['image']='5';

         }elseif ($id=='5') {
              $this->global['pageTitle'] = 'HOME LOAN ';
            $this->global['address']='';
             $this->global['details']='<ul><li>1. Acquisition over a residence then plane and apartment,new and old .</li>
                  <li>  2. Acquisition about a web page and plot yet building concerning a house then flat and condo thereon.</li>
                   <li> 3. Construction concerning a house over a web site yet machinate over coast in the meantime acquired.</li>
                  <li>  4. Extension or reparation over the residence then plane yet apartment already acquired.</li>
                  <li>     5. Acquisition concerning an historical residence and reparation / extension thereof then winning concerning an ancient house, its kill then reconstruction. </li>
                  <li>6. inte rate : 12% </li></ul>';
             $this->global['image']=$id;

         }elseif ($id=='6') {
            $this->global['pageTitle'] = 'LAND PURCHASE LOAN & LAND MORTGAGE LOAN ';
            $this->global['address']='';
            $this->global['details']='One of the financial instruments that society provide are land loan or plot loan. It is a loan provided by society to borrowers for the purchase of a plot of land. <ul><li>• Land purchase loan - 14.90%</li><li>• Land Mortgage loan - 14.90%</li>';
            $this->global['image']=$id;

         }elseif ($id=='7') {
            $this->global['pageTitle'] = 'VEHICLE LOAN ';
            $this->global['address']='';
            $this->global['details']='Purchase of new vehicle / second hand vehicle <br>
                                      <h4>Intrest rate</h4>
                                       <ul><li> a. New vehicle purchase - 13%</li>
                                           <li> b. Second hand vehicle - 16% </li>';
            $this->global['image']=$id;

         }elseif ($id=='8') {
              $this->global['pageTitle'] = 'MEMBERS CHILDREN EDUCATION LOANS ';
            $this->global['address']='';
            $this->global['details']='<br><b>Interest of Jewel loan 4%</br> ';
            $this->global['image']=$id;

         }elseif ($id=='9') {
              $this->global['pageTitle'] = 'SELF HELP GROUP LOANS ';
            $this->global['address']='';
            $this->global['details']='<b>Self help group loan Interest 9.5%</b>';
            $this->global['image']=$id;

         }
       
         $this->loadLoanViews("loan", $this->global,NULL, NULL);

    }


}

?>