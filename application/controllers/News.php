<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Home (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.

 */
require APPPATH . '/libraries/BaseController.php';


class News extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('utility');
        $this->load->model('Common_model','common');
        $this->load->helper('validation_helper');
        $this->load->model('News_model');
        $this->load->library('pagination');
        $this->load->library('encryption');

    }

    public function index(){
        $this->isLoggedIn();
        $this->global['pageTitle'] = 'Bank : News : All News';
        $data['data']=$this->common_model->get_data('admin_news','',array('IS_DELETE'=>0),'','result_array');
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]['ID'] = base64_encode($this->encryption->encrypt($data['data'][$key]['ID']));
        }
        $this->loadAdminViews("news/show_news", $this->global, $data , NULL);
    }
    public function create(){
        $this->isLoggedIn();
        $this->global['pageTitle'] = 'Bank : News : Create News';
        $this->loadAdminViews("news/create_news", $this->global, NULL , NULL);
    }
    public function saveNews(){
        $this->isLoggedIn();
        $data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering


        $image=$this->utility->upload_files('image',BASEPATH . '../upload/',$_FILES['image']);
        if($image===FALSE){
             $response=array(
                'status'=>'error',
                'error_desc'=>$this->upload->display_errors()
            );
            $this->session->set_flashdata('flsh_msg', $response);
            redirect('news/create');
        }
       $processed_news = array(
            'TITLE'=>!empty($data['title']) ? $data['title'] : null,
            'CONTENT'=>!empty($data['content']) ? $data['content'] : null,
            'NEWS_DATE'=>!empty($data['news_date']) ? date('Y-m-d', strtotime($data['news_date'])) : null,
            'IMAGE_PATH'=>$image['config'][0]['caption'],
            'CREATED_ON'=>date('Y-m-d'),
            //'UPDATED_ON'=>date('Y-m-d'),
            'CREATED_BY'=>$this->vendorId,
            //'UPDATED_BY'=>$this->vendorId,
            'IS_DELETE'=>0
        );
        $news_validation = news_validation_config();
        $this->form_validation->reset_validation();
        $this->form_validation->set_data($processed_news);
        $this->form_validation->set_rules($news_validation);
        if($this->form_validation->run()==FALSE){
            $response=array(
                'status'=>'error',
                'type'=>'News',
                'error_desc'=>$this->form_validation->error_array()
            );
            $this->session->set_flashdata('flsh_msg', $response);
            redirect('news/create');
        }
        $status=$this->common->save_data('admin_news',$processed_news);
        if($status=='1'){
            $response=array(
                'status'=>'success',
                'success_msg'=>'Your News has been successfully saved.'
            );
        }
        $this->session->set_flashdata('flsh_msg', $response);
        redirect('news/create');
    }
    public function public_news(){
        $this->global['pageTitle'] = 'Pottery society: News : All News';
        $data['news']=$this->common->get_data('admin_news',array('TITLE'),array('IS_DELETE'=>0),'','row_array','',array(
               array('column'=>'NEWS_DATE','mode'=>'DESC')
           ));
        $this->global['news']=!empty($data['news']['TITLE'])?$data['news']['TITLE']:'';
        

        $config = array();
        $config["base_url"] = base_url() . "news/public_news";
        $total_row = $this->News_model->record_count();
        $config["total_rows"] = $total_row;
        $config["per_page"] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="fa fa-long-arrow-left"></span>';
        $config['cur_tag_open'] = '<li><a href="#" class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li class="prev">';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = '<span class="fa fa-long-arrow-right"></span>';
        $config['num_tag_open'] = '<li>' ;
        $config['num_tag_close'] = '</li>';
        

        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["results"] = $this->News_model->fetch_data($config["per_page"], $page);
        foreach ($data['results'] as $key => $value) {
            $data['results'][$key]['ID'] = base64_encode($this->encryption->encrypt($data['results'][$key]['ID']));
        }
        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;',$str_links );
        $this->loadSubViews("news/public_news", $this->global, $data , NULL);
    }
    public function edit(){
        $this->isLoggedIn();
        $this->global['pageTitle'] = 'Bank : News : Edit News';
        if($this->uri->segment(3)){
            $id = ($this->uri->segment(3)) ;
            $ID = $this->encryption->decrypt(base64_decode($id));
            $data['data']=$this->common_model->get_data('admin_news','',array('IS_DELETE'=>0,'ID'=>$ID),'','row_array');
            $data['data']['ID'] = base64_encode($this->encryption->encrypt($data['data']['ID']));
            $this->loadAdminViews("news/edit_news", $this->global, $data , NULL);
        }
        else redirect('/news');
    }
    public function updateNews(){
        $this->isLoggedIn();
        $data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering
        $ID = $this->encryption->decrypt(base64_decode($data['id']));
        
        $image='';
        if(!empty($_FILES['image']['name'][0])){
            $image=$this->utility->upload_files('image',BASEPATH . '../upload/',$_FILES['image']);
            if($image===FALSE){
                 $response=array(
                    'status'=>'error',
                    'error_desc'=>$this->upload->display_errors()
                );
                $this->session->set_flashdata('flsh_msg', $response);
                redirect('news/create');
            }
        }
       $processed_news = array(
            'TITLE'=>!empty($data['title']) ? $data['title'] : null,
            'CONTENT'=>!empty($data['content']) ? $data['content'] : null,
            'NEWS_DATE'=>!empty($data['news_date']) ? date('Y-m-d', strtotime($data['news_date'])) : null,
            'IMAGE_PATH'=>!empty($image['config'][0]['caption'])? $image['config'][0]['caption'] : null,
            //'CREATED_ON'=>date('Y-m-d'),
            'UPDATED_ON'=>date('Y-m-d'),
            //'CREATED_BY'=>$this->vendorId,
            'UPDATED_BY'=>$this->vendorId,
            //'IS_DELETE'=>0
        );
        if(empty($image)){
            unset($processed_news['IMAGE_PATH']);
        }

        $news_validation = news_validation_config();
        $this->form_validation->reset_validation();
        $this->form_validation->set_data($processed_news);
        $this->form_validation->set_rules($news_validation);
        if($this->form_validation->run()==FALSE){
            $response=array(
                'status'=>'error',
                'type'=>'News',
                'error_desc'=>$this->form_validation->error_array()
            );
            $this->session->set_flashdata('flsh_msg', $response);
            redirect('news/edit/'.$data['id']);
        }

    
        $status=$this->News_model->update_data($processed_news,$ID);
        $response=array();
        if($status=='1'){
            $response=array(
                'status'=>'success',
                'success_msg'=>'Your News has been successfully updated.'
            );
        }
        $this->session->set_flashdata('flsh_msg', $response);
        redirect('news/edit/'.$data['id']);
    }
    public function deleteNews(){
        $this->isLoggedIn();
        $data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering
        $ID = $this->encryption->decrypt(base64_decode($data['id']));
        
        
        $status=$this->common->delete_data('admin_news','IS_DELETE','ID',$ID,$this->vendorId);
        $response=array();
        if($status=='1'){
            $response=array(
                'status'=>'success',
                'success_msg'=>'Your News has been successfully deleted.'
            );
        }
        $this->session->set_flashdata('flsh_msg', $response);
        redirect('news');
    }

    public function detail_news(){
        $this->global['pageTitle'] = 'Pottery society: News : Detail News';
        if($this->uri->segment(3)){
            $id = ($this->uri->segment(3)) ;
            $ID = $this->encryption->decrypt(base64_decode($id));
            $data['data']=$this->common_model->get_data('admin_news','',array('IS_DELETE'=>0,'ID'=>$ID),'','row_array');
            $data['data']['ID'] = base64_encode($this->encryption->encrypt($data['data']['ID']));
            $this->loadSubViews("news/detail_news", $this->global, $data , NULL);
        }
        else redirect('/news');
        

    }
}

?>