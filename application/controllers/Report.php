<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Report extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/month_of_year", $this->global, NULL , NULL);
    }
    public function month_of_year(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/month_of_year", $this->global, NULL , NULL);
    }
    public function time_of_day(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/time_of_day", $this->global, NULL , NULL);
    }
    public function weather(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/weather", $this->global, NULL , NULL);
    }
    public function road_classification(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/road_classification", $this->global, NULL , NULL);
    }
    public function road_envrironment(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/road_environment", $this->global, NULL , NULL);
    }
    public function road_feature(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/road_feature", $this->global, NULL , NULL);
    }
    public function junction(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/junction_type", $this->global, NULL , NULL);
    }
    public function traffic_control_junction(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/traffic_control_junction", $this->global, NULL , NULL);
    }
    public function impacting_vehicle(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/impacting_vehicle", $this->global, NULL , NULL);
    }
    public function age_impacting_vehicle(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/age_impacting_vehicle", $this->global, NULL , NULL);
    }
    public function load_condition(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/load_condition", $this->global, NULL , NULL);
    }
    public function collision(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/collision", $this->global, NULL , NULL);
    }
    public function traffic_violation(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/traffic_violation", $this->global, NULL , NULL);
    }
    public function safety(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/safety", $this->global, NULL , NULL);
    }
    public function license(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/license", $this->global, NULL , NULL);
    }
    public function victim(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/victim", $this->global, NULL , NULL);
    }
    public function road_user(){
        $this->global['pageTitle'] = 'KTP : TIAMS Criteria Wise Month of Year';
        
        $this->loadViews("report/criteria/road_user", $this->global, NULL , NULL);
    }
    
}

?>