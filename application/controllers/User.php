<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.

 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->library('common_data');
        $this->load->model('User_model','user_model');
        $this->load->model('Common_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = ' PCICS:  Dashboard';
        $data['news']=$this->Common_model->get_data('admin_news','',array('IS_DELETE'=>0),'','num_rows');
        $this->loadAdminViews("admin/dashboard", $this->global, $data , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    function userListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
		// FOR CI PAGINATION
			$searchText = $this->input->post('searchText');
        /*
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->user_model->userListingCount($searchText);
			$returns = $this->paginationCompress ( "userListing/", $count, 10 );
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);
		*/
            $data['userRecords'] = $this->user_model->userListing($searchText);
            $this->global['pageTitle'] = 'PCICS :  User Listing';
            
			// $this->loadViews("users", $this->global, $data, NULL);
			$this->loadViews("admin/set_roles", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['ROLES'] = $this->user_model->getUserRoles();
            $this->global['pageTitle'] = 'PCICS :  Add New User';
            $this->loadViews("admin/create_users", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $name = ucwords(strtolower($this->input->post('fname')));
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->input->post('mobile');
				$tg = $this->input->post('tg_name');
				$ps = $this->input->post('ps_name');
				$ref_token = "";
                
                $date = date('y-m-d h:i:s');
                $userInfo = array('EMAIL'=>$email, 'PASSWORD'=>getHashedPassword($password), 'ROLEID'=>$roleId, 'NAME'=> $name,
                                    'MOBILE'=>$mobile, 'ISDELETED'=>'0', 'CREATEDBY'=>$this->vendorId, 'CREATEDDTM'=>$date, 'TG_CD'=>$tg, 'PS_CD'=>$ps, 'REFRESH_TOKEN'=>"");
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User Created Successfully & User Permission Assigned to "NULL"');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User Creation Failed');
                }
                
                redirect('addNew');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            $data['ROLES'] = $this->user_model->getUserRoles();
            $data['USERINFO'] = $this->user_model->getUserInfo($userId);
            $this->global['pageTitle'] = 'PCICS :  Edit User';
            $this->loadViews("admin/edit_users", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $name = ucwords(strtolower($this->input->post('fname')));
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->input->post('mobile');
				$tg = $this->input->post('tg_name');
				$ps = $this->input->post('ps_name');
                
                $userInfo = array();
                
                $date = date('y-m-d h:i:s');
                if(empty($password))
                {
                    $userInfo = array('EMAIL'=>$email, 'ROLEID'=>$roleId, 'NAME'=>$name,
                                    'MOBILE'=>$mobile, 'UPDATEDBY'=>$this->vendorId, 'UPDATEDDTM'=>$date, 'TG_CD'=>$tg, 'PS_CD'=>$ps);
                }
                else
                {
                    $userInfo = array('EMAIL'=>$email, 'PASSWORD'=>getHashedPassword($password), 'ROLEID'=>$roleId,
                        'NAME'=>ucwords($name), 'MOBILE'=>$mobile, 'UPDATEDBY'=>$this->vendorId, 
                        'UPDATEDDTM'=>$date, 'TG_CD'=>$tg, 'PS_CD'=>$ps);
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
				
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User Updated Successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User Updation Failed');
                }
				
				redirect('editOld/'.$userId);
                // redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
                $date = date('y-m-d h:i:s');
            $userId = $this->input->post('userId');
            $userInfo = array('ISDELETED'=>1,'UPDATEDBY'=>$this->vendorId, 'UPDATEDDTM'=>$date);
            
            $result = $this->user_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        $this->global['pageTitle'] = 'PCICS :  Change Password';
        
        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }
    
    
    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->loadChangePass();
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            }
            else
            {
                $date = date('y-m-d h:i:s');
                $usersData = array('PASSWORD'=>getHashedPassword($newPassword), 'UPDATEDBY'=>$this->vendorId,
                                'UPDATEDDTM'=>$date);
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('loadChangePass');
            }
        }
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'PCICS :  404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>