<?php

 if(!defined('BASEPATH')) exit('No direct script access allowed');
 require APPPATH . '/libraries/BaseController.php';
class insert_Data extends BaseController {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('common_data');
        $this->load->library('task');
		$this->load->helper('utility');
		$this->load->model('user_model');
		$this->isLoggedIn(); 
    }
	
	public function index()
    {
        if(!$this->isAllowedOperation('REMOVE')) redirect('/');
        $data['ROLES'] = $this->user_model->getUserRoles();
		$data['GMAP_API_KEY'] = $this->map_key;
        $this->global['pageTitle'] = 'KTP : TIAMS New Report';
        $this->loadViews("user/new_Record", $this->global, $data, NULL);
    }
	
	
	public function deleteImage() {
		 // echo "<pre>" . print_r($_POST);
		 // die;
        $data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering

        if(isset($data['image_id'])){
            if(!empty($data['image_id'])){
                $status=$this->task->delete_image($data['image_id'],$this->vendorId);
                if($status>0){
                    echo json_encode(['success'=> 'Deleted successfully !']);            
                }
            }
            else{
                echo json_encode(['error'=> 'Action failed !']);        
            }
        }
        else{
            echo json_encode(['error'=> 'Action failed !']);        
        }
//		echo json_encode(['error'=> 'Action failed !']);
	}
	
/////////			DO NOT DELETE THIS FUNCTION. IT IS NECESSARY FOR SHOW RECORDS.
	public function loadSavedImage() {
		$data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering

        $master_record_id=!empty($data['master_record_id']) ? $data['master_record_id'] : null;
        if($master_record_id==null){
            echo json_encode(array('error' => 'master_record_id is wrong'));
            return;
        }
        $imageData=$this->task->loadSavedImage($master_record_id);
        $preview = $imageData['preview'];
        $config = $imageData['config'];
        $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
		echo json_encode( $out);
	}
        
    /*
    input:FILES
    output:array(
                'initialPreview':image array
                initialPreviewConfig:config array
            );
    */
	
    public function uploadImage(){
        //Image Upload
	//	pre($this->input->post()); pre($_FILES['fileToUpload']); exit();
        $this->load->library('upload');
        $this->load->library('utility');
        $image=$this->utility->upload_files('',BASEPATH . '../assets/upload/',$_FILES['fileToUpload']);
        if($image===FALSE){
             $response=array(
                'error'=>$this->upload->display_errors()
            );
            //$response['error']=$this->upload->display_errors();
            echo json_encode($response);
        }
        else{
            $master_record_id = html_escape($this->security->xss_clean($this->input->post('master_record_id')));
            $data = array(
                'master_record_id' => $master_record_id,
                'Image' => $image['filename']
            );
            $status=$this->task->save_image($data,$this->vendorId);
            if(isset($status['status'])){
                if($status['status']==='error'){
                    echo json_encode($status);
                    return;
                }
            }
            $response=array(
                'initialPreview'=>$image['filename'],
                'initialPreviewConfig' => $image['config'],
                'initialPreviewAsData' => true
            );
            echo json_encode($response);
        }
        //End Image Upload
    }

    public function new_task(){
        $data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering

        //Process Image Tag
        isset($data['FilePaths'])? $FilePaths=$data['FilePaths']:$FilePaths="";
        if($FilePaths!==""){
            $image_files=explode(',', $FilePaths);    
            $image_array=array();
            foreach ($image_files as $key => $value) {
                $item= array('path' => $value );
                array_push($image_array, $item);
            }
            $data['Image']=$image_array;
        }
        //End Process Image Tag
        //$data['acc_time']="02:00";
		//$data['Inj']['0']['inj_sex']="M";
        //$data['Kill']['0']['kill_sex']="M";
        //Save New Task
        $output=$this->task->save_new_task($data,$this->vendorId);
        echo json_encode($output);
    }
	
	public function get_traffic_guard() {
        $data= $this->common_data->get_traffic_guard($this->tg);
		echo json_encode($data);
	}
	
	public function get_ps() {
		$traffic_guard= empty($this->tg) ? $this->input->post('traffic_guard') : $this->tg;
		$data= '';
		if($traffic_guard != '') {
			$data= $this->common_data->get_all_ps($traffic_guard, $this->ps);
		}
		 echo json_encode($data);
	}
	
	public function get_dropDown() {
		$data= array();
		$tables =$this->common_data->get_drop_down_table_name();
		foreach($tables as $key=>$table){
			$data['#' . str_replace(str_split('/ '),'_',$table->VALUE)]= $this->common_data->get_drop_down($table->VALUE);
		}
		echo json_encode($data);
	}

    private function all_accident($mode){
        $this->load->model('user_model');
        $data['ROLES'] = $this->user_model->getUserRoles();
		$data['mode'] = $mode;	//"P" OR "C"
		$this->global['pageTitle'] = 'KTP : TIAMS List Accident';
		$this->loadViews("user/list_accident", $this->global, $data, NULL);
    }
	public function all_par_accident(){ $this->all_accident("partial"); }
	public function all_com_accident(){ $this->all_accident("complete");}
	
    public function get_all_accident(){
        
        $post=$this->input->post();//Get All Post Data
        $post=html_escape($post);//HTML Escape string
        $post=$this->security->xss_clean($post);//XSS Filtering

        //Get All task
		$condition=get_session_role_condition($this->role,$this->tg,$this->ps,'0');	// '0'=> Non-delete RECORDS, '1'=> Deleted RECORDS, ''=> Super Admin(KPT Developers)
		if(!empty($post['dt_mode'])){
			if($post['dt_mode']==='partial'){
				$condition['STATUS']='1';
			}
			else{
				$condition['STATUS']='2';
			}
		}
        $paginate_config = array(
//            'draw'=>'1',
            'table'=>array('ACC_REP',
							array('M_PS_CODE','M_PS_CODE.PS_CD=ACC_REP.PS_CD','inner'),
							array('GUARD_DETAIL','GUARD_DETAIL.CODE=ACC_REP.GRD_OFF_CD','inner'),
							array('DROP_DOWN','DROP_DOWN.ID=ACC_REP.NATURE_OF_ACCIDENT','inner')
						),
            'condition'=>$condition,
            'column'=>array("ACC_REP.MASTER_RECORD_ID","FIR_NO","TO_CHAR(FIR_DATE,'DD/MM/YYYY') AS FIR_DATE","GD_NO","TO_CHAR(GD_DATE,'DD/MM/YYYY') AS GD_DATE","TO_CHAR(ACC_DATE,'DD/MM/YYYY') AS ACC_DATE","ACC_TIME","GUARD_DETAIL.GUARD_NAME","M_PS_CODE.PS_NAME","ACC_PLACE","DROP_DOWN.VALUE NATURE_OF_ACCIDENT"),
            'column_search'=>array('FIR_NO','ACC_PLACE', 'M_PS_CODE.PS_NAME','GUARD_DETAIL.GUARD_NAME','DROP_DOWN.VALUE'),
            'from_date'=>$post['from_date'],
            'to_date'=>$post['to_date'],
            'search_value'=>$post['search']['value'],
            'date_range_column'=>'ACC_REP.CREATED_ON',
            'column_order'=>array("ACC_REP.CREATED_ON DESC"),
            'length'=>$post['length'],
            'start'=>$post['start']   
        );

		//pre($paginate_config); exit();

        $output=$this->common_data->get_paginate_data($paginate_config);
		// pre($this->db->last_query());

        $recordsTotal=$output['recordsTotal'];
        $recordsFiltered=$output['recordsFiltered'];
        $data_list= $output['data'];

        $data=$this->task->process_task_table_data($data_list);

          $output = array(
            "draw" => $post['draw'],
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data,
			"test" => $post,
        );  
        echo json_encode($output);
    }

    public function get_specific_accident(){
        $accident_id=$this->input->post('accident_id');
        $accident_id=html_escape($accident_id);//HTML Escape string
        $accident_id=$this->security->xss_clean($accident_id);//XSS Filtering
		
		$data=array();
        if(isset($accident_id)){
            $data=$this->task->get_specific_accident($accident_id,$this->vendorId);
            $data=$this->task->process_specific_accident_data($data);
        }
//		pre($data);
        echo json_encode($data);
    }
	public function delete_accident(){
        $accident_id=$this->input->post('accident_id');
        $accident_id=html_escape($accident_id);//HTML Escape string
        $accident_id=$this->security->xss_clean($accident_id);//XSS Filtering

        if(isset($accident_id)){
            $data=$this->task->delete_accident($accident_id,$this->vendorId);
            echo json_encode($data);
        }
    }
    public function assign_IO(){
        $post=$this->input->post();//Get All Post Data
        $post=html_escape($post);//HTML Escape string
        $post=$this->security->xss_clean($post);//XSS Filtering
		$data=array();
        if(!empty($post['accident_id']) && !empty($post['io'])){
            $data['status']=$this->task->set_IO($post['accident_id'],$post['io'],$this->vendorId);	//$this->vendorId
//			pre($data);
        }
		echo json_encode($data);
	}
	
	public function get_IO_List() {
		$masterFileID = $this->input->post('caseId');
		$data["file_info"]=$this->task->get_fir_no($masterFileID);
		$data["io_list"]= $this->common_data->get_all_io($this->tg);
		echo json_encode($data);
//		echo json_encode (password_hash('123456', PASSWORD_DEFAULT));
	}
	
	public function edit() {
		$accident_id=$this->input->post('accident_id');
        $accident_id=html_escape($accident_id);//HTML Escape string
        $accident_id=$this->security->xss_clean($accident_id);//XSS Filtering

        if(!empty($accident_id)){
			$data['accident_id']=$accident_id;
				
			$data['ROLES'] = $this->user_model->getUserRoles();
			$this->global['pageTitle'] = 'KTP : TIAMS Edit Report';
			// pre($data);
			// exit();
			$this->loadViews("user/edit_record", $this->global, $data, NULL);
        }
	}
	
    public function update(){
        $data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering

        //Process Image Tag
        isset($data['FilePaths'])? $FilePaths=$data['FilePaths']:$FilePaths="";
        if($FilePaths!==""){
            $image_files=explode(',', $FilePaths);    
            $image_array=array();
            foreach ($image_files as $key => $value) {
                $item= array('path' => $value );
                array_push($image_array, $item);
            }
            $data['Image']=$image_array;
        }
        //End Process Image Tag
        //$data['acc_time']="02:00";
        //$data['Inj']['0']['inj_sex']="M";
        //$data['Kill']['0']['kill_sex']="M";
        //Save New Task
        $output=$this->task->update_task($data,$this->vendorId);
        echo json_encode($output);
    }
    public function get_specific_accident_edit(){
		$temp_arr= array();
        $accident_id=$this->input->post('accident_id');
        $accident_id=html_escape($accident_id);//HTML Escape string
        $accident_id=$this->security->xss_clean($accident_id);//XSS Filtering
		
		$data=array();
        if(isset($accident_id)){
            $data=$this->task->get_specific_accident($accident_id,$this->vendorId);
            $temp_arr=$this->task->process_specific_accident_data(array('person_data'=>$data['person_data'], 'driver_vehicle_data'=> $data['driver_vehicle_data']));
			$data['person_data']=$temp_arr['person_data'];
			$data['driver_vehicle_data']=$temp_arr['driver_vehicle_data'];
		}
//		pre($this->encryption->decrypt(base64_decode($accident_id)););
		unset($temp_arr);
        echo json_encode($data);
    }
	public function delete_specific_person() {
		$person_id=''; $master_record_id='';
		$post_Data= $this->input->post();
        $post_Data=html_escape($post_Data);//HTML Escape string
        $post_Data=$this->security->xss_clean($post_Data);//XSS Filtering
		$data= array();
		if(!empty($post_Data['p_id'] && !empty($post_Data['master_record_id']))) {
			$person_id= $this->encryption->decrypt(base64_decode($post_Data['p_id']));
			$master_record_id= $this->encryption->decrypt(base64_decode($post_Data['master_record_id']));
			$data= $this->task->delete_person_with_refresh($person_id,$master_record_id,$this->vendorId);
		}
		$data= $this->task->process_specific_accident_data($data);
		echo json_encode($data);
	}
	public function update_specific_person() {
		$post_Data= $this->input->post();
        $post_Data=html_escape($post_Data);//HTML Escape string
        $post_Data=$this->security->xss_clean($post_Data);//XSS Filtering
		
		$data= array();
		$person=  (!empty($post_Data['Person']) ? $post_Data['Person'] : array() );
		$inj=  (!empty($post_Data['Inj']) ? $post_Data['Inj'] : array() );
		$kill=  (!empty($post_Data['Kill']) ? $post_Data['Kill'] : array() );
		$master_record_id= $this->input->post('master_record_id');

		if(!empty($post_Data['master_record_id'])) {
			$data= $this->task->update_person_with_refresh( $person, $kill, $inj, $master_record_id,
									$this->vendorId );
		}
		$data= $this->task->process_specific_accident_data($data);
//		pre($post_Data); exit();
		echo json_encode($data);
	}
	
	public function get_specific_person(){
		$persons= array();
		$id= $this->input->post('id');
		// pre($this->encryption->decrypt(base64_decode($id)));
		// exit();
		if(!empty($id)) {
			$persons= $this->task->get_person($id);
		}
		echo json_encode($persons);
	}
	
	public function insert_person() {
		$post_Data= $this->input->post();
        $post_Data=html_escape($post_Data);//HTML Escape string
        $post_Data=$this->security->xss_clean($post_Data);//XSS Filtering
		
		$data= array();
		$person=  (!empty($post_Data['Person']) ? $post_Data['Person'] : array() );
		$inj=  (!empty($post_Data['Inj']) ? $post_Data['Inj'] : array() );
		$kill=  (!empty($post_Data['Kill']) ? $post_Data['Kill'] : array() );
		$master_record_id= $this->input->post('master_record_id');

		if(!empty($post_Data['master_record_id'])) {
			 $data= $this->task->insert_person_with_refresh( $person, $kill, $inj, $master_record_id,
									 $this->vendorId );
		}
		$data= $this->task->process_specific_accident_data($data);
//		pre($post_Data); exit();
		echo json_encode($data);
	}
	
	
	public function get_test() {
		$data= $this->input->post();
		pre($data);
		exit();
//		echo json_encode($data);
	}

    public function save_general(){
        $data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering

        //Process Image Tag
        isset($data['FilePaths'])? $FilePaths=$data['FilePaths']:$FilePaths="";
        if($FilePaths!==""){
            $image_files=explode(',', $FilePaths);    
            $image_array=array();
            foreach ($image_files as $key => $value) {
                $item= array('path' => $value );
                array_push($image_array, $item);
            }
            $data['Image']=$image_array;
        }
        //End Process Image Tag
        
        //Save General Data & Image
        $output=$this->task->save_general($data,$this->vendorId);
        echo json_encode($output);
    }
	public function delete_Driver_Vehicle() {		
		$driver_vehicle_data= array();
		$master_record_id= $this->security->xss_clean( html_escape($this->input->post('master_record_id')) );
		$driver_vehicle= $this->security->xss_clean( html_escape($this->input->post('driver_data')) );
		
		if( !empty($master_record_id) && !empty($driver_vehicle) ) {
			$driver_vehicle_data= $this->task->driver_vehicle_operation_with_refresh($driver_vehicle,
										$master_record_id, $this->vendorId, 'delete');
		}
		$driver_vehicle_data= $this->task->process_specific_accident_data($driver_vehicle_data);
		echo json_encode($driver_vehicle_data);
	}
	
	public function insert_Driver_Vehicle() {
        $data=$this->input->post();//Get All Post Data
        $data=html_escape($data);//HTML Escape string
        $data=$this->security->xss_clean($data);//XSS Filtering
		
		$driver_vehicle_data= array();
		if(!empty($data['master_record_id']) && !empty($data['Driver_Vehicle']) ) {
			$driver_vehicle_data= $this->task->driver_vehicle_operation_with_refresh($data['Driver_Vehicle'],
										$data['master_record_id'], $this->vendorId, 'insert');
		}
		$driver_vehicle_data= $this->task->process_specific_accident_data($driver_vehicle_data);
		echo json_encode($driver_vehicle_data);
	}
	
	public function update_Driver_Vehicle() {
		$driver_Vehicle= $this->security->xss_clean( html_escape($this->input->post('Driver_Vehicle')) );
		$master_record_id= $this->security->xss_clean( html_escape($this->input->post('master_record_id')) );
		$driver_vehicle_data= array();
		if(!empty($master_record_id) && !empty($driver_Vehicle) ) {
			$driver_vehicle_data= $this->task->driver_vehicle_operation_with_refresh($driver_Vehicle,
												$master_record_id, $this->vendorId, 'update');
		}
		$driver_vehicle_data= $this->task->process_specific_accident_data($driver_vehicle_data);
		echo json_encode($driver_vehicle_data);
	}
	public function get_Driver_Vehicle() {
		$id= $this->security->xss_clean( html_escape($this->input->post('id')) );
		$master_record_id= $this->security->xss_clean( html_escape($this->input->post('master_record_id')) );
		$driver_vehicle_data= array();
		
		if(!empty($id) && !empty($master_record_id)) {
			$driver_vehicle_data= $this->task->get_driver_vehicle($master_record_id,$id);
		}
		echo json_encode($driver_vehicle_data);
	}
}

?>