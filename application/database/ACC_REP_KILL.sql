--------------------------------------------------------
--  File created - Thursday-July-18-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ACC_REP_KILL
--------------------------------------------------------

  CREATE TABLE "ROOT"."ACC_REP_KILL" 
   (	"PS_CD" VARCHAR2(20 BYTE), 
	"FIR_NO" VARCHAR2(10 BYTE), 
	"FIR_DATE" DATE, 
	"ACC_KIL_SEX" CHAR(1 BYTE), 
	"ACC_KIL_AGE" NUMBER(3,0), 
	"ACC_KIL_RUT_CODE" VARCHAR2(50 BYTE), 
	"ACC_KIL_DOI_CODE" VARCHAR2(50 BYTE), 
	"ACC_KIL_EQ" VARCHAR2(50 BYTE), 
	"ACC_KIL_AFI" VARCHAR2(50 BYTE), 
	"MODI_DT" DATE, 
	"USER_NM" VARCHAR2(20 BYTE), 
	"ACC_KIL_DOI_CODE_TYPE" VARCHAR2(25 BYTE), 
	"RECORDID" NUMBER, 
	"MASTER_RECORD_ID" VARCHAR2(20 BYTE), 
	"ACC_KIL_REMOVED_HOSPITAL" VARCHAR2(100 BYTE), 
	"ACC_KIL_DIED_ON" DATE, 
	"ACC_KIL_NAME" VARCHAR2(70 BYTE), 
	"ACC_KIL_ADDRESS" VARCHAR2(250 BYTE), 
	"ID" VARCHAR2(30 BYTE), 
	"IS_DELETE" VARCHAR2(20 BYTE), 
	"CREATED_ON" TIMESTAMP (6), 
	"UPDATED_ON" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(20 BYTE), 
	"UPDATED_BY" VARCHAR2(20 BYTE), 
	"PARENT_TABLE" VARCHAR2(20 BYTE), 
	"PARENT_TABLE_ID" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.ACC_REP_KILL
SET DEFINE OFF;
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('28','1234',to_date('10-07-19','DD-MM-RR'),'M',78,null,null,null,null,null,null,null,null,'20190713115162','hospital',to_date('09-07-19','DD-MM-RR'),'4545tdffdt','dtdftdftfdt','201907131152460','0',to_timestamp('13-07-19 11:51:55.000000000 AM','DD-MM-RR HH12:MI:SSXFF AM'),null,null,null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('28','1234',to_date('10-07-19','DD-MM-RR'),'M',78,null,null,null,null,null,null,null,null,'20190713115509','hospital',to_date('09-07-19','DD-MM-RR'),'4545tdffdt','dtdftdftfdt','201907131155190','0',to_timestamp('13-07-19 11:54:46.000000000 AM','DD-MM-RR HH12:MI:SSXFF AM'),null,null,null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717171178','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171712010','0',to_timestamp('17-07-19 05:11:44.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717171228','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171712350','0',to_timestamp('17-07-19 05:12:26.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717171875','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171718510','0',to_timestamp('17-07-19 05:18:23.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717170859','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171709000','0',to_timestamp('17-07-19 05:08:36.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717171091','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171711030','0',to_timestamp('17-07-19 05:10:52.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717172732','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171727280','0',to_timestamp('17-07-19 05:27:07.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717173162','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171731620','0',to_timestamp('17-07-19 05:31:31.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717173433','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171734000','0',to_timestamp('17-07-19 05:33:41.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
Insert into ROOT.ACC_REP_KILL (PS_CD,FIR_NO,FIR_DATE,ACC_KIL_SEX,ACC_KIL_AGE,ACC_KIL_RUT_CODE,ACC_KIL_DOI_CODE,ACC_KIL_EQ,ACC_KIL_AFI,MODI_DT,USER_NM,ACC_KIL_DOI_CODE_TYPE,RECORDID,MASTER_RECORD_ID,ACC_KIL_REMOVED_HOSPITAL,ACC_KIL_DIED_ON,ACC_KIL_NAME,ACC_KIL_ADDRESS,ID,IS_DELETE,CREATED_ON,UPDATED_ON,CREATED_BY,UPDATED_BY,PARENT_TABLE,PARENT_TABLE_ID) values ('14',null,null,'M',23,null,null,null,null,null,null,null,null,'20190717175024','hospital',to_date('08-07-19','DD-MM-RR'),'23423fgdfgdfg','fdgfdg df gdfg dfgdfgdf','201907171749690','0',to_timestamp('17-07-19 05:49:46.000000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'1',null,null,null);
--------------------------------------------------------
--  DDL for Index ACC_REP_KILL_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "ROOT"."ACC_REP_KILL_ID" ON "ROOT"."ACC_REP_KILL" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table ACC_REP_KILL
--------------------------------------------------------

  ALTER TABLE "ROOT"."ACC_REP_KILL" ADD CONSTRAINT "ACC_REP_KILL_ID" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
