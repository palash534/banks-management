--------------------------------------------------------
--  File created - Monday-July-29-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table DROP_DOWN
--------------------------------------------------------

  CREATE TABLE "ROOT"."DROP_DOWN" 
   (	"ID" VARCHAR2(20 BYTE), 
	"VALUE" VARCHAR2(120 BYTE), 
	"TYPE" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.DROP_DOWN
SET DEFINE OFF;
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('1','Fatal','1');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('2','Grievous Injured (Need Hospitalization)','1');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('3','Minor Injured (Need Hospitalization)','1');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('4','No Injury','1');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('5','Urban','2');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('6','Non Urban/Rural','2');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('7','Sunny/Clear','3');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('8','Rainy','3');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('9','Foggy/Misty','3');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('10','Hail/Sleet','3');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('11','Others (Specify)','3');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('14','Hit And Run','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('15','Pedestrian','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('16','Hit from Back','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('17','Hit from Side','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('18','Run Off Road','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('19','With Animal','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('20','Fixed/Stationary Object','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('21','With Parked Vehicle','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('22','Vehicle Overturn','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('23','Head on Collision','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('24','Other','5');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('25','2 Lanes or Less','6');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('26','More than 2 Lanes','6');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('27','Paved','7');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('28','Unpaved','7');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('29','Expressways','8');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('30','National Highways','8');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('31','State Highways','8');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('32','Other','8');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('37','Less than 40','11');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('38','40 to 60','11');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('39','60 to 80','11');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('40','Greater than 80','11');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('41','No Speed Sign','11');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('42','Good','12');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('43','Poor','12');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('44','Not Known','12');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('45','Residential Area','13');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('46','Institutional Area','13');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('47','Market/Commercial Area','13');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('48','Open Area','13');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('49','Other','13');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('50','Straight Road','14');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('51','Curved Road','14');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('52','Bridge','14');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('53','Culvert','14');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('54','Pot Holes','14');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('55','Steep Grade','14');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('56','T junction','15');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('57','Y junction','15');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('58','Four Arm Junction','15');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('59','Staggered Junction','15');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('60','Round About Junction','15');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('61','Traffic light Signal','16');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('62','Police Controlled','16');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('63','Stop sign','16');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('64','Flashing signal/blinker','16');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('65','Uncontrolled','16');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('66','No Facility','17');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('67','Footpath','17');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('68','Zebra Crossing','17');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('69','Foot Bridge/Subway','17');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('70','Not Applicable','17');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('71','Valid Permanent License','18');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('72','Learner License','18');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('73','No License','18');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('76','Truck/Lorry','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('77','Heavy Articilated Vehicle/Trolly','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('78','Tempo/Tractor','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('79','Motorised two Wheeler','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('80','Auto Rickshaw','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('81','Car/Jeep/Van/Taxi','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('82','Bus','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('83','Bicycle','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('84','Cycle Rickshaw','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('85','Hand Drawn Cart','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('86','Animal Drawn Cart','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('87','Other','20');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('88','Fatal','21');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('89','Grievous Injury','21');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('90','Minor Injury','21');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('91','Non Injury','21');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('92','Not Known','21');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('93','Over Speeding','22');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('94','Jumping Red Light','22');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('95','Driving on wrong side','22');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('96','Drunken Driving','22');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('97','Use of Mobile Phone','22');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('98','No Violation','22');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('99','Not Known','22');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('100','Seat Belt','23');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('101','Helmet','23');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('102','Not Known (In case of Hit & Run)','23');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('103','Goods','24');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('104','Passenger','24');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('105','Private','24');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('106','Truck/Lorry','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('107','Heavy Articilated Vehicle/Trolly','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('108','Tempo/Tractor','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('109','Motorised two Wheeler','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('110','Auto Rickshaw','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('111','Car/Jeep/Van/Taxi','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('112','Bus','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('113','Bicycle','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('114','Cycle Rickshaw','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('115','Hand Drawn Cart','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('116','Animal Drawn Cart','25');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('117','Normally/Loaded','26');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('118','Overloaded/Hanging','26');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('119','Empty','26');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('120','Not Known','26');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('121','Needs to be Towed','27');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('122','Can be Driven Away','27');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('125','Passenger','29');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('126','Pedestrain','29');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('127','Cyclist','29');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('130','Truck/Lorry','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('131','Heavy Articilated Vehicle/Trolly','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('132','Tempo/Tractor','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('133','Motorised two Wheeler','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('134','Auto Rickshaw','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('135','Car/Jeep/Van/Taxi','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('136','Bus','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('137','Bicycle','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('138','Cycle Rickshaw','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('140','Hand Drawn Cart','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('141','Animal Drawn Cart','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('142','Other','31');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('143','Fatal','32');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('144','Grievous Injury','32');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('145','Minor Injury','32');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('146','Non Injury','32');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('147','Not Known','32');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('148','Seat Belt','33');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('149','Helmet','33');
Insert into ROOT.DROP_DOWN (ID,VALUE,TYPE) values ('150','Not Known (In case of Hit & Run)','33');
--------------------------------------------------------
--  DDL for Index DROP_DOWN_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ROOT"."DROP_DOWN_PK" ON "ROOT"."DROP_DOWN" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table DROP_DOWN
--------------------------------------------------------

  ALTER TABLE "ROOT"."DROP_DOWN" ADD CONSTRAINT "DROP_DOWN_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "ROOT"."DROP_DOWN" MODIFY ("ID" NOT NULL ENABLE);
