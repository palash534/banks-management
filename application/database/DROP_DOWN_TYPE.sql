--------------------------------------------------------
--  File created - Saturday-July-20-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table DROP_DOWN_TYPE
--------------------------------------------------------

  CREATE TABLE "ROOT"."DROP_DOWN_TYPE" 
   (	"ID" VARCHAR2(20 BYTE), 
	"VALUE" VARCHAR2(120 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.DROP_DOWN_TYPE
SET DEFINE OFF;
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('1','Accident Type');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('2','Type of Area');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('3','Type of Weather');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('5','Type of Collision');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('6','Lanes');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('7','Surface Condition');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('8','Road Type');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('11','Speed Limit');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('12','Visibility');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('13','Accident Spot');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('14','Road Features');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('15','Road Junction');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('16','Type of Traffic Control');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('17','Pedestrian Involved');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('18','Type of License');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('20','Impacting Vehicle');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('21','Type of Injury');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('22','Type of Traffic Violation');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('23','Use of Requisite Safety Devices');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('24','Passenger /Goods Vehicle');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('25','Vehicle Type');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('26','Load Condition');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('27','Type of Disposition');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('29','Type of Person');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('31','Person_Impact_Vehicle');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('32','Person_Injury');
Insert into ROOT.DROP_DOWN_TYPE (ID,VALUE) values ('33','Person_Safety_Device');
--------------------------------------------------------
--  DDL for Index DROP_DOWN_TYPE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ROOT"."DROP_DOWN_TYPE_PK" ON "ROOT"."DROP_DOWN_TYPE" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table DROP_DOWN_TYPE
--------------------------------------------------------

  ALTER TABLE "ROOT"."DROP_DOWN_TYPE" ADD CONSTRAINT "DROP_DOWN_TYPE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "ROOT"."DROP_DOWN_TYPE" MODIFY ("ID" NOT NULL ENABLE);
