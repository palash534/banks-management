--------------------------------------------------------
--  File created - Thursday-July-18-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table M_GRD_PS_MAPPING
--------------------------------------------------------

  CREATE TABLE "ROOT"."M_GRD_PS_MAPPING" 
   (	"GRD_OFF_CD" VARCHAR2(5 BYTE), 
	"SEC_CD" VARCHAR2(10 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.M_GRD_PS_MAPPING
SET DEFINE OFF;
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','ADP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','BHL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','BND');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','GRF');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','HDV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','JDV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','KSB');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','MTZ');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','NDL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','NGR');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','NPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','PGM');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','PJV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','PRS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','PTL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','RGP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','RJB');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-A');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-B');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-C');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-D2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-E1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-E2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-F');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-G');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-H');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-I');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-J1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-J2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-K1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-K2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-L1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-L2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-M1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-M2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-N1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-N2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-O1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-O2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-P1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-P2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-P3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-Q1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-Q2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-R1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-R2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-S1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-S2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-T1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-T2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-U1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-U2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-U3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-V1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-V2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-V3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-W');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-X1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-X2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-Y');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SEC-Z');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','SVP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','TKP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','TLJ');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('SRAID','WPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TA','NPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TA','SEC-D1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TA','SEC-E1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TA','SEC-G');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TA','SEC-H');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TB','NPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TB','SEC-D1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TB','SEC-D2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TC','NPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TC','SEC-A');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TC','SEC-M1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TC','SEC-M2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TC','SEC-N1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TC','SEC-N2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TC','SEC-O2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TD','NPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TD','SEC-A');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TD','SEC-B');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TD','SEC-C');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TD','SEC-D1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TD','SEC-D2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TD','SEC-E1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TD','SEC-E2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TE','SEC-E1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TE','SEC-F');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TE','SEC-H');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TE','SEC-I');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TE','SEC-J1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TE','SEC-P3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TE','SEC-Q1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TE','SPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TF','SEC-G');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TF','SEC-J2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TF','SEC-K1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TF','SEC-K2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TF','SEC-L1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TF','SEC-L2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TG','SEC-K1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TG','SEC-K2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TG','SEC-R1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TG','SEC-R2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TG','SEC-S1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TG','SEC-T1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TG','SEC-Y');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TH','SEC-S1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TH','SEC-S2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TH','SEC-T1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TH','SEC-U1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TH','SEC-U2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TH','SEC-U4');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TH','SEC-Y');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TI','SEC-L1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TI','SEC-T1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TI','SEC-T2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TI','SEC-U1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TI','SEC-V1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TJ','SEC-V1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TJ','SEC-V2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TJ','SEC-V3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TJ','SEC-X1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TJ','SEC-X2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TJ','SEC-Z');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TJ','SPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TJ','WPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TK','SEC-L1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TK','SEC-L2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TK','SEC-V1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TK','SEC-W');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TK','SEC-Z');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TK','SPPS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-A');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-C');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-E1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-E2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-F');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-O1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-O2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-P2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TL','SEC-P3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TM','PGM');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TM','SEC-P1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TM','SEC-P2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TM','SEC-P3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TM','SEC-Q1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TM','SEC-Q2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TN','SEC-T2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TN','SEC-U1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TN','SEC-U2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TN','SEC-U3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TN','SEC-V1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TN','SEC-V2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TN','SEC-V3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TO','SEC-J1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TO','SEC-K1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TO','SEC-Q1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TO','SEC-Q2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TO','SEC-R1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TO','SEC-R2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TP','MTZ');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TP','NDL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TP','RJB');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TP','SEC-X1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TQ','BHL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TQ','SEC-V2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TR','BHL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TR','PRS');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TR','SEC-V2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TR','SEC-X2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TR','SNA');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TR','TKP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TS','BND');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TS','HDV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TS','JDV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TS','NGR');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TS','PTL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TS','RGP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TS','SEC-U3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TT','JDV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TT','SEC-U2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TT','SEC-U3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TU','JDV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TU','NGR');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TU','PTL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TV','ADP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TV','GRF');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TV','KSB');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TV','SEC-U2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TV','TLJ');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','ADP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','GRF');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','JDV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','KSB');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','PJV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','PSR');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','PTL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','SEC-U2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','SEC-U3');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TW','SVP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TX','ADP');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TX','KLC');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TX','KSB');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TX','PGM');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TX','SEC-Q2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TX','SEC-R2');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TX','SEC-S1');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TX','TLJ');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TY','BHL');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TY','HDV');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TY','SNA');
Insert into ROOT.M_GRD_PS_MAPPING (GRD_OFF_CD,SEC_CD) values ('TY','TKP');
--------------------------------------------------------
--  DDL for Index M_GRD_PS_MAPPING_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ROOT"."M_GRD_PS_MAPPING_PK" ON "ROOT"."M_GRD_PS_MAPPING" ("GRD_OFF_CD", "SEC_CD") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table M_GRD_PS_MAPPING
--------------------------------------------------------

  ALTER TABLE "ROOT"."M_GRD_PS_MAPPING" ADD CONSTRAINT "M_GRD_PS_MAPPING_PK" PRIMARY KEY ("GRD_OFF_CD", "SEC_CD")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
