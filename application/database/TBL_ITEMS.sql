--------------------------------------------------------
--  File created - Thursday-July-18-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TBL_ITEMS
--------------------------------------------------------

  CREATE TABLE "ROOT"."TBL_ITEMS" 
   (	"ITEMID" NUMBER(10,0), 
	"ITEMHEADER" VARCHAR2(512 CHAR), 
	"ITEMIMAGE" VARCHAR2(80 CHAR), 
	"ISDELETED" NUMBER(10,0), 
	"CREATEDBY" NUMBER(10,0), 
	"CREATEDDTM" TIMESTAMP (6), 
	"UPDATEDDTM" TIMESTAMP (6), 
	"UPDATEDBY" NUMBER(10,0), 
	"ITEMSUB" VARCHAR2(4000 BYTE), 
	"ITEMDESC" VARCHAR2(4000 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.TBL_ITEMS
SET DEFINE OFF;
Insert into ROOT.TBL_ITEMS (ITEMID,ITEMHEADER,ITEMIMAGE,ISDELETED,CREATEDBY,CREATEDDTM,UPDATEDDTM,UPDATEDBY,ITEMSUB,ITEMDESC) values (1,'jquery.validation.js','validation.png',0,1,to_timestamp('02-09-15 12:00:00.000000000 AM','DD-MM-RR HH12:MI:SSXFF AM'),null,null,'Contribution towards jquery.validation.js','jquery.validation.js is the client side javascript validation library authored by J�rn Zaefferer hosted on github for us and we are trying to contribute to it. Working on localization now');
Insert into ROOT.TBL_ITEMS (ITEMID,ITEMHEADER,ITEMIMAGE,ISDELETED,CREATEDBY,CREATEDDTM,UPDATEDDTM,UPDATEDBY,ITEMSUB,ITEMDESC) values (2,'CodeIgniter User Management','cias.png',0,1,to_timestamp('02-09-15 12:00:00.000000000 AM','DD-MM-RR HH12:MI:SSXFF AM'),null,null,'Demo for user management system','This the demo of User Management System (Admin Panel) using CodeIgniter PHP MVC Framework and AdminLTE bootstrap theme. You can download the code from the repository or forked it to contribute. Usage and installation instructions are provided in ReadMe.MD');
--------------------------------------------------------
--  DDL for Trigger TBL_ITEMS_ITEMID_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ROOT"."TBL_ITEMS_ITEMID_TRIG" BEFORE INSERT OR UPDATE ON tbl_items
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.itemId IS NULL THEN
    SELECT  tbl_items_itemId_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN 
      --get the max indentity value from the table
      SELECT NVL(max(itemId),0) INTO v_newVal FROM tbl_items;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT tbl_items_itemId_SEQ.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
   -- assign the value from the sequence to emulate the identity column
   :new.itemId := v_newVal;
  END IF;
END;

/
ALTER TRIGGER "ROOT"."TBL_ITEMS_ITEMID_TRIG" ENABLE;
--------------------------------------------------------
--  Constraints for Table TBL_ITEMS
--------------------------------------------------------

  ALTER TABLE "ROOT"."TBL_ITEMS" MODIFY ("ITEMID" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_ITEMS" MODIFY ("ITEMHEADER" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_ITEMS" MODIFY ("ISDELETED" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_ITEMS" MODIFY ("CREATEDBY" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_ITEMS" MODIFY ("CREATEDDTM" NOT NULL ENABLE);
