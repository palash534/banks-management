--------------------------------------------------------
--  File created - Thursday-July-18-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TBL_RESET_PASSWORD
--------------------------------------------------------

  CREATE TABLE "ROOT"."TBL_RESET_PASSWORD" 
   (	"ID" NUMBER(38,0), 
	"EMAIL" VARCHAR2(128 CHAR), 
	"ACTIVATION_ID" VARCHAR2(32 CHAR), 
	"AGENT" VARCHAR2(512 CHAR), 
	"CLIENT_IP" VARCHAR2(32 CHAR), 
	"ISDELETED" NUMBER(10,0), 
	"CREATEDBY" NUMBER(38,0), 
	"CREATEDDTM" TIMESTAMP (6), 
	"UPDATEDBY" NUMBER(38,0), 
	"UPDATEDDTM" TIMESTAMP (6)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.TBL_RESET_PASSWORD
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Trigger TBL_RESET_PASSWORD_ID_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ROOT"."TBL_RESET_PASSWORD_ID_TRIG" BEFORE INSERT OR UPDATE ON tbl_reset_password
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.id IS NULL THEN
    SELECT  tbl_reset_password_id_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN 
      --get the max indentity value from the table
      SELECT NVL(max(id),0) INTO v_newVal FROM tbl_reset_password;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT tbl_reset_password_id_SEQ.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
   -- assign the value from the sequence to emulate the identity column
   :new.id := v_newVal;
  END IF;
END;

/
ALTER TRIGGER "ROOT"."TBL_RESET_PASSWORD_ID_TRIG" ENABLE;
--------------------------------------------------------
--  Constraints for Table TBL_RESET_PASSWORD
--------------------------------------------------------

  ALTER TABLE "ROOT"."TBL_RESET_PASSWORD" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_RESET_PASSWORD" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_RESET_PASSWORD" MODIFY ("ACTIVATION_ID" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_RESET_PASSWORD" MODIFY ("AGENT" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_RESET_PASSWORD" MODIFY ("CLIENT_IP" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_RESET_PASSWORD" MODIFY ("ISDELETED" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_RESET_PASSWORD" MODIFY ("CREATEDBY" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_RESET_PASSWORD" MODIFY ("CREATEDDTM" NOT NULL ENABLE);
