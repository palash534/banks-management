--------------------------------------------------------
--  File created - Saturday-August-03-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TBL_ROLES
--------------------------------------------------------

  CREATE TABLE "ROOT"."TBL_ROLES" 
   (	"ROLEID" NUMBER(10,0), 
	"ROLE" VARCHAR2(50 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.TBL_ROLES
SET DEFINE OFF;
Insert into ROOT.TBL_ROLES (ROLEID,ROLE) values (1,'Developer');
Insert into ROOT.TBL_ROLES (ROLEID,ROLE) values (2,'KP Traffic HQ');
Insert into ROOT.TBL_ROLES (ROLEID,ROLE) values (3,'Traffic Guard');
Insert into ROOT.TBL_ROLES (ROLEID,ROLE) values (4,'Police Station');
Insert into ROOT.TBL_ROLES (ROLEID,ROLE) values (5,'Police User');
--------------------------------------------------------
--  DDL for Trigger TBL_ROLES_ROLEID_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ROOT"."TBL_ROLES_ROLEID_TRIG" BEFORE INSERT OR UPDATE ON tbl_roles
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.roleId IS NULL THEN
    SELECT  tbl_roles_roleId_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN 
      --get the max indentity value from the table
      SELECT NVL(max(roleId),0) INTO v_newVal FROM tbl_roles;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT tbl_roles_roleId_SEQ.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
   -- assign the value from the sequence to emulate the identity column
   :new.roleId := v_newVal;
  END IF;
END;


/
ALTER TRIGGER "ROOT"."TBL_ROLES_ROLEID_TRIG" ENABLE;
--------------------------------------------------------
--  Constraints for Table TBL_ROLES
--------------------------------------------------------

  ALTER TABLE "ROOT"."TBL_ROLES" MODIFY ("ROLE" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_ROLES" MODIFY ("ROLEID" NOT NULL ENABLE);
