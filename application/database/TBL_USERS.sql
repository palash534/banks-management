--------------------------------------------------------
--  File created - Saturday-August-03-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TBL_USERS
--------------------------------------------------------

  CREATE TABLE "ROOT"."TBL_USERS" 
   (	"USERID" NUMBER(10,0), 
	"EMAIL" VARCHAR2(128 CHAR), 
	"PASSWORD" VARCHAR2(128 CHAR), 
	"NAME" VARCHAR2(128 CHAR), 
	"MOBILE" VARCHAR2(20 CHAR), 
	"ROLEID" NUMBER(10,0), 
	"ISDELETED" NUMBER(10,0), 
	"CREATEDBY" NUMBER(10,0), 
	"UPDATEDBY" NUMBER(10,0), 
	"CREATEDDTM" VARCHAR2(100 BYTE), 
	"UPDATEDDTM" VARCHAR2(100 BYTE), 
	"REFRESH_TOKEN" VARCHAR2(500 BYTE), 
	"REFRESH_TOKEN_TIMESTAMP" TIMESTAMP (6), 
	"PS_CD" VARCHAR2(50 BYTE), 
	"TG_CD" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.TBL_USERS
SET DEFINE OFF;
Insert into ROOT.TBL_USERS (USERID,EMAIL,PASSWORD,NAME,MOBILE,ROLEID,ISDELETED,CREATEDBY,UPDATEDBY,CREATEDDTM,UPDATEDDTM,REFRESH_TOKEN,REFRESH_TOKEN_TIMESTAMP,PS_CD,TG_CD) values (1,'developer@ktp.in','$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq','Developer (BOSS)','9890098900',1,0,0,1,null,'19-06-07 02:13:39','C2D66DB3C8F7925D62A36DD7C87A09FB9A9022AB',to_timestamp('17-07-19 12:32:13.694000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,null);
Insert into ROOT.TBL_USERS (USERID,EMAIL,PASSWORD,NAME,MOBILE,ROLEID,ISDELETED,CREATEDBY,UPDATEDBY,CREATEDDTM,UPDATEDDTM,REFRESH_TOKEN,REFRESH_TOKEN_TIMESTAMP,PS_CD,TG_CD) values (2,'beleghatatg@ktp.in','$2y$10$BW/Hv7h2d6w80D8eftCi8epirwNnC5TPMWkm6ts4qWEjTH2wsGO2.','Beliaghata Traffic Guard','1234567890',3,0,1,1,null,'19-08-02 03:39:38','05E9EC78BC414ED4C8E206DC2E1245795F34B237',to_timestamp('17-07-19 12:32:59.022000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),null,'TM');
Insert into ROOT.TBL_USERS (USERID,EMAIL,PASSWORD,NAME,MOBILE,ROLEID,ISDELETED,CREATEDBY,UPDATEDBY,CREATEDDTM,UPDATEDDTM,REFRESH_TOKEN,REFRESH_TOKEN_TIMESTAMP,PS_CD,TG_CD) values (3,'beleghataps@ktp.in','$2y$10$ezPWESTBrz/R3q4gsKn53el2JmUIbmD8Mdba27TgDKVGYLZWzVYfO','Beleghata Police Station','0123456789',4,0,1,1,null,'19-06-07 02:16:11','6EE9757E7CAD44D0C96F1219F2C53D8910B5CF9B',to_timestamp('17-07-19 12:34:18.558000000 PM','DD-MM-RR HH12:MI:SSXFF AM'),'34','TM');
Insert into ROOT.TBL_USERS (USERID,EMAIL,PASSWORD,NAME,MOBILE,ROLEID,ISDELETED,CREATEDBY,UPDATEDBY,CREATEDDTM,UPDATEDDTM,REFRESH_TOKEN,REFRESH_TOKEN_TIMESTAMP,PS_CD,TG_CD) values (4,'traffichq@ktp.in','$2y$10$ezPWESTBrz/R3q4gsKn53el2JmUIbmD8Mdba27TgDKVGYLZWzVYfO','KP Traffic HQ','0123456789',2,0,0,1,null,null,null,null,null,null);
--------------------------------------------------------
--  DDL for Trigger TBL_USERS_USERID_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ROOT"."TBL_USERS_USERID_TRIG" BEFORE INSERT OR UPDATE ON tbl_users
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.userId IS NULL THEN
    SELECT  tbl_users_userId_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN 
      --get the max indentity value from the table
      SELECT NVL(max(userId),0) INTO v_newVal FROM tbl_users;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT tbl_users_userId_SEQ.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
   -- assign the value from the sequence to emulate the identity column
   :new.userId := v_newVal;
  END IF;
END;


/
ALTER TRIGGER "ROOT"."TBL_USERS_USERID_TRIG" DISABLE;
--------------------------------------------------------
--  Constraints for Table TBL_USERS
--------------------------------------------------------

  ALTER TABLE "ROOT"."TBL_USERS" MODIFY ("CREATEDBY" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_USERS" MODIFY ("ISDELETED" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_USERS" MODIFY ("ROLEID" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_USERS" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_USERS" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "ROOT"."TBL_USERS" MODIFY ("USERID" NOT NULL ENABLE);
