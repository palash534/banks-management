--------------------------------------------------------
--  File created - Wednesday-August-21-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TBL_USER_PERMISSION
--------------------------------------------------------

  CREATE TABLE "ROOT"."TBL_USER_PERMISSION" 
   (	"USERID" VARCHAR2(20 BYTE), 
	"EDIT" VARCHAR2(20 BYTE), 
	"SHOW" VARCHAR2(20 BYTE), 
	"REMOVE" VARCHAR2(20 BYTE), 
	"MAKE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ROOT.TBL_USER_PERMISSION
SET DEFINE OFF;
Insert into ROOT.TBL_USER_PERMISSION (USERID,EDIT,SHOW,REMOVE,MAKE) values ('1','1','1','1','1');
Insert into ROOT.TBL_USER_PERMISSION (USERID,EDIT,SHOW,REMOVE,MAKE) values ('2','1','1','0','1');
Insert into ROOT.TBL_USER_PERMISSION (USERID,EDIT,SHOW,REMOVE,MAKE) values ('3','0','1','1','1');
Insert into ROOT.TBL_USER_PERMISSION (USERID,EDIT,SHOW,REMOVE,MAKE) values ('4','1','1','1','0');
--------------------------------------------------------
--  DDL for Trigger TBL_USER_PERMISSION_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ROOT"."TBL_USER_PERMISSION_TRIG" 
   before insert or update on "ROOT"."TBL_USER_PERMISSION" 
   for each row 
begin  
   if inserting then 
      if :NEW."USERID" is null then 
         select TBL_USERS_USERID_SEQ.currval into :NEW."USERID" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "ROOT"."TBL_USER_PERMISSION_TRIG" ENABLE;
--------------------------------------------------------
--  Constraints for Table TBL_USER_PERMISSION
--------------------------------------------------------

  ALTER TABLE "ROOT"."TBL_USER_PERMISSION" MODIFY ("USERID" NOT NULL ENABLE);
