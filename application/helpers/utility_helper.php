<?php if(!defined('BASEPATH')) exit('No direct script access allowed');




/**
 * This function used to get ID
 */
if(!function_exists('get_new_id'))
{
    function get_new_id(){
       $id=date('YmdHis')+rand(0,99);
        return $id;
    }
}
/**
 * This function change date to dd/mm/yyyy
 */
if(!function_exists('get_specific_date'))
{
    function get_specific_date($input,$mode){
       if($mode==="client"){
            $d = DateTime::createFromFormat('Y-m-d', $input);
            if( $d && $d->format('Y-m-d') === $input){
                return $d->format('d/m/Y');
            }
            else return null; 
       }
       else{
            $d = DateTime::createFromFormat('d/m/Y', $input);
            if( $d && $d->format('d/m/Y') === $input){
                return $d->format('d/m/Y');
            }
            else return null; 
       }
       
    }
}
/**
 * Get USER ID FROM SESSION
 */
if(!function_exists('get_session_user_id'))
{
    function get_session_user_id(){
        $CI =& get_instance();
        return $CI->session->userdata('userId');
    }
}

/**
 * Get USER ID FROM SESSION
 */
if(!function_exists('get_session_role_condition'))
{
    function get_session_role_condition($role,$tg,$ps,$is_delete=''){
		$condition="";
        switch ($role) {
			case ROLE_DEVELOPER: //ADMIN
				$condition=array();
                break;
            case ROLE_TRAFFIC_HQ: //HQ
				$condition=array();
                break;
            case ROLE_MANAGER: //TG
                $condition=array('ACC_REP.GRD_OFF_CD'=>$tg);
                break;
            case ROLE_EMPLOYEE: //PS
                $condition=array('ACC_REP.GRD_OFF_CD'=>$tg,'ACC_REP.PS_CD'=>$ps);
                break;
			case ROLE_INDIVIDUAL: //PU
                $condition=array('ACC_REP.GRD_OFF_CD'=>$tg,'ACC_REP.PS_CD'=>$ps);
                break;
            default:
                $condition=array('ACC_REP.GRD_OFF_CD'=>null,'ACC_REP.PS_CD'=>null);
                break;
        }
		if($is_delete !=='') {
			$condition['IS_DELETE']= $is_delete;
		}
		return $condition;
    }
}
?>