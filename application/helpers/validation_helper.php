<?php if(!defined('BASEPATH')) exit('No direct script access allowed');




/**
 * This function used to get Validation Config of New Task
 */
if(!function_exists('news_validation_config'))
{
    function news_validation_config(){
       //For Validation Config
        $config = [
            [
                    'field' => 'TITLE',
                    'label' => 'TITLE',
                    'rules' => 'required',
                    'errors' => [
                            'required' => 'TITLE is Required',
                    ],
            ],
            [
                    'field' => 'CONTENT',
                    'label' => 'CONTENT',
                    'rules' => 'required',
                    'errors' => [
                            'required' => 'CONTENT is Required',
                    ],
            ],
            /*
            [
                    'field' => 'NEWS_DATE',
                    'label' => 'NEWS DATE',
                    'rules' => 'regex_match[/\d{4}-\d{2}-\d{2}/]',
                    'errors' => [
                            'regex_match' => 'NEWS DATE Not in format',
                    ],
            ]
            */
        ];
        //End Validation Config 
        return $config;
    }
}
?>