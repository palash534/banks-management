<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 
class Authorization
{
    private $CI;

    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->library('encryption');
        $this->CI->load->model('Authorization_model','auth_model');
    }

    public function validateTimestamp($REFRESH_TOKEN,$ACCESS_TOKEN)
    {
        $this->CI =& get_instance();
        $RefreshTokenText=base64_decode($REFRESH_TOKEN);
        $AccessTokenText=base64_decode($ACCESS_TOKEN);
        //Validate RefreshToken and decode AccessToken and return information
        //Check RefreshToken Validation
        
        $RefreshTokenStatus=$this->CI->auth_model->validateRefreshToken($RefreshTokenText);

        if($RefreshTokenStatus){//Valid
            $TokenDetails= (array) JWT::decode($AccessTokenText, $REFRESH_TOKEN);
            $current_timestamp=time();
            $timestamp=$TokenDetails['timestamp'];
            if (($current_timestamp-$timestamp) < ($this->CI->config->item('access_token_timeout') * 60)) {
                return array(
                    'status'=>'success'
                );
            }
            else{
                return array(
                    'status'=>'error',
                    'error_desc'=>'Token expired or mismatched'
                );
            }

        }
        else{//Not Valid
            return array(
                'status'=>'error',
                'error_desc'=>'Refresh Token expired or mismatched'
            );
        }
    }
    
    public function generateToken($data)
    {
        // Generate Token from Username and password
        $data['timestamp']=date("Y-m-d H:i:s");
        $TokenText = JWT::encode($data, $this->CI->config->item('jwt_key'));
        $EncryptedToken=password_hash($TokenText,PASSWORD_BCRYPT);
        // Update table with timestammp
        $this->CI->auth_model->InsertToken($data['login_id'],$EncryptedToken);
        return base64_encode($EncryptedToken);
    }
    public function getRefreshToken($userid)
    {
        //Get refresh token from database. if expired update and return refresh token
        $RefreshToken=$this->CI->auth_model->GetRefreshToken($userid);
        return base64_encode($RefreshToken);
    }
    private function decodeToken($AccessTokenText, $RefreshTokenText){
        $TokenDetailsString = $this->CI->encryption->decrypt(
            $AccessTokenText,
            array(
                    'cipher' => 'AES-128',
                    'mode' => 'cbc',
                    'hmac' => false,
                    'key' => $RefreshTokenText,
            )
        );
        return $TokenDetailsString;
    }
    
}