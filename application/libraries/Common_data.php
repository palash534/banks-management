<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class Common_data  {
	
	private $CI;

	public function __construct(){
		$this->CI =& get_instance();
        $this->CI->load->model('Common_model','common');
        $this->CI->load->model('login_model','login');
	}
    
    public function get_login($email,$password){
        $data=$this->CI->login->loginMe($email,$password);
        if(count($data)>0){
            return $data[0]->USERID;
        }
        else return null;
    }
    public function get_all_traffic_guard(){
        $data=$this->CI->common->get_data('GUARD_DETAIL');
        return $data;
    }
    public function get_traffic_guard($tg){
        if(!empty($tg)){
            $data=$this->CI->common->get_data('GUARD_DETAIL','',array('GUARD_DETAIL.CODE'=>$tg));    
        }
        else{
            $data=$this->CI->common->get_data('GUARD_DETAIL');       
        }
        //pre($this->CI->db->last_query());
        return $data;
    }
    public function get_all_ps($traffic_guard, $ps_cd){
        if($traffic_guard!==""){
            $call_sign_object=$this->CI->common->get_data('M_GRD_PS_MAPPING',array('SEC_CD CALL_SIGN'),array('GRD_OFF_CD'=>$traffic_guard),'result');
            
			$call_sign_array=array();
            foreach($call_sign_object as $key=>$call_sign_item){
                array_push($call_sign_array,$call_sign_item->CALL_SIGN);
            }  
			if(empty($ps_cd)) {
				$data=$this->CI->common->get_data('M_PS_CODE',array('PS_CD','PS_NAME'),'',array('column'=>'CALL_SIGN','value'=>$call_sign_array),'result_array');
			} else {
				$data=$this->CI->common->get_data('M_PS_CODE',array('PS_CD','PS_NAME'),array('PS_CD'=> $ps_cd),array('column'=>'CALL_SIGN','value'=>$call_sign_array),'result_array');
			}
			return $data;    
        }
        else{
           return array(); 
        }
        
    }
	public function get_drop_down_table_name() {
		$table= $this->CI->common->get_data('DROP_DOWN_TYPE',array('ID','VALUE'));
		return $table;
	}
    public function get_drop_down($value){
        if($value!==""){
            $type_id=$this->CI->common->get_data('DROP_DOWN_TYPE','ID',array('VALUE'=>$value));
            $type=array();
            foreach($type_id as $key=>$type_item){
                array_push($type,$type_item->ID);
            }
            if(count($type)>0){
                $drop_down=$this->CI->common->get_data('DROP_DOWN',array('ID','VALUE'),"",array('column'=>'TYPE','value'=>$type));
                return $drop_down;    
            }
            else{
                return array();
            }
        }
        else{
            return array();
        }

    }
    public function get_paginate_data($paginate_config){
		
        
        $column=$paginate_config['column'];
        $table=$paginate_config['table'];
        $condition=$paginate_config['condition'];
		$where_in=isset($paginate_config['where_in'])? $paginate_config['where_in'] : '';
        $search_value=$paginate_config['search_value'];
        $column_search=$paginate_config['column_search'];
        $from_date=$paginate_config['from_date'];
        $to_date=$paginate_config['to_date'];
        $date_range_column=$paginate_config['date_range_column'];
        $column_order=$paginate_config['column_order'];
        $length=$paginate_config['length'];
        $start=$paginate_config['start'];

		$recordsTotal=$this->CI->common->count_all($paginate_config['table'],$paginate_config['condition'],$where_in);
        $recordsFiltered=$this->CI->common->count_filtered($column,$table,$condition,$search_value,$column_search,$from_date,$to_date,$date_range_column,$where_in);

        $data_list= $this->CI->common->get_filtered_data($column,$table,$condition,$search_value,$column_search,$from_date,$to_date,$date_range_column,$column_order,$length,$start,$where_in);

          $output = array(
//            "draw" => $paginate_config['draw'],
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data_list,
        );
        return $output;
    }
    public function checkAllowed($role_id){
        $current_url =& get_instance(); //  get a reference to CodeIgniter
        $controller_name=$current_url->router->fetch_class(); // for Class name or controller
        $method_name=$current_url->router->fetch_method(); // for method name

        if(isset($role_id) && $this->CI->common->checkAllowed($role_id,$controller_name,$method_name)){
            return true;
        }
        else{
            return false;
        }
    }

    public function checkPermission($user_id,$type){
        if($this->CI->common->checkPermission($user_id,$type)){
            return true;
        }
        else{
            return false;
        }
    }
    public function get_tg_from_user($REFRESH_TOKEN){
        $REFRESH_TOKEN=base64_decode($REFRESH_TOKEN);
        $tg=$this->CI->common->get_tg_from_user($REFRESH_TOKEN);
        return $tg;
    }
	public function get_all_io($tg) {
		$io_list='';
		if(!empty($tg)) {
			$io_list= $this->CI->common->get_data(
						array('TBL_USERS',array('TBL_ROLES','TBL_ROLES.ROLEID=TBL_USERS.ROLEID','inner')),
						array('TBL_USERS.USERID as io_id','TBL_USERS.NAME as io_name'),
						array('TBL_USERS.TG_CD'=> $tg, 'TBL_ROLES.ROLEID'=> 4)
					);
		}
		return $io_list;
	}
}
