<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class Dashboard_data  {
	
	private $CI;

	public function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('common_data');
		$this->CI->load->library('task');
        $this->CI->load->model('Common_model','common');
        $this->CI->load->model('login_model','login');
	}
    public function get_total_accident_count($tg,$ps,$from_date,$to_date){
		$condition=array('IS_DELETE'=>'0');
		if(!empty($tg)) {
			$condition['GRD_OFF_CD']=$tg;
		}
		if(!empty($ps)) {
			$condition['PS_CD']=$ps;
		}
        $data=$this->CI->common->get_data('ACC_REP',
										  '',
										  $condition,
										  '',
										  'num_rows',
										  array('from_date'=>$from_date,
												'to_date'=>$to_date,
												'date_range_coloumn'=>'CREATED_ON'
											   )
										 );
		return $data;
    }
	
	public function get_drunken_driving_count($tg,$ps,$from_date,$to_date){
		$condition=array('IS_DELETE'=>'0','D_TYPE_OF_TRAFFIC_VIOLATION'=>'96');
		if(!empty($tg)) {
			$condition['GRD_OFF_CD']=$tg;
		}
		if(!empty($ps)) {
			$condition['PS_CD']=$ps;
		}
        $data=$this->CI->common->get_data(array('ACC_REP',array('DRIVER_VEHICLE_DETAIL','ACC_REP.MASTER_RECORD_ID=DRIVER_VEHICLE_DETAIL.MASTER_RECORD_ID','left')),
										  array('UNIQUE(ACC_REP.MASTER_RECORD_ID)'),
										  $condition,
										  '',
										  'result_array',
										  array('from_date'=>$from_date,
												'to_date'=>$to_date,
												'date_range_coloumn'=>'CREATED_ON'
											   )
										 );
		// pre($this->CI->db->last_query()); exit();
		return count($data);
    }
	
	public function get_drunken_driving_data($tg,$ps,$from_date,$to_date){
		$condition=array('IS_DELETE'=>'0','D_TYPE_OF_TRAFFIC_VIOLATION'=>'96');
		if(!empty($tg)) {
			$condition['GRD_OFF_CD']=$tg;
		}
		if(!empty($ps)) {
			$condition['PS_CD']=$ps;
		}
        $data=$this->CI->common->get_data(array('ACC_REP',array('DRIVER_VEHICLE_DETAIL','ACC_REP.MASTER_RECORD_ID=DRIVER_VEHICLE_DETAIL.MASTER_RECORD_ID','left')),
										  array('UNIQUE(ACC_REP.MASTER_RECORD_ID)'),
										  $condition,
										  '',
										  'result_array',
										  array('from_date'=>$from_date,
												'to_date'=>$to_date,
												'date_range_coloumn'=>'CREATED_ON'
											   )
										 );
		// Inserting MASTER_RECORD_ID in an array
		$wherein = array('column'=>'ACC_REP.MASTER_RECORD_ID','value'=>array());
		foreach($data as $item) {
			array_push($wherein['value'], $item['MASTER_RECORD_ID']);
		}
		// Delete array element D_TYPE_OF_TRAFFIC_VIOLATION from array $condition
		unset($condition['D_TYPE_OF_TRAFFIC_VIOLATION']);
		$data=$this->CI->common->get_data('ACC_REP',
										  '',
										  $condition,
										  $wherein,
										  'result_array',
										  array('from_date'=>$from_date,
												'to_date'=>$to_date,
												'date_range_coloumn'=>'CREATED_ON'
											   )
										 );
		// pre($data); exit();
		return $data;
		
		
		
		
		
		// $paginate_config = array(
// //            'draw'=>'1',
            // 'table'=>'ACC_REP',
            // 'condition'=>$condition,
            // 'column'=>array("ACC_REP.MASTER_RECORD_ID","FIR_NO","TO_CHAR(FIR_DATE,'DD/MM/YYYY') AS FIR_DATE","GD_NO","TO_CHAR(GD_DATE,'DD/MM/YYYY') AS GD_DATE","TO_CHAR(ACC_DATE,'DD/MM/YYYY') AS ACC_DATE","ACC_TIME","GUARD_DETAIL.GUARD_NAME","M_PS_CODE.PS_NAME","ACC_PLACE","DROP_DOWN.VALUE NATURE_OF_ACCIDENT"),
            // 'column_search'=>array('FIR_NO','ACC_PLACE', 'M_PS_CODE.PS_NAME','GUARD_DETAIL.GUARD_NAME','DROP_DOWN.VALUE'),
            // 'from_date'=>$post['from_date'],
            // 'to_date'=>$post['to_date'],
            // 'search_value'=>$post['search']['value'],
            // 'date_range_column'=>'ACC_REP.CREATED_ON',
            // 'column_order'=>array("ACC_REP.CREATED_ON DESC"),
            // 'length'=>$post['length'],
            // 'start'=>$post['start']   
        // );

		// //pre($paginate_config); exit();

        // $output=$this->common_data->get_paginate_data($paginate_config);
		// // pre($this->db->last_query());

        // $recordsTotal=$output['recordsTotal'];
        // $recordsFiltered=$output['recordsFiltered'];
        // $data_list= $output['data'];

        // $data=$this->task->process_task_table_data($data_list);

          // $output = array(
            // "draw" => $post['draw'],
            // "recordsTotal" => $recordsTotal,
            // "recordsFiltered" => $recordsFiltered,
            // "data" => $data,
			// "test" => $post,
        // );  
        // echo json_encode($output);
    }
	
}
