<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 
class MY_Log
{
    private $CI;
    private $MASTER_RECORD_ID;
    private $prev_data;
    private $new_data;
    private $used_by;

    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->model('Common_model','common_model');
        $this->CI->load->helper('Utility');
        $this->CI->load->helper('file');
    }
    public function write_start($MASTER_RECORD_ID,$used_by){
        $this->MASTER_RECORD_ID=$MASTER_RECORD_ID;
        $this->used_by=$used_by;
        $this->prev_data=$this->get_log_data($this->MASTER_RECORD_ID);
    }
    public function write_end($MASTER_RECORD_ID,$used_by){
        $this->MASTER_RECORD_ID=$MASTER_RECORD_ID;
        $this->used_by=$used_by;
        $created_date=date('d/m/Y H:i:s');
        $this->new_data=$this->get_log_data($this->MASTER_RECORD_ID);
        
        $data=json_encode(array(
                    'ID'=>get_new_id(),
                    'PREV_RECORD'=>$this->prev_data,
                    'NEW_RECORD'=>$this->new_data,
                    'CREATED_ON'=>'$created_date',
                    'CREATED_BY'=>$this->used_by,
                ));
        if ( ! write_file(APPPATH .'opration_log', $data,'a+'))
        {
                echo 'Unable to write the file';
        }
        /*
        $this->CI->common_model->insert_log_data(
                                'TBL_LOG',
                                array(
                                    'ID'=>get_new_id(),
                                    'PREV_RECORD'=>implode(unpack("H*", json_encode($this->prev_data))),
                                    'NEW_RECORD'=>implode(unpack("H*", json_encode($this->new_data))),
                                    'CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                                    'CREATED_BY'=>$this->used_by,
                                )
                            );
                            */
    }
    private function get_log_data($MASTER_RECORD_ID){
       return array(
                            'general_data'=>$this->CI->common_model->get_data(
                                                                    'ACC_REP',
                                                                    '',
                                                                    array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID),
                                                                    "",
                                                                    'row_array'
                                                                ),
                            'driver_vehicle_data'=>$this->CI->common_model->get_data(
                                                                    'DRIVER_VEHICLE_DETAIL',
                                                                    '',
                                                                    array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID),
                                                                    "",
                                                                    'result_array'
                                                                ),
                            'person_detail'=>$this->CI->common_model->get_data(
                                                                    'PERSON_DETAIL',
                                                                    '',
                                                                    array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID),
                                                                    "",
                                                                    'result_array'
                                                                ),
                            'inj_detail'=>$this->CI->common_model->get_data(
                                                                    'ACC_REP_INJ',
                                                                    '',
                                                                    array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID),
                                                                    "",
                                                                    'result_array'
                                                                ),
                            'kill_detail'=>$this->CI->common_model->get_data(
                                                                    'ACC_REP_KILL',
                                                                    '',
                                                                    array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID),
                                                                    "",
                                                                    'result_array'
                                                                ),
                            'image_detail'=>$this->CI->common_model->get_data(
                                                                    'IMAGE_DETAIL',
                                                                    '',
                                                                    array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID),
                                                                    "",
                                                                    'result_array'
                                                                ),
            ); 
    }
    
}