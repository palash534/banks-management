<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class Task  {
	
	private $CI;

	public function __construct(){
		$this->CI =& get_instance();
        $this->CI->load->library('form_validation');
        $this->CI->load->library('encryption');
        $this->CI->load->helper('utility');
        $this->CI->load->model('Task_model','task_model');
        $this->CI->load->model('Common_model','common_model');
        $this->CI->load->helper('validation');
	}
    
    public function save_general($data,$USER_ID=null){
        //Get Validation Config
        $validation= array(
            'task_validation'=>get_new_task_validation_config(),
            'image_validation'=>get_image_validation_config()
        );
        //Master Record ID
        $MASTER_RECORD_ID=get_new_id();
        //Process Image Data
        if(isset($data['Image'])){
            $image_data=$this->process_image_data($data['Image'],$MASTER_RECORD_ID,$USER_ID);
        }
        else{
            $image_data=array();
        }
        $image_count=count($image_data);
        //Process General Data
        $general_data=$this->process_general_data($data,$MASTER_RECORD_ID,0,0,$image_count,0,0,0,$USER_ID);
        //General Data Validation
        $this->CI->form_validation->reset_validation();
        $this->CI->form_validation->set_data($general_data);
        $this->CI->form_validation->set_rules($validation['task_validation']);
        if($this->CI->form_validation->run()==FALSE){
            $response=array(
                'status'=>'error',
                'type'=>'General',
                'error_desc'=>$this->CI->form_validation->error_array()
            );
            return $response;
        }
        //Image Data Validation
        foreach ($image_data as $key => $image_item) {
            $this->CI->form_validation->reset_validation();
            $this->CI->form_validation->set_data($person_item);
            $this->CI->form_validation->set_rules($validation['image_validation']);
            if($this->CI->form_validation->run()==FALSE){
                $response=array(
                    'status'=>'error',
                    'type'=>'Image',
                    'error_desc'=>$this->CI->form_validation->error_array()
                );
                return $response;
            }
        }
        //Save Data
        $status=$this->CI->task_model->save_general_image($general_data,$image_data);
        return $MASTER_RECORD_ID;
    }
    public function save_driver_vehicle($data,$MASTER_RECORD_ID,$USER_ID=null){
        //Get Validation Config
        $driver_vehicle_validation=get_driver_vehicle_validation_config();
        //Process Driver Vehicle
        if(isset($data['Driver_Vehicle'])){
            $driver_vehicle_data=$this->process_driver_vehicle_data($data['Driver_Vehicle'],$MASTER_RECORD_ID,$USER_ID);
            $driver_vehicle_count=count($driver_vehicle_data);
        }
        else{
            $driver_vehicle_data=array();
        }
        //Driver Vehicle Validation
        foreach ($driver_vehicle_data as $key => $driver_vehicle_item) {
            $this->CI->form_validation->reset_validation();
            $this->CI->form_validation->set_data($driver_vehicle_item);
            $this->CI->form_validation->set_rules($validation['driver_vehicle_validation']);
            if($this->CI->form_validation->run()==FALSE){
                $response=array(
                    'status'=>'error',
                    'type'=>'Driver_vehicle',
                    'error_desc'=>$this->CI->form_validation->error_array()
                );
                return $response;
            }
        }
         //Save Data
        $status=$this->driver_vehicle_operation_with_refresh($driver_vechicle,$MASTER_RECORD_ID,$USER_ID,'insert');
        return $status;
    }
    public function save_person($data,$USER_ID=null){
        
    }
    public function save_image($data,$USER_ID=null){
        if(isset($data['Image'])){
            $image = array();
            foreach ($data['Image'] as $key => $value) {
                array_push($image, array('path'=>$data['Image'][$key]));    
            }
            
            $image_data=$this->process_image_data($image,$this->encryption->decrypt(base64_decode($data['master_record_id'])),$USER_ID);
            $image_count=count($image_data);
        }
        else{
            $image_data=array();
        }
        //Image Data Validation
        $image_validation = get_image_validation_config();
        foreach ($image_data as $key => $image_item) {
            $this->CI->form_validation->reset_validation();
            $this->CI->form_validation->set_data($image_item);
            $this->CI->form_validation->set_rules($image_validation);
            if($this->CI->form_validation->run()==FALSE){
                $response=array(
                    'status'=>'error',
                    'type'=>'Image',
                    'error_desc'=>$this->CI->form_validation->error_array()
                );
                return $response;
            }
        }
        foreach ($image_data as $key => $value) {
            $this->CI->task_model->insert_image($image_data[$key]);    
        }
        return $image_data;
    }
    public function delete_image($image_id,$USER_ID=null){
        $image_id=$this->encryption->decrypt(base64_decode($image_id));
        $status=$this->CI->common_model->delete_data('IMAGE_DETAIL','IS_DELETE','ID',$image_id,$USER_ID);
        return $status;
    }

    public function save_new_task($data,$USER_ID=null){
        //Get Validation Config
        $validation= array(
            'task_validation'=>get_new_task_validation_config(),
            'driver_vehicle_validation'=>get_driver_vehicle_validation_config(),
            'person_validation'=>get_person_validation_config(),
            'image_validation'=>get_image_validation_config(),
            'kill_validation'=>get_kill_validation_config(),
            'inj_validation'=>get_inj_validation_config()
        );
        //Processing of Data
        $driver_vehicle_count=0;
        $person_count=0;
        $image_count=0;
        $kill_count=0;
        $inj_gre_count=0;
        $inj_min_count=0;
        $id="";
        if(isset($data['id'])){
            
            $id= $this->encryption->decrypt(base64_decode($data['id']));
        }
        else{
            $id=get_new_id();    
        }
        if(isset($data['Driver_Vehicle'])){
            $driver_vehicle_data=$this->process_driver_vehicle_data($data['Driver_Vehicle'],$id,$USER_ID);
            $driver_vehicle_count=count($driver_vehicle_data);
        }
        else{
            $driver_vehicle_data=array();
        }
        if(isset($data['Person'])){
            $person_data=$this->process_person_data($data['Person'],$id,$USER_ID);
            $person_count=count($person_data);
        }
        else{
            $person_data=array();
        }
        if(isset($data['Image'])){
            $image_data=$this->process_image_data($data['Image'],$id,$USER_ID);
            $image_count=count($image_data);
        }
        else{
            $image_data=array();
        }
        if(isset($data['Kill'])){
            $date=get_specific_date($data['fir_date'],'server');
            $general=array(
                'FIR_NO'=>isset($data['fir_no']) ? $data['fir_no'] : null,
                'FIR_DATE'=>isset($data['fir_date']) ? "TO_DATE('$date','dd/mm/yyyy')" : null,
                'PS_CD'=>isset($data['ps_name']) ? $data['ps_name'] : null,
            );
            $kill_data=$this->process_kill_data($data['Kill'],$id,$general,$USER_ID);
            $kill_count=count($kill_data);
        }
        else{
            $kill_data=array();
        }
        if(isset($data['Inj'])){
            $date=get_specific_date($data['fir_date'],'server');
            $general=array(
                'FIR_NO'=>isset($data['fir_no']) ? $data['fir_no'] : null,
                'FIR_DATE'=>isset($data['fir_date']) ? "TO_DATE('$date','dd/mm/yyyy')" : null,
                'PS_CD'=>isset($data['ps_name']) ? $data['ps_name'] : null,
            );
            $inj_data=$this->process_inj_data($data['Inj'],$id,$general,$USER_ID);
            $like='2';//Grevious Injury
            $result = array_filter($inj_data, function ($item) use ($like) {
                if (stripos($item['ACC_INJ_TYPE'], $like) !== false) {
                    return true;
                }
                return false;
            });
            $inj_gre_count=count($result);
            $like='3';//Minor Injury
            $result = array_filter($inj_data, function ($item) use ($like) {
                if (stripos($item['ACC_INJ_TYPE'], $like) !== false) {
                    return true;
                }
                return false;
            });
            $inj_min_count=count($result);
        }
        else{
            $inj_data=array();
        }

        $general_data=$this->process_general_data($data,$id,$driver_vehicle_count,$person_count,$image_count,$kill_count,$inj_gre_count,$inj_min_count,$USER_ID);
        //General Data Validation
        $this->CI->form_validation->reset_validation();
        $this->CI->form_validation->set_data($general_data);
        $this->CI->form_validation->set_rules($validation['task_validation']);
        if($this->CI->form_validation->run()==FALSE){
            $response=array(
                'status'=>'error',
                'type'=>'General',
                'error_desc'=>$this->CI->form_validation->error_array()
            );
            return $response;
        }
        //Driver Vehicle Validation
        foreach ($driver_vehicle_data as $key => $driver_vehicle_item) {
            $this->CI->form_validation->reset_validation();
            $this->CI->form_validation->set_data($driver_vehicle_item);
            $this->CI->form_validation->set_rules($validation['driver_vehicle_validation']);
            if($this->CI->form_validation->run()==FALSE){
                $response=array(
                    'status'=>'error',
                    'type'=>'Driver_vehicle',
                    'error_desc'=>$this->CI->form_validation->error_array()
                );
                return $response;
            }
        }

        //Person Data Validation
        foreach ($person_data as $key => $person_item) {
            $this->CI->form_validation->reset_validation();
            $this->CI->form_validation->set_data($person_item);
            $this->CI->form_validation->set_rules($validation['person_validation']);
            if($this->CI->form_validation->run()==FALSE){
                $response=array(
                    'status'=>'error',
                    'type'=>'Person',
                    'error_desc'=>$this->CI->form_validation->error_array()
                );
                return $response;
            }
        }
        //Image Data Validation
        foreach ($image_data as $key => $image_item) {
            $this->CI->form_validation->reset_validation();
            $this->CI->form_validation->set_data($person_item);
            $this->CI->form_validation->set_rules($validation['image_validation']);
            if($this->CI->form_validation->run()==FALSE){
                $response=array(
                    'status'=>'error',
                    'type'=>'Image',
                    'error_desc'=>$this->CI->form_validation->error_array()
                );
                return $response;
            }
        }
        //Kill Data Validation
        foreach ($kill_data as $key => $kill_item) {
            $this->CI->form_validation->reset_validation();
            $this->CI->form_validation->set_data($kill_item);
            $this->CI->form_validation->set_rules($validation['kill_validation']);
            if($this->CI->form_validation->run()==FALSE){
                $response=array(
                    'status'=>'error',
                    'type'=>'Kill',
                    'error_desc'=>$this->CI->form_validation->error_array()
                );
                return $response;
            }
        }
        //Inj Data Validation
        foreach ($inj_data as $key => $inj_item) {
            $this->CI->form_validation->reset_validation();
            $this->CI->form_validation->set_data($inj_item);
            $this->CI->form_validation->set_rules($validation['inj_validation']);
            if($this->CI->form_validation->run()==FALSE){
                $response=array(
                    'status'=>'error',
                    'type'=>'Inj',
                    'error_desc'=>$this->CI->form_validation->error_array()
                );
                return $response;
            }
        }
        
        if(isset($data['id'])){
            $deleted_driver_vehicle_data=$data['deleted_driver_vehicle_data'];
            $deleted_person_data=$data['deleted_person_data'];
            $deleted_image_data=$data['deleted_image_data'];
            $deleted_kill_data=$data['deleted_kill_data'];
            $deleted_inj_data=$data['deleted_inj_data'];
            $status=$this->CI->task_model->update_task($general_data,$driver_vehicle_data,$person_data,$image_data,$kill_data,$inj_data,$deleted_driver_vehicle_data,$deleted_person_data,$deleted_image_data,$deleted_kill_data,$deleted_inj_data,$USER_ID);
        }
        else{
            $status=$this->CI->task_model->save_task($general_data,$driver_vehicle_data,$person_data,$image_data,$kill_data,$inj_data);    
        }
    }
    private function process_driver_vehicle_data($driver_vehicle,$MASTER_RECORD_ID,$USER_ID){
        $created_date=date('d/m/Y H:i:s');
        $driver_vehcile_processed_data=array();
        if(!is_array($driver_vehicle))
            return $driver_vehcile_processed_data;
        $i=0;
        foreach($driver_vehicle as $key=>$driver_vehcile_item){
            $driver_vehicle_processed_item=array(
                'ID'=>isset($driver_vehcile_item['id']) ? !empty($driver_vehcile_item['id']) ? $this->CI->encryption->decrypt(base64_decode($driver_vehcile_item['id'])) : get_new_id().$i : get_new_id().$i,
                //Driver
                'D_LICENSE_NO'=>isset($driver_vehcile_item['license_no']) ? !empty($driver_vehcile_item['license_no'])? $driver_vehcile_item['license_no'] : null : null,
                'D_TYPE_OF_LICENSE'=>isset($driver_vehcile_item['type_of_license']) ? !empty($driver_vehcile_item['type_of_license'])? $driver_vehcile_item['type_of_license'] : null : null,
                'D_DRIVER_GENDER'=>isset($driver_vehcile_item['driver_sex']) ? !empty($driver_vehcile_item['driver_sex'])? $driver_vehcile_item['driver_sex'] : null : null,
                'D_DRIVER_AGE'=>isset($driver_vehcile_item['driver_age']) ? !empty($driver_vehcile_item['driver_age'])? $driver_vehcile_item['driver_age'] : null : null,
                'D_IMPACTING_VEHICLE'=>isset($driver_vehcile_item['impacting_vehicle']) ? !empty($driver_vehcile_item['impacting_vehicle'])? $driver_vehcile_item['impacting_vehicle'] : null : null,
                'D_TYPE_OF_INJURY'=>isset($driver_vehcile_item['type_of_injury']) ? !empty($driver_vehcile_item['type_of_injury'])? $driver_vehcile_item['type_of_injury'] : null : null,
                'D_TYPE_OF_TRAFFIC_VIOLATION'=>isset($driver_vehcile_item['traffic_violation']) ? !empty($driver_vehcile_item['traffic_violation'])? $driver_vehcile_item['traffic_violation'] : null : null,
                'D_SAFETY_DEVICE'=>isset($driver_vehcile_item['driver_safety_device']) ? !empty($driver_vehcile_item['driver_safety_device'])? $driver_vehcile_item['driver_safety_device'] : null : null,
                //Vehicle
                'V_REGISTRATION_NO'=>isset($driver_vehcile_item['reg_No']) ? !empty($driver_vehcile_item['reg_No'])? $driver_vehcile_item['reg_No'] : null : null,
                'V_PASSENGER_GOOD_VEHICLE'=>isset($driver_vehcile_item['passenger_good_vehicle']) ? !empty($driver_vehcile_item['passenger_good_vehicle'])? $driver_vehcile_item['passenger_good_vehicle'] : null : null,
                'V_VEHICLE_TYPE'=>isset($driver_vehcile_item['vehicle_type']) ? !empty($driver_vehcile_item['vehicle_type'])? $driver_vehcile_item['vehicle_type'] : null : null,
                'V_LOAD_CONDITION'=>isset($driver_vehcile_item['load_condition']) ? !empty($driver_vehcile_item['load_condition'])? $driver_vehcile_item['load_condition'] : null : null,
                'V_TYPE_OF_DISPOSITION'=>isset($driver_vehcile_item['type_of_disposition']) ? !empty($driver_vehcile_item['type_of_disposition'])? $driver_vehcile_item['type_of_disposition'] : null : null,
                'V_MECHANIC_FAILURE'=>isset($driver_vehcile_item['mechanical_failure']) ? !empty($driver_vehcile_item['mechanical_failure'])? $driver_vehcile_item['mechanical_failure'] : null : null,
                'V_AGE_OF_VEHICLE'=>isset($driver_vehcile_item['age_of_vehicle']) ? !empty($driver_vehcile_item['age_of_vehicle'])? $driver_vehcile_item['age_of_vehicle'] : null : null,
                'VD_CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'VD_CREATED_BY'=>$USER_ID,
                'VD_UPDATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'VD_UPDATED_BY'=>$USER_ID,
                'VD_IS_DELETE'=>0,
                'MASTER_RECORD_ID'=>$MASTER_RECORD_ID
            );
            array_push($driver_vehcile_processed_data,$driver_vehicle_processed_item);
            $i++;
        }
        return $driver_vehcile_processed_data;
    }

    private function process_person_data($person,$MASTER_RECORD_ID,$USER_ID){
        $created_date=date('d/m/Y H:i:s');
        $person_processed_data=array();
        if(!is_array($person))
            return $person_processed_data;
        $i=0;
        foreach($person as $key=>$person_item){
            $person_processed_item=array(
                'ID'=>isset($person_item['id']) ? !empty($person_item['id'])? $this->CI->encryption->decrypt(base64_decode($person_item['id'])) : get_new_id().$i : get_new_id().$i,
                'TYPE_OF_PERSON'=>isset($person_item['person_type']) ? !empty($person_item['person_type'])? $person_item['person_type'] : null : null,
                'PERSON_GENDER'=>isset($person_item['person_sex']) ? !empty($person_item['person_sex'])? $person_item['person_sex'] : null : null,
                'PERSON_AGE'=>isset($person_item['person_age']) ? !empty($person_item['person_age'])? $person_item['person_age'] : null : null,
                'IMPACTING_VEHICLE'=>isset($person_item['person_impact_vehicle']) ? !empty($person_item['person_impact_vehicle'])? $person_item['person_impact_vehicle'] : null : null,
                'TYPE_OF_INJURY'=>isset($person_item['person_injury']) ? !empty($person_item['person_injury'])? $person_item['person_injury'] : null : null,
                'SAFETY_DEVICE'=>isset($person_item['person_safety_device']) ? !empty($person_item['person_safety_device'])? $person_item['person_safety_device'] : null : null,
                'CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'CREATED_BY'=>$USER_ID,
                'UPDATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'UPDATED_BY'=>$USER_ID,
                'IS_DELETE'=>0,
                'MASTER_RECORD_ID'=>$MASTER_RECORD_ID
            );
            array_push($person_processed_data,$person_processed_item);
            $i++;
        }
        return $person_processed_data;
    }

    private function process_image_data($image,$MASTER_RECORD_ID,$USER_ID){
        $created_date=date('d/m/Y H:i:s');
        $image_processed_data=array();
        if(!is_array($image))
            return $image_processed_data;
        $i=0;
        foreach($image as $key=>$image_item){
            $image_processed_item=array(
                'ID'=>isset($image_item['id']) ? !empty($image_item['id']) ? $this->encryption->decrypt(base64_decode($image_item['id'])) : get_new_id().$i : get_new_id().$i,
                'PATH'=>isset($image_item['path']) ? !empty($image_item['path'])? $image_item['path'] : null : null,
                'CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'CREATED_BY'=>$USER_ID,
                'UPDATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'UPDATED_BY'=>$USER_ID,
                'IS_DELETE'=>0,
                'MASTER_RECORD_ID'=>$MASTER_RECORD_ID
            );
            array_push($image_processed_data,$image_processed_item);
            $i++;
        }
        return $image_processed_data;
    }
 
    private function process_kill_data($kill,$MASTER_RECORD_ID,$general,$USER_ID,$person_id=null){
        $created_date=date('d/m/Y H:i:s');

        $kill_processed_data=array();
        if(!is_array($kill))
            return $kill_processed_data;
        $i=0;
        foreach($kill as $key=>$kill_item){
            $died_on=get_specific_date($kill_item['kill_died_on'],'server');
            $kill_processed_item=array(
                'ID'=>isset($kill_item['id']) ? !empty($kill_item['id']) ? $this->CI->encryption->decrypt(base64_decode($kill_item['id'])) : get_new_id().$i : get_new_id().$i,
                'PS_CD'=>isset($general['FIR_DATE']) ? $general['PS_CD'] : null ,
                'FIR_NO'=>isset($general['FIR_DATE']) ? $general['FIR_NO'] : null ,
                'FIR_DATE'=>isset($general['FIR_DATE']) ? "TO_DATE('".$general['FIR_DATE']."','dd/mm/yyyy')" : null ,
                'ACC_KIL_SEX'=>isset($kill_item['kill_sex']) ? !empty($kill_item['kill_sex'])? $kill_item['kill_sex'] : null : null,
                'ACC_KIL_AGE'=>isset($kill_item['kill_age']) ? !empty($kill_item['kill_age'])? $kill_item['kill_age'] : null : null,
                'ACC_KIL_REMOVED_HOSPITAL'=>isset($kill_item['kill_removed_hospital']) ? !empty($kill_item['kill_removed_hospital'])? $kill_item['kill_removed_hospital'] : null : null,
                'ACC_KIL_DIED_ON'=>isset($kill_item['kill_died_on']) ? !empty($kill_item['kill_died_on'])? "TO_DATE('$died_on','dd/mm/yyyy')" : null  : null,
                'ACC_KIL_NAME'=>isset($kill_item['kill_name']) ? !empty($kill_item['kill_name'])? $kill_item['kill_name'] : null : null,
                'ACC_KIL_ADDRESS'=>isset($kill_item['kill_address']) ? !empty($kill_item['kill_address'])? $kill_item['kill_address'] : null : null,
                'PARENT_TABLE'=>'P', //Need to change accordingly as D for driver P for Person
                'PARENT_TABLE_ID'=>$person_id,
                'CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'CREATED_BY'=>$USER_ID,
                'UPDATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'UPDATED_BY'=>$USER_ID,
                'IS_DELETE'=>0,
                'MASTER_RECORD_ID'=>$MASTER_RECORD_ID
            );
            array_push($kill_processed_data,$kill_processed_item);
            $i++;
        }
        return $kill_processed_data;
    }

    private function process_inj_data($inj,$MASTER_RECORD_ID,$general,$USER_ID,$person_id=null){
        $created_date=date('d/m/Y H:i:s');
        $inj_processed_data=array();
        if(!is_array($inj))
            return $inj_processed_data;
        $i=0;
        foreach($inj as $key=>$inj_item){
            $inj_processed_item=array(
                'ID'=> !empty($inj_item['id']) ?  $this->CI->encryption->decrypt(base64_decode($inj_item['id'])) : get_new_id().$i,
                'PS_CD'=>isset($general['PS_CD']) ? $general['PS_CD'] : null ,
                'FIR_NO'=>isset($general['FIR_NO']) ? $general['FIR_NO'] : null ,
                'FIR_DATE'=>isset($general['FIR_DATE']) ? "TO_DATE('".$general['FIR_DATE']."','dd/mm/yyyy')" : null ,
                'ACC_INJ_SEX'=>isset($inj_item['inj_sex']) ? !empty($inj_item['inj_sex'])? $inj_item['inj_sex'] : null : null,
                'ACC_INJ_AGE'=>isset($inj_item['inj_age']) ? !empty($inj_item['inj_age'])? $inj_item['inj_age'] : null : null,
                'ACC_INJ_REMOVED_HOSPITAL'=>isset($inj_item['inj_removed_hospital']) ? !empty($inj_item['inj_removed_hospital'])? $inj_item['inj_removed_hospital'] : null : null,
                'ACC_INJ_CONDITION_OF_VICTIM'=>isset($inj_item['inj_victim_condition']) ? !empty($inj_item['inj_victim_condition'])? $inj_item['inj_victim_condition'] : null : null,
                'ACC_INJ_NAME'=>isset($inj_item['inj_name']) ?!empty($inj_item['inj_name'])? $inj_item['inj_name'] : null : null,
                'ACC_INJ_ADDRESS'=>isset($inj_item['inj_address']) ? !empty($inj_item['inj_address'])? $inj_item['inj_address'] : null : null,
                'ACC_INJ_TYPE'=>isset($inj_item['type']) ? !empty($inj_item['type'])? $inj_item['type'] : null : null,//Value=2/3
                'CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'PARENT_TABLE'=>'P', //Need to change accordingly as D for driver P for Person
                'PARENT_TABLE_ID'=>$person_id,
                'CREATED_BY'=>$USER_ID,
                'UPDATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
                'UPDATED_BY'=>$USER_ID,
                'IS_DELETE'=>'0',
                'MASTER_RECORD_ID'=>$MASTER_RECORD_ID
            );
            array_push($inj_processed_data,$inj_processed_item);
            $i++;
        }
        return $inj_processed_data;
    }

    private function process_general_data($data,$id,$driver_vehicle_count,$person_count,$image_count,$kill_count,$inj_gre_count,$inj_min_count,$USER_ID){
        $fir_date=get_specific_date($data['fir_date'],'server');
        $created_date=date('d/m/Y H:i:s');
        $fir_time=date('H:i',strtotime($data['fir_time']));
        $gd_date=get_specific_date($data['gd_date'],'server');//date('m/d/Y',strtotime($data['gd_date']));
        $gd_time=date('H:i',strtotime($data['gd_time']));
        $acc_date=get_specific_date($data['acc_date'],'server');//date('m/d/Y',strtotime($data['acc_date']));
        $acc_time=date('H:i:s',strtotime($data['acc_time']));
        $general_item=array(
            'MASTER_RECORD_ID'=>$id,
            'FIR_NO'=>isset($data['fir_no']) ? !empty($data['fir_no'])? $data['fir_no'] : null : null,
            'FIR_DATE'=>isset($data['fir_date']) ? !empty($data['fir_date'])? "TO_DATE('$fir_date','dd/mm/yyyy')" : null : null,
            'FIR_TIME'=>isset($data['fir_time']) ? !empty($data['fir_time'])? "TO_DATE('$fir_time','HH24:mi')" : null : null,
            'GD_NO'=>isset($data['gd_no']) ? !empty($data['gd_no'])? $data['fir_no'] : null : null,
            'GD_DATE'=>isset($data['gd_date']) ? !empty($data['gd_date'])? "TO_DATE('$gd_date','dd/mm/yyyy')" : null : null,
            'GD_TIME'=>isset($data['gd_time']) ? !empty($data['gd_time'])? "TO_DATE('$gd_time','HH24:mi')" : null : null,
            'ACC_DATE'=>isset($data['acc_date']) ? !empty($data['acc_date'])? "TO_DATE('$acc_date','dd/mm/yyyy')" : null : null,
            'ACC_TIME'=>isset($data['acc_time']) ? !empty($data['acc_time'])? $data['acc_time'] : null : null,
            'ACC_US_CD'=>isset($data['sec_law']) ? !empty($data['sec_law'])? $data['sec_law'] : null : null,
            'GRD_OFF_CD'=>isset($data['tg_name']) ? !empty($data['tg_name'])? $data['tg_name'] : null : null,
            'PS_CD'=>isset($data['ps_name']) ? !empty($data['ps_name'])? $data['ps_name'] : null : null,
            'ACC_PLACE'=>isset($data['accident_Place']) ? !empty($data['accident_Place'])? $data['accident_Place'] : null : null,
            'LAT_DEG_MIN'=>isset($data['latitude']) ? !empty($data['latitude'])? $data['latitude'] : null : null,
            'LONG_DEG_MIN'=>isset($data['longitude']) ? !empty($data['longitude'])? $data['longitude'] : null : null,
            'NATURE_OF_ACCIDENT'=>isset($data['accident_type']) ? !empty($data['accident_type'])? $data['accident_type'] : null : null,
            'NO_OF_FATALITIES'=>$kill_count,
            'NO_GRIEVOUS_INJURY'=>$inj_gre_count,
            'NO_OF_MINOR_INJURY'=>$inj_min_count,
            'WETHER'=>isset($data['weather']) ? !empty($data['weather'])? $data['weather'] : null : null,
			'HIT_RUN'=>isset($data['hit_and_run']) ? !empty($data['hit_and_run'])? $data['hit_and_run'] : null : null,
			'COLLISION_TYPE'=>isset($data['collision_type']) ? !empty($data['collision_type'])? $data['collision_type'] : null : null,
            'ROAD_TYPE'=>isset($data['road_Type']) ? !empty($data['road_Type'])? $data['road_Type'] : null : null,
            'TYPE_OF_AREA'=>isset($data['area_type']) ? !empty($data['area_type'])? $data['area_type'] : null : null,
            'ACC_SPOT'=>isset($data['accident_Spot']) ? !empty($data['accident_Spot'])? $data['accident_Spot'] : null : null,
            'ROAD_NM'=>isset($data['road_name']) ? !empty($data['road_name'])? $data['road_name'] : null : null,
            'ROAD_CONDITION'=>isset($data['surface_Cond']) ? !empty($data['surface_Cond'])? $data['surface_Cond'] : null : null,
            'ROAD_FEATURES'=>isset($data['rd_feature']) ? !empty($data['rd_feature'])? $data['rd_feature'] : null : null,
            'ROAD_RULE'=>isset($data['speed_Limit']) ? !empty($data['speed_Limit'])? $data['speed_Limit'] : null : null,
            'JUNCTION_TYPE'=>isset($data['rd_junction']) ? !empty($data['rd_junction'])? $data['rd_junction'] : null : null,
            'JUNCTION_CONTROL'=>isset($data['traffic_ctrl']) ? !empty($data['traffic_ctrl'])? $data['traffic_ctrl'] : null : null,
            'LANES'=>isset($data['Lanes']) ? !empty($data['Lanes'])? $data['Lanes'] : null : null,
            'ROAD_CHAINGE'=>isset($data['road_chainge']) ? !empty($data['road_chainge'])? $data['road_chainge'] : null : null,
            'VISIBILITY'=>isset($data['visibility']) ? !empty($data['visibility'])? $data['visibility'] : null : null,
            'PHYSICAL_DIVIDER'=>isset($data['phy_Divider']) ? !empty($data['phy_Divider'])? $data['phy_Divider'] : null : null,
            'ONGOIG_RD_WRKS'=>isset($data['roadWorks']) ? !empty($data['roadWorks'])? $data['roadWorks'] : null : null,
            'PEDESTRIAN_INVOLVED'=>isset($data['pedes_inv']) ? !empty($data['pedes_inv'])? $data['pedes_inv'] : null : null,
            'NO_OF_DRIVER'=>$driver_vehicle_count,
            'NO_OF_VEHICLE'=>$driver_vehicle_count,
            'NO_OF_PERSON'=>$person_count,
            'CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
            'CREATED_BY'=>$USER_ID,
            'UPDATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
            'UPDATED_BY'=>$USER_ID,
            'IS_DELETE'=>'0',
            'STATUS'=>null,
        );
        $general_data['STATUS']=$this->get_status($general_data);
        return $general_item;
    }

    public function process_task_table_data($data_list){
        $this->CI->load->library('encryption');
        // $no=1;
        // $data=array();
        foreach ($data_list as $key => $value) {
            
            // $row = array();
            // $row[] = $no++;
            // $row[] = "<a target='_blank' href='#'>$Emp_list->EMPNO</a>";
            $value->MASTER_RECORD_ID=base64_encode($this->CI->encryption->encrypt($value->MASTER_RECORD_ID));
            // $row[] = isset($value->FIR_NO) ? $value->FIR_NO ." Dated ".$value->FIR_DATE:"";
            // $row[] = isset($value->GD_NO) ? $value->FIR_NO ." Dated ".$value->GD_DATE:"";
            // $row[] = $value->PS_NAME ." of ".$value->GUARD_NAME;
            // $row[] = isset($value->ACC_DATE) ? $value->ACC_DATE ." at ".$value->ACC_TIME:"";
            // $row[] = $value->ACC_PLACE;
            // $row[] = $value->NATURE_OF_ACCIDENT;
            // $row[] = "<button class='btn btn-md' data-tag='$id'>Show</button><button class='btn btn-md' tag='$id'>Edit</button><button class='btn btn-md' tag='$id'>Delete</button>";
            // array_push($data,$row);
        }
        return $data_list;
    }

    public function get_specific_accident($accident_id_encrypted){
        $MASTER_RECORD_ID=$this->CI->encryption->decrypt(base64_decode($accident_id_encrypted));
		$data=array(
            'general_data'=>$this->CI->common_model->get_data('ACC_REP'
                ,array(
                    'MASTER_RECORD_ID',
                    'FIR_NO fir_no',
					"TO_CHAR(FIR_DATE,'DD/MM/YYYY') fir_date",
					"TO_CHAR(FIR_TIME,'HH24:mi') fir_time",
                    'GD_NO gd_no',
					"TO_CHAR(GD_DATE,'DD/MM/YYYY') gd_date",
					"TO_CHAR(GD_TIME,'HH24:mi') gd_time",
					"TO_CHAR(ACC_DATE,'DD/MM/YYYY') acc_date",
                    'ACC_TIME acc_time',
                    'ACC_US_CD sec_law',
                    'GRD_OFF_CD tg_name',
					'PS_CD ps_name' ,
                    'ACC_PLACE accident_Place',
                    'LAT_DEG_MIN latitude',
                    'LONG_DEG_MIN longitude',
                    'NATURE_OF_ACCIDENT accident_type',
                    'NO_OF_FATALITIES',
                    'NO_GRIEVOUS_INJURY',
                    'NO_OF_MINOR_INJURY',
                    'WETHER weather',
					'HIT_RUN hit_and_run',
					'COLLISION_TYPE collision_type',
                    'ROAD_TYPE road_Type',
                    'TYPE_OF_AREA area_type',
                    'ACC_SPOT accident_Spot',
                    'ROAD_NM road_name',
                    'ROAD_CONDITION surface_Cond',
                    'ROAD_FEATURES rd_feature',
                    'ROAD_RULE speed_Limit',
                    'JUNCTION_TYPE rd_junction',
                    'JUNCTION_CONTROL traffic_ctrl',
                    'LANES Lanes',
                    'ROAD_CHAINGE',
                    'VISIBILITY visibility',
                    'PHYSICAL_DIVIDER phy_Divider',
                    'ONGOIG_RD_WRKS roadWorks',
                    'PEDESTRIAN_INVOLVED pedes_inv',
                    'NO_OF_DRIVER',
                    'NO_OF_VEHICLE',
                    'NO_OF_PERSON'
                ),
                array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID,'IS_DELETE'=>'0'),"",'row_array'), 
            'driver_vehicle_data'=>$this->CI->common_model->get_data('DRIVER_VEHICLE_DETAIL'
                ,array(
                    'ID',
                    //Driver
                    'D_LICENSE_NO license_no',
                    'D_TYPE_OF_LICENSE type_of_license',
                    'D_DRIVER_GENDER driver_sex',
                    'D_DRIVER_AGE driver_age',
                    'D_IMPACTING_VEHICLE impacting_vehicle',
                    'D_TYPE_OF_INJURY type_of_injury',
                    'D_TYPE_OF_TRAFFIC_VIOLATION traffic_violation',
                    'D_SAFETY_DEVICE driver_safety_device',
                    //Vehicle
                    'V_REGISTRATION_NO reg_No',
                    'V_PASSENGER_GOOD_VEHICLE passenger_good_vehicle',
                    'V_VEHICLE_TYPE vehicle_type',
                    'V_LOAD_CONDITION load_condition',
                    'V_TYPE_OF_DISPOSITION type_of_disposition',
                    'V_MECHANIC_FAILURE mechanical_failure',
                    'V_AGE_OF_VEHICLE age_of_vehicle',
                ),
                array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID,'VD_IS_DELETE'=>'0'),"","result_array"),
            'image_data'=>$this->CI->common_model->get_data('IMAGE_DETAIL'
                ,array(
                    'ID',
                    'PATH',
                ),
                array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID,'IS_DELETE'=>'0'),"","result_array"),
            'person_data'=>$this->CI->common_model->get_data('PERSON_DETAIL'
                ,array(
                    'ID',
                    'TYPE_OF_PERSON person_type',
                    'PERSON_GENDER person_sex',
                    'PERSON_AGE person_age',
                    'IMPACTING_VEHICLE person_impact_vehicle',
                    'TYPE_OF_INJURY person_injury',
                    'SAFETY_DEVICE person_safety_device',
                ),
                array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID,'IS_DELETE'=>'0'),"","result_array"),
            'inj_data'=>$this->CI->common_model->get_data('ACC_REP_INJ'
                ,array(
                    'ID',
                    'PS_CD ps_name',
                    'FIR_NO fir_no',
					"TO_CHAR(FIR_DATE,'DD/MM/YYYY') fir_date",
                    'ACC_INJ_SEX inj_sex',
                    'ACC_INJ_AGE inj_age',
                    'ACC_INJ_REMOVED_HOSPITAL inj_removed_hospital',
                    'ACC_INJ_CONDITION_OF_VICTIM inj_victim_condition',
                    'ACC_INJ_NAME inj_name',
                    'ACC_INJ_ADDRESS inj_address',
                    'ACC_INJ_TYPE type',//Value=2/3
                    'PARENT_TABLE_ID p_id'
                ),
                array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID,'IS_DELETE'=>'0'),"","result_array"),
            'kill_data'=>$this->CI->common_model->get_data('ACC_REP_KILL'
                ,array(
                    'ID',
                    'PS_CD ps_name',
                    'FIR_NO fir_no',
					"TO_CHAR(FIR_DATE,'DD/MM/YYYY') fir_date",
                    'ACC_KIL_SEX kill_sex',
                    'ACC_KIL_AGE kill_age',
                    'ACC_KIL_REMOVED_HOSPITAL kill_removed_hospital',
					"TO_CHAR(ACC_KIL_DIED_ON,'DD/MM/YYYY') kill_died_on",
                    'ACC_KIL_NAME kill_name',
                    'ACC_KIL_ADDRESS kill_address',
                    'PARENT_TABLE_ID p_id'
                ),
                array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID,'IS_DELETE'=>'0'),"","result_array"),     
        );
        $data['general_data']['MASTER_RECORD_ID']=base64_encode($this->CI->encryption->encrypt($data['general_data']['MASTER_RECORD_ID']));
        foreach ($data['driver_vehicle_data'] as $key => $value) {
            $data['driver_vehicle_data'][$key]['ID']=base64_encode($this->CI->encryption->encrypt($value['ID']));
        }
        foreach ($data['image_data'] as $key => $value) {
            $data['image_data'][$key]['ID']=base64_encode($this->CI->encryption->encrypt($value['ID']));
        }
        foreach ($data['person_data'] as $key => $value) {
            $data['person_data'][$key]['ID']=base64_encode($this->CI->encryption->encrypt($value['ID']));
        }
        foreach ($data['kill_data'] as $key => $value) {
            $data['kill_data'][$key]['ID']=base64_encode($this->CI->encryption->encrypt($value['ID']));
        }
        foreach ($data['inj_data'] as $key => $value) {
            $data['inj_data'][$key]['ID']=base64_encode($this->CI->encryption->encrypt($value['ID']));
        }
        return $data;
    }
	
	public function set_IO($accident_id,$io_id,$created_by) {
		
		$created_date=date('d/m/Y H:i:s');
		$MASTER_RECORD_ID=$this->CI->encryption->decrypt(base64_decode($accident_id));
		$data=array( 'ID' => get_new_id(),
						'USERID' => $io_id,
						'MASTER_RECORD_ID' => $MASTER_RECORD_ID,
						'CREATED_ON'=>"TO_DATE('$created_date','dd/mm/yyyy HH24:mi:ss')",
						'CREATED_BY'=> $created_by
				);
		return $this->CI->task_model->insert_io_assign($data);
		
	}

	public function get_fir_no($accident_id) {
		$data='';
		if(!empty($accident_id)) {
			$master_record_id= $this->CI->encryption->decrypt(base64_decode($accident_id));
			$data= $this->CI->common->get_data( 'ACC_REP',
						array('ACC_REP.MASTER_RECORD_ID as file_id','ACC_REP.FIR_NO as case_no'),
						array('ACC_REP.MASTER_RECORD_ID'=>$master_record_id),
						'','row_array'
					);
			$data['file_id']=base64_encode($this->CI->encryption->encrypt($data['file_id']));
		}
		return $data;
	}
	
    public function process_specific_accident_data($data){
        foreach($data as $key => $recordset) {
			// pre($key);
			// continue;
			switch ($key) {
				case "general_data":		
							//For General Data
							$general_column_array=array(
											'accident_type',
											'weather',
											'hit_and_run',
											'road_Type',
											'area_type',
											'accident_Spot',
											'road_name',
											'surface_Cond',
											'rd_feature',
											'speed_Limit',
											'rd_junction',
											'traffic_ctrl',
											'Lanes',
											'visibility',
											'phy_Divider',
											'roadWorks',
											'pedes_inv'
										);
							foreach ($general_column_array as $key => $value) {
								$data['general_data'][$value]=$this->CI->common_model->get_data('DROP_DOWN',array('VALUE'),array('ID'=>$data['general_data'][$value]),'','row_array')['VALUE'];    
							}
							break;
				case "inj_data":
							//For Injury Person Data
							$inj_column_array=array(
											//'inj_removed_hospital',
											//'inj_victim_condition',
										);
							foreach ($data['inj_data'] as $item_key => $item) {
								foreach ($inj_column_array as $key => $value) {
									$data['inj_data'][$item_key][$value]=$this->CI->common_model->get_data('DROP_DOWN',array('VALUE'),array('ID'=>$data['inj_data'][$item_key][$value]),'','row_array')['VALUE'];    
								}
							}
							break;
				case "kill_data":
							break;
				case "person_data":
							//For Person Data
							$person_column_array=array(
											'person_type',
											'person_impact_vehicle',
											'person_injury',
											'person_safety_device',
										);
							foreach ($data['person_data'] as $item_key => $item) {
								foreach ($person_column_array as $key => $value) {
									$data['person_data'][$item_key][$value]=$this->CI->common_model->get_data('DROP_DOWN',array('VALUE'),array('ID'=>$data['person_data'][$item_key][$value]),'','row_array')['VALUE'];    
								}
							}
							break;
				case "driver_vehicle_data":
							//For Driver Vehicle Data
							$driver_vehicle_column_array=array(
											//Driver
											'type_of_license',
											'impacting_vehicle',
											'type_of_injury',
											'traffic_violation',
											'driver_safety_device',
											//Vehicle
											'passenger_good_vehicle',
											'vehicle_type',
											'load_condition',
											'type_of_disposition',
											'mechanical_failure',
										);
							foreach ($data['driver_vehicle_data'] as $item_key => $item) {
								foreach ($driver_vehicle_column_array as $key => $value) {
									$data['driver_vehicle_data'][$item_key][$value]=$this->CI->common_model->get_data('DROP_DOWN',array('VALUE'),array('ID'=>$data['driver_vehicle_data'][$item_key][$value]),'','row_array')['VALUE'];    
								}
							}
							break;
			}
		}
        return $data;
    }
    
	public function delete_accident($accident_id_encrypted,$deleted_by){
		$MASTER_RECORD_ID=$this->CI->encryption->decrypt(base64_decode($accident_id_encrypted));
		$this->CI->db->trans_start();
		$this->CI->common->delete_data('ACC_REP','IS_DELETE','MASTER_RECORD_ID',$MASTER_RECORD_ID,$deleted_by);
		$this->CI->common->delete_data('ACC_REP_INJ','IS_DELETE','MASTER_RECORD_ID',$MASTER_RECORD_ID,$deleted_by);
		$this->CI->common->delete_data('ACC_REP_KILL','IS_DELETE','MASTER_RECORD_ID',$MASTER_RECORD_ID,$deleted_by);
		$this->CI->common->delete_data('DRIVER_VEHICLE_DETAIL','VD_IS_DELETE','MASTER_RECORD_ID',$MASTER_RECORD_ID,$deleted_by);
		$this->CI->common->delete_data('IMAGE_DETAIL','IS_DELETE','MASTER_RECORD_ID',$MASTER_RECORD_ID,$deleted_by);
		$this->CI->common->delete_data('PERSON_DETAIL','IS_DELETE','MASTER_RECORD_ID',$MASTER_RECORD_ID,$deleted_by);
		$this->CI->db->trans_complete();
        return $this->CI->db->trans_status();
	}
    private function get_status(){
        $flag=false;
        $status_complte_fields=array(
            'FIR_NO',
            'FIR_TIME',
            'GD_NO',
            'GD_DATE',
            'ACC_DATE',
            'ACC_TIME',
            'ACC_US_CD',
            'GRD_OFF_CD',
            'PS_CD',
            'ACC_PLACE',
            'LAT_DEG_MIN',
            'LONG_DEG_MIN',
            'NATURE_OF_ACCIDENT',
            'WETHER',
            'HIT_RUN',
            'COLLISION_TYPE',
            'ROAD_TYPE',
            'TYPE_OF_AREA',
            'ACC_SPOT',
            'ROAD_NM',
            'ROAD_CONDITION',
            'ROAD_FEATURES',
            'ROAD_RULE',
            'JUNCTION_TYPE',
            'JUNCTION_CONTROL',
            'LANES',
            'ROAD_CHAINGE',
            'VISIBILITY',
            'PHYSICAL_DIVIDER',
            'ONGOIG_RD_WRKS',
            'PEDESTRIAN_INVOLVED'
        );
        foreach ($status_complte_fields as $value) {
            if(is_null($Value)){
                $flag=true;
                break;
            }
        }
        if($flag==true){
            return '1';    
        }
        else{
            return '2';    
        }
    }
    public function get_person($person_id){
        $person_id=$this->CI->encryption->decrypt(base64_decode($person_id));
		$person= array();
		if(empty($person_id)) {
			return $person;
		}
        $person['person']=$this->CI->common_model->get_data(
                                array(
                                    'PERSON_DETAIL',
                                    array('DROP_DOWN','PERSON_DETAIL.TYPE_OF_INJURY=DROP_DOWN.ID','inner')
                                ),
                                array(
                                    'PERSON_DETAIL.ID person_id',
                                    'TYPE_OF_PERSON person_type',
                                    'PERSON_GENDER person_sex',
                                    'PERSON_AGE person_age',
                                    'IMPACTING_VEHICLE person_impact_vehicle',
                                    'TYPE_OF_INJURY person_injury',
                                    'SAFETY_DEVICE person_safety_device',
                                    'DROP_DOWN.VALUE drop_down_text'
                                ),
                                array(
                                    'IS_DELETE'=>'0',
                                    'PERSON_DETAIL.ID'=>$person_id
                                ),
                                '',
                                'row_array'
                            );
        if($person['person']['drop_down_text']==='Fatal'){
            $person['acc_rep_kill']=$this->CI->common_model->get_data(
                'ACC_REP_KILL',
                array(
                    'ID id',
                    'PS_CD ps_name',
                    'FIR_NO fir_no',
                    "TO_CHAR(FIR_DATE,'DD/MM/YYYY') fir_date",
                    'ACC_KIL_SEX kill_sex',
                    'ACC_KIL_AGE kill_age',
                    'ACC_KIL_REMOVED_HOSPITAL kill_removed_hospital',
                    "TO_CHAR(ACC_KIL_DIED_ON,'DD/MM/YYYY') kill_died_on",
                    'ACC_KIL_NAME kill_name',
                    'ACC_KIL_ADDRESS kill_address',
                    'PARENT_TABLE_ID p_id'
                ),
                array('ACC_REP_KILL.PARENT_TABLE_ID'=>$person['person']['person_id']),
                '',
                'row_array'
            );
        }
        else{
            $person['acc_rep_inj']=$this->CI->common_model->get_data(
                'ACC_REP_INJ',
                array(
                    'ID id',
                    'PS_CD ps_name',
                    'FIR_NO fir_no',
                    "TO_CHAR(FIR_DATE,'DD/MM/YYYY') fir_date",
                    'ACC_INJ_SEX inj_sex',
                    'ACC_INJ_AGE inj_age',
                    'ACC_INJ_REMOVED_HOSPITAL inj_removed_hospital',
                    'ACC_INJ_CONDITION_OF_VICTIM inj_victim_condition',
                    'ACC_INJ_NAME inj_name',
                    'ACC_INJ_ADDRESS inj_address',
                    'ACC_INJ_TYPE type',
                    'PARENT_TABLE_ID p_id'
                ),
                array('ACC_REP_INJ.PARENT_TABLE_ID'=>$person['person']['person_id']),
                '',
                'row_array'
            );   
        }
		// pre($person['acc_rep_kill']['id']);
		// exit();
        $person['person']['person_id']= isset($person['person']['person_id']) ? !empty($person['person']['person_id']) ? base64_encode($this->CI->encryption->encrypt($person['person']['person_id'])) : null : null;
        
        if(isset($person['acc_rep_inj'])){
			$person['acc_rep_inj']['id'] = base64_encode($this->CI->encryption->encrypt($person['acc_rep_inj']['id']));
			$person['acc_rep_inj']['p_id'] = $person['person']['person_id'];
        }
        if(isset($person['acc_rep_kill'])){                
			$person['acc_rep_kill']['id'] = base64_encode($this->CI->encryption->encrypt($person['acc_rep_kill']['id']));
            $person['acc_rep_kill']['p_id'] =$person['person']['person_id'];
        }
        
        unset($person['person']['drop_down_text']);
        return ($person);
    }
    private function get_all_person_refresh_data($MASTER_RECORD_ID){
        $data=array(
                    'person_data'=>$this->CI->common_model->get_data('PERSON_DETAIL'
                                                                ,array(
                                                                    'ID',
                                                                    'TYPE_OF_PERSON person_type',
                                                                    'PERSON_GENDER person_sex',
                                                                    'PERSON_AGE person_age',
                                                                    'IMPACTING_VEHICLE person_impact_vehicle',
                                                                    'TYPE_OF_INJURY person_injury',
                                                                    'SAFETY_DEVICE person_safety_device',
                                                                ),
                                                                array('MASTER_RECORD_ID'=>$MASTER_RECORD_ID,'IS_DELETE'=>'0'),
                                                                "",
                                                                "result_array"));
        foreach($data['person_data'] as $key=>$person){

            $data['person_data'][$key]['inj']=$this->CI->common_model->get_data('ACC_REP_INJ'
                                                                ,array(
                                                                    'ID',
                                                                    'PS_CD ps_name',
                                                                    'FIR_NO fir_no',
                                                                    "TO_CHAR(FIR_DATE,'DD/MM/YYYY') fir_date",
                                                                    'ACC_INJ_SEX inj_sex',
                                                                    'ACC_INJ_AGE inj_age',
                                                                    'ACC_INJ_REMOVED_HOSPITAL inj_removed_hospital',
                                                                    'ACC_INJ_CONDITION_OF_VICTIM inj_victim_condition',
                                                                    'ACC_INJ_NAME inj_name',
                                                                    'ACC_INJ_ADDRESS inj_address',
                                                                    'ACC_INJ_TYPE type',//Value=2/3
                                                                    'PARENT_TABLE_ID'
                                                                ),
                                                                array('PARENT_TABLE_ID'=>$person['ID'],'IS_DELETE'=>'0'),
                                                                "",
                                                                "row_array");
            $data['person_data'][$key]['kill']=$this->CI->common_model->get_data('ACC_REP_KILL'
                                                                ,array(
                                                                    'ID',
                                                                    'PS_CD ps_name',
                                                                    'FIR_NO fir_no',
                                                                    "TO_CHAR(FIR_DATE,'DD/MM/YYYY') fir_date",
                                                                    'ACC_KIL_SEX kill_sex',
                                                                    'ACC_KIL_AGE kill_age',
                                                                    'ACC_KIL_REMOVED_HOSPITAL kill_removed_hospital',
                                                                    "TO_CHAR(ACC_KIL_DIED_ON,'DD/MM/YYYY') kill_died_on",
                                                                    'ACC_KIL_NAME kill_name',
                                                                    'ACC_KIL_ADDRESS kill_address',
                                                                    'PARENT_TABLE_ID'
                                                                ),
                                                                array('PARENT_TABLE_ID'=>$person['ID'],'IS_DELETE'=>'0'),
                                                                "",
                                                                "row_array");
        }
        foreach ($data['person_data'] as $key => $value) {
            $data['person_data'][$key]['ID']=base64_encode($this->CI->encryption->encrypt($data['person_data'][$key]['ID']));
            if( !empty($value['kill']) ) {
				$data['person_data'][$key]['kill']['ID']=base64_encode($this->CI->encryption->encrypt($data['person_data'][$key]['kill']['ID']));
				$data['person_data'][$key]['kill']['PARENT_TABLE_ID']=base64_encode($this->CI->encryption->encrypt($data['person_data'][$key]['kill']['PARENT_TABLE_ID']));
			}
            if( !empty($value['inj']) ) {
				$data['person_data'][$key]['inj']['ID']= base64_encode($this->CI->encryption->encrypt($data['person_data'][$key]['inj']['ID']));
				$data['person_data'][$key]['inj']['PARENT_TABLE_ID']= base64_encode($this->CI->encryption->encrypt($data['person_data'][$key]['inj']['PARENT_TABLE_ID']));
			}
        }
        return $data;
    }
    public function delete_person_with_refresh($person_id,$master_record_id,$USER_ID){
        $all_person=array();
		
//        return ($all_person['status']='done');
		
        if(!empty($person_id)){
            $status=$this->CI->task_model->delete_person($person_id,$master_record_id,$USER_ID);
            $all_person=$this->get_all_person_refresh_data($master_record_id);
        }
        return $all_person;
    }
    public function update_person_with_refresh($person_data,$kill_data,$inj_data,$master_record_id,$USER_ID){
        $all_person=array();
        $kill=array();
        $inj=array();
        $master_record_id= $this->CI->encryption->decrypt(base64_decode($master_record_id));
        if(count($person_data)>0){
             $person=$this->process_person_data($person_data,$master_record_id,$USER_ID);
             if(count($kill_data)>0){
                $kill=$this->process_kill_data($kill_data,$master_record_id,array(),$USER_ID,$person[0]['ID']);   
             }
             if(count($inj_data)>0){
                $inj=$this->process_inj_data($inj_data,$master_record_id,array(),$USER_ID,$person[0]['ID']);
             }
             $status=$this->CI->task_model->update_person_on_edit($person[0]['ID'],$person,$kill,$inj,$master_record_id,$USER_ID);
             $all_person=$this->get_all_person_refresh_data($master_record_id);
         }
         return $all_person;  
     }
    public function insert_person_with_refresh($person_data,$kill_data,$inj_data,$master_record_id,$USER_ID){
        $all_person=array();
        $kill=array();
        $inj=array();
        $master_record_id= $this->CI->encryption->decrypt(base64_decode($master_record_id));
        if(count($person_data)){
            $person=$this->process_person_data($person_data,$master_record_id,$USER_ID);
            if(count($kill_data)>0){
				$accident_data=$this->CI->common_model->get_data(
									'ACC_REP',
									array("PS_CD","FIR_NO","TO_CHAR(FIR_DATE,'DD/MM/YYYY') AS FIR_DATE"),
									array('MASTER_RECORD_ID'=>$master_record_id),
									'','row_array');
				$general=array(
					'PS_CD'=>$accident_data['PS_CD'],
					'FIR_NO'=>$accident_data['FIR_NO'],
					'FIR_DATE'=>$accident_data['FIR_DATE']
				);
                $kill=$this->process_kill_data($kill_data,$master_record_id,$general,$USER_ID,$person[0]['ID']);
				
            }
            if(count($inj_data)>0){
				$accident_data=$this->CI->common_model->get_data(
									'ACC_REP',
									array("PS_CD","FIR_NO","TO_CHAR(FIR_DATE,'DD/MM/YYYY') AS FIR_DATE"),
									array('MASTER_RECORD_ID'=>$master_record_id),
									'','row_array');
				$general=array(
					'PS_CD'=>$accident_data['PS_CD'],
					'FIR_NO'=>$accident_data['FIR_NO'],
					'FIR_DATE'=>$accident_data['FIR_DATE']
				);
                $inj=$this->process_inj_data($inj_data,$master_record_id,$general,$USER_ID,$person[0]['ID']);
            }
            $status=$this->CI->task_model->insert_person_on_edit($person[0]['ID'],$person,$kill,$inj,$master_record_id,$USER_ID);
            $all_person=$this->get_all_person_refresh_data($master_record_id);
        }
        return $all_person;
    }
    public function driver_vehicle_operation_with_refresh($driver_vechicle,$master_record_id,$USER_ID,$mode){
        $all_driver_vehicle=array();
        $master_record_id= $this->CI->encryption->decrypt(base64_decode($master_record_id));
        if(count($driver_vechicle)>0){
            $driver_vechicle=$this->process_driver_vehicle_data($driver_vechicle,$master_record_id,$USER_ID);
            switch ($mode) {
                case 'insert':
                    $status=$this->CI->task_model->insert_driver_vehicle($driver_vechicle[0]);
                    break;
                case 'update':
					//pre($driver_vechicle); exit(); 
                    $status=$this->CI->task_model->update_driver_vehicle($driver_vechicle[0],$master_record_id,$USER_ID);
                    break;
                case 'delete':
		//			pre($driver_vechicle); exit(); 
                    $status=$this->CI->task_model->delete_driver_vehicle($driver_vechicle[0]['ID'],$master_record_id,$USER_ID);
                    break;
                default:
                    # code...
                    break;
            }
            $all_driver_vehicle['driver_vehicle_data']=$this->get_all_driver_vehicle_refresh_data($master_record_id);
        }
        return $all_driver_vehicle;
    }
    private function get_all_driver_vehicle_refresh_data($master_record_id, $driver_vechicle_id=''){
        $where_cond=  array(
								'MASTER_RECORD_ID'=>$master_record_id,
								'VD_IS_DELETE'=>'0'
							);
		if(!empty($driver_vechicle_id)) {
			$where_cond['ID']= $driver_vechicle_id;
		}
		$data=$this->CI->common_model->get_data(
            'DRIVER_VEHICLE_DETAIL',
            array(
				'ID',
				//Driver
				'D_LICENSE_NO license_no',
				'D_TYPE_OF_LICENSE type_of_license',
				'D_DRIVER_GENDER driver_sex',
				'D_DRIVER_AGE driver_age',
				'D_IMPACTING_VEHICLE impacting_vehicle',
				'D_TYPE_OF_INJURY type_of_injury',
				'D_TYPE_OF_TRAFFIC_VIOLATION traffic_violation',
				'D_SAFETY_DEVICE driver_safety_device',
				//Vehicle
				'V_REGISTRATION_NO reg_No',
				'V_PASSENGER_GOOD_VEHICLE passenger_good_vehicle',
				'V_VEHICLE_TYPE vehicle_type',
				'V_LOAD_CONDITION load_condition',
				'V_TYPE_OF_DISPOSITION type_of_disposition',
				'V_MECHANIC_FAILURE mechanical_failure',
				'V_AGE_OF_VEHICLE age_of_vehicle'
			), 
			$where_cond, '', 'result_array'
		);
        foreach ($data as $key => $value) {
            $data[$key]['ID'] = base64_encode($this->CI->encryption->encrypt($data[$key]['ID']));
        }
		unset($where_cond);
        return $data;
    }
	public function get_driver_vehicle($master_record_id, $id) {
        $all_driver_vehicle= array();
        $id= $this->CI->encryption->decrypt(base64_decode($id));
        $master_record_id= $this->CI->encryption->decrypt(base64_decode($master_record_id));
		
		$all_driver_vehicle= $this->CI->task->get_all_driver_vehicle_refresh_data($master_record_id,$id);
		return $all_driver_vehicle;
	}

    public function loadSavedImage($master_record_id_encrypted){
         $master_record_id= $this->CI->encryption->decrypt(base64_decode($master_record_id_encrypted));
         //pre($master_record_id);exit();
         $serverData = $this->CI->common_model->get_data('IMAGE_DETAIL','',array('MASTER_RECORD_ID'=>$master_record_id,'IS_DELETE' => 0),'','result_array');
         $imageURL = array();
         $config = array();
         foreach ($serverData as $key => $value) {
             array_push($imageURL, $value['PATH']);
             array_push($config, array(
                    'key' => base64_encode($this->CI->encryption->encrypt($value['ID'])),
                    'caption'=>$value['PATH'],
                    'size' => 'size',
                    'downloadUrl' => $value['PATH'],
                    'url' => 'deleteUrl',
                    'extra' => array(
                            'fileName' => $value['PATH'],
                            'masterFileID' => base64_encode($this->CI->encryption->encrypt($master_record_id)),
                        )
                ));
         }
         $imageData = array(
            'preview' => $imageURL,
            'config' => $config
            );
        return $imageData;
    }

}
