<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class Utility  {
	
	private $CI;

	public function __construct(){
		$this->CI =& get_instance();
	}
	//Image Upload Custom Function
	/*
    input:FILES
    output:array(
                'initialPreview':image array
                initialPreviewConfig:config array
            );
    */
    public function upload_files($prefix="upload",$path,$files){
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|gif|png',
            'overwrite'     => 1,      
            'max_size' => 200000,
            'max_width' => 1500,
            'max_height' => 1500                 
        );
        //$CI =& get_instance();
        $this->CI->load->library('upload', $config);
        $images = array();
        $image_config = array();
        $newFileName = array();
        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = $prefix.'_'. $image;

            $images[] = $fileName;

            $config['file_name'] = $fileName;

            $this->CI->upload->initialize($config);
            if ($this->CI->upload->do_upload('images[]')) {
                $data_after_upload=$this->CI->upload->data();
            } else {
                return false;
            }
            $caption = $data_after_upload['file_name']; // the file name
	        $fileSize = $data_after_upload['file_size']; // the file size
	        $downloadUrl = base_url().'assets/upload/'.$data_after_upload['file_name']; //download url
	        $config_item = array(
	            'key' => $fileName.$key,
	            'caption' => $caption,
	            'size' => $fileSize,
	            //'downloadUrl' => $downloadUrl, // the url to download the file
				//'url' => base_url() . 'insert_Data/deleteImage', // server api to delete the file based on key
                /*
				'extra' => array( 
							'fileName' => 'Untitled2.png', 
							'masterFileID' => 384 ),
                            */
	        );
	        array_push($image_config,$config_item);
	        array_push($newFileName,$downloadUrl);
        }
        
        return array('config'=> $image_config,'filename' => $newFileName);
    }
    //End Image Upload Custom Function
    
}
