<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function updateAPItoken($gmap_key)
    {
		$data=array(
			'KEY'=>$gmap_key
		);
        $this->db->where('VENDOR', "Google Map");
        $this->db->update('TBL_APIKEY', $data);
        return $this->db->affected_rows();
    }
	
	function set_permission_id($uid, $data)
    {
        $this->db->where('USERID', $uid);
        $this->db->update('TBL_USER_PERMISSION', $data);
        return $this->db->affected_rows();
    }
	
	function set_rolename($data)
    {
		$this->db->insert('TBL_ROLES', $data);
        return $this->db->affected_rows();
    }

    function save_ControllerMethod($data){
		foreach ($data as $key => $value) {
            $exclude_key_array=array('UPDATED_ON','UPDATED_BY');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="CREATED_ON"){
                if($value==="TO_DATE('','dd/mm/yyyy')"||$value===null||$value===""){
                    $this->db->set($key,null);    
                }
                else{
                    $this->db->set($key,$value,false);        
                }
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }  
            }
        }
        $this->db->insert('TBL_GROUP_PERMISSION');
		
        return $this->db->affected_rows();
    }
}

  