<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Authorization_model extends CI_Model{
    
    /*public function insertToken($user_id,$EncryptedToken){
        $created_timestamp=date('d/m/Y H:i:s');
        $this->db->set('TOKEN', $EncryptedToken);
        $this->db->set('TOKEN_TIMESTAMP', "TO_DATE('$created_timestamp','dd/mm/yyyy HH24:mi:ss')",false);
        $this->db->set('USERID', $user_id);
        $this->db->insert('TBL_TOKEN_HASH');
    }*/
    public function getValidatedTimeStamp($token,$user_id){
        $this->load->model('Common_model','common_model');
        $data=$this->common_model->get_data('TBL_TOKEN_HASH',array("TO_CHAR(TOKEN_TIMESTAMP, 'mm/dd/yyyy HH24:mi:ss') TOKEN_TIMESTAMP"),array('TOKEN'=>$token,'USERID'=>$user_id),'','row_array');
        return isset($data['TOKEN_TIMESTAMP'])? $data['TOKEN_TIMESTAMP'] : null;
    }
    public function GetRefreshToken($userid){
        $this->load->library('encryption');
        $this->load->model('Common_model','common_model');
        $data=$this->common_model->get_data('TBL_USERS',array("USERID","EMAIL","PASSWORD","REFRESH_TOKEN","TO_CHAR(REFRESH_TOKEN_TIMESTAMP, 'mm/dd/yyyy HH24:mi:ss') REFRESH_TOKEN_TIMESTAMP"),array('USERID'=>$userid),'','row_array');
        
        $current_timestamp=time();
        $token_timestamp=strtotime($data['REFRESH_TOKEN_TIMESTAMP']);
        if (($current_timestamp-$token_timestamp) < ($this->config->item('refresh_token_timeout') * 24 * 60 * 60)) { //Not Expired
            return $data['REFRESH_TOKEN'];
        }
        else{ //Expired
            $UpdatedString=$data['EMAIL']."&".$data['PASSWORD'];
            $UpdatedToken=sha1($data['USERID']);
            //Update it in table with timestamp
            $this->updateRefreshToken($UpdatedToken,$data['USERID']);

        }
        return isset($data['TOKEN_TIMESTAMP'])? $data['TOKEN_TIMESTAMP'] : null;
    }
    private function updateRefreshToken($UpdatedToken,$user_id){
        $this->db->query("ALTER TRIGGER TBL_USERS_USERID_TRIG DISABLE");
        $created_timestamp=date('d/m/Y H:i:s');
        $this->db->set('REFRESH_TOKEN', $UpdatedToken);
        $this->db->set('REFRESH_TOKEN_TIMESTAMP', "TO_DATE('$created_timestamp','dd/mm/yyyy HH24:mi:ss')",false);
        $this->db->where('USERID', $user_id);
        $this->db->update('TBL_USERS');
        $this->db->query("ALTER TRIGGER TBL_USERS_USERID_TRIG ENABLE");
    }
    public function validateRefreshToken($RefreshTokenText){
        $this->load->model('Common_model','common_model');
        $data=$this->common_model->get_data('TBL_USERS',array("TO_CHAR(REFRESH_TOKEN_TIMESTAMP, 'mm/dd/yyyy HH24:mi:ss') REFRESH_TOKEN_TIMESTAMP"),array('REFRESH_TOKEN'=>$RefreshTokenText),'','row_array');

        $current_timestamp=time();
        $token_timestamp=strtotime($data['REFRESH_TOKEN_TIMESTAMP']);
        if (($current_timestamp-$token_timestamp) < ($this->config->item('refresh_token_timeout') * 24 * 60 * 60)) { //Not Expired
            return true;
        }
        else{ //Expired
            return false;
        }
    }
    
}

?>
