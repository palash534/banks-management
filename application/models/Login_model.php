<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $password)
    {
        $this->db->select('TBL_USERS.USERID, TBL_USERS.PASSWORD, TBL_USERS.NAME, TBL_USERS.ROLEID, TBL_ROLES.ROLE');
        $this->db->from('tbl_users TBL_USERS');
        $this->db->join('tbl_roles TBL_ROLES','TBL_ROLES.ROLEID = TBL_USERS.ROLEID');
        $this->db->where('TBL_USERS.EMAIL', $email);
        $this->db->where('TBL_USERS.ISDELETED', 0);
        $query = $this->db->get();
        $user = $query->result();
        
        if(!empty($user)){
            if(verifyHashedPassword($password, $user[0]->PASSWORD)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /**
     * This function used to check email exists or not
     * @param {string} $email : This is users email id
     * @return {boolean} $result : TRUE/FALSE
     */
    function checkEmailExist($email)
    {
        $this->db->select('USERID');
        $this->db->where('EMAIL', $email);
        $this->db->where('ISDELETED', 0);
        $query = $this->db->get('TBL_USERS');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }


    /**
     * This function used to insert reset password data
     * @param {array} $data : This is reset password data
     * @return {boolean} $result : TRUE/FALSE
     */
    function resetPasswordUser($data)
    {
        $result = $this->db->insert('TBL_RESET_PASSWORD', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This function is used to get customer information by email-id for forget password email
     * @param string $email : Email id of customer
     * @return object $result : Information of customer
     */
    function getCustomerInfoByEmail($email)
    {
        $this->db->select('USERID, EMAIL, NAME');
        $this->db->from('TBL_USERS');
        $this->db->where('ISDELETED', 0);
        $this->db->where('EMAIL', $email);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function used to check correct activation deatails for forget password.
     * @param string $email : Email id of user
     * @param string $activation_id : This is activation string
     */
    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('ID');
        $this->db->from('TBL_RESET_PASSWORD');
        $this->db->where('EMAIL', $email);
        $this->db->where('ACTIVATION_ID', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    // This function used to create new password by reset link
    function createPasswordUser($email, $password)
    {
        $this->db->where('EMAIL', $email);
        $this->db->where('ISDELETED', 0);
        $this->db->update('TBL_USERS', array('PASSWORD'=>getHashedPassword($password)));
        $this->db->delete('TBL_RESET_PASSWORD', array('EMAIL'=>$email));
    }
}

?>
