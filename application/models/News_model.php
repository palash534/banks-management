<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('Common_model','common_model');
    }
    public function record_count(){
        return $this->common->get_data(
            'admin_news',
            '',
            array('IS_DELETE'=>0),
            '',
            'num_rows'
        );
    }
    // Fetch data according to per_page limit.
    public function fetch_data($limit, $id) {
        $record_count=$this->common->get_data(
            'admin_news',
            '',
            array('IS_DELETE'=>0),
            '',
            'num_rows'
        );
        if($record_count>0){
            $data=$this->common->get_data(
                'admin_news',
                '',
                array('IS_DELETE'=>0),
                '',
                'result_array',
                '',
                array(
                    array('column'=>'NEWS_DATE','mode'=>'DESC')
                ),
                '','', $limit ,$id==1?'0':(($id-1)*$limit)
            );
            //pre($this->db->last_query());exit();
            return $data;       
        }
        else return false;
    }
    public function update_data($data,$id){
        $this->db->where(array('ID'=>$id));
        $this->db->update('admin_news',$data);
        return $this->db->affected_rows();
    }
    
}

?>
