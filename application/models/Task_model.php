<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Task_model extends CI_Model{
    public function __construct(){
        $this->load->model('Common_model','common_model');
        $this->load->library('MY_Log','my_log');
    }
    public function save_general_image($general_data,$image_data){
        $this->db->trans_start();
            //Insert General Data
            if(isset($general_data)){
                $this->insert_general($general_data);
            }
            //Insert Image Data
            if(isset($image_data) && is_array($image_data)){
                foreach ($image_data as $key => $image_data_item) {
                    $this->insert_image($image_data_item);    
                }
            }
        $this->db->trans_complete();
        return $this->db->trans_status();
    }
    public function save_task($general_data,$driver_vehicle_data,$person_data,$image_data,$kill_data,$inj_data){
        $this->db->trans_start();
            //Insert General Data
            if(isset($general_data)){
                $this->insert_general($general_data);
            }
            //Insert Driver Vehicle Data
            if(isset($driver_vehicle_data) && is_array($driver_vehicle_data)){
                foreach ($driver_vehicle_data as $key => $driver_vehicle_item) {
                    $this->insert_driver_vehicle($driver_vehicle_item);    
                }
            }
            //Insert Person Data
            if(isset($person_data) && is_array($person_data)){
                foreach ($person_data as $key => $person_data_item) {
                    $this->insert_person($person_data_item);  
                }
            }
            //Insert Image Data
            if(isset($image_data) && is_array($image_data)){
                foreach ($image_data as $key => $image_data_item) {
                    $this->insert_image($image_data_item);    
                }
            }
            //Insert Kill Data 
            if(isset($kill_data) && is_array($kill_data)){
                foreach ($kill_data as $key => $kill_data_item) {
                    $this->insert_kill($kill_data_item);
                }
            }
            //Insert Injury Data
            if(isset($inj_data) && is_array($inj_data)){
                foreach ($inj_data as $key => $inj_data_item) {
                    $this->insert_inj($inj_data_item);
                }
            }
        $this->db->trans_complete();
        return $this->db->trans_status();
    }
	public function insert_io_assign($data) {
        if(isset($data)){
            foreach ($data as $key => $value) {
                if($key==="CREATED_ON"){
                    if($value==="TO_DATE('','dd/mm/yyyy')"||$value===null||$value===""){
                        $this->db->set($key,null);    
                    }
                    else{
                        $this->db->set($key,$value,false);        
                    }
                }else{
                    if($value===""){
                        $this->db->set($key,null);
                    }
                    else{
                        $this->db->set($key,$value);
                    }  
                }
            }
            return $this->db->insert('TBL_IO');
        }
	}
    public function update_task($general_data,$driver_vehicle_data,$person_data,$image_data,$kill_data,$inj_data,$deleted_driver_vehicle_data,$deleted_person_data,$deleted_image_data,$deleted_kill_data,$deleted_inj_data,$USER_ID){
        $this->db->trans_start();
            //Update General Data
            if(isset($general_data)){
                $this->update_general($general_data);
            }
            //Update Driver Vehicle Data
            if(isset($driver_vehicle_data) && is_array($driver_vehicle_data)){
                foreach ($driver_vehicle_data as $key => $driver_vehicle_item) {
                    switch($driver_vehicle_item['MODE_TAG']){
                        case 'save':
                            unset($driver_vehicle_item['MODE_TAG']);
                            $this->insert_driver_vehicle($driver_vehicle_item);
                            break;
                        case 'update':
                            unset($driver_vehicle_item['MODE_TAG']);
                            $this->update_driver_vehicle($driver_vehicle_item);
                            break;
                        /*case 'delete':
                            unset($driver_vehicle_item['MODE_TAG']);
                            $this->delete_driver_vehicle($driver_vehicle_item);
                            break;*/
                    }
                }
            }
            //Update Person Data
            if(isset($person_data) && is_array($person_data)){
                foreach ($person_data as $key => $person_data_item) {
                    switch($person_data_item['MODE_TAG']){
                        case 'save':
                            unset($person_data_item['MODE_TAG']);
                            $this->insert_person($person_data_item);
                            break;
                        case 'update':
                            unset($person_data_item['MODE_TAG']);
                            $this->update_person($person_data_item);
                            break;
                        /*case 'delete':
                            unset($person_data_item['MODE_TAG']);
                            $this->delete_person($person_data_item);
                            break;*/
                    }  
                }
            }
            //Update Image Data
            if(isset($image_data) && is_array($image_data)){
                foreach ($image_data as $key => $image_data_item) {
                    switch($image_data_item['MODE_TAG']){
                        case 'save':
                            unset($image_data_item['MODE_TAG']);
                            $this->insert_image($image_data_item);
                            break;
                        case 'update':
                            unset($image_data_item['MODE_TAG']);
                            $this->update_image($image_data_item);
                            break;
                       /* case 'delete':
                            unset($image_data_item['MODE_TAG']);
                            $this->delete_image($image_data_item);
                            break;*/
                    } 
                }
            }
            //Update Kill Data
            if(isset($kill_data) && is_array($kill_data)){
                foreach ($kill_data as $key => $kill_data_item) {
                    switch($kill_data_item['MODE_TAG']){
                        case 'save':
                            unset($kill_data_item['MODE_TAG']);
                            $this->insert_kill($kill_data_item);
                            break;
                        case 'update':
                            unset($kill_data_item['MODE_TAG']);
                            $this->update_kill($kill_data_item);
                            break;
                        /*case 'delete':
                            unset($kill_data_item['MODE_TAG']);
                            $this->delete_kill($kill_data_item);
                            break;*/
                    }
                }
            }
            //Update Injury Data
            if(isset($inj_data) && is_array($inj_data)){
                foreach ($inj_data as $key => $inj_data_item) {
                    switch($inj_data_item['MODE_TAG']){
                        case 'save':
                            unset($inj_data_item['MODE_TAG']);
                            $this->insert_inj($inj_data_item);
                            break;
                        case 'update':
                            unset($inj_data_item['MODE_TAG']);
                            $this->update_inj($inj_data_item);
                            break;
                        /*case 'delete':
                            unset($inj_data_item['MODE_TAG']);
                            $this->delete_inj($inj_data_item);
                            break;*/
                    }
                }
            }
            $this->delete_on_update($deleted_driver_vehicle_data,$deleted_person_data,$deleted_image_data,$deleted_kill_data,$deleted_inj_data,$USER_ID);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }
    private function insert_general($general_data){
        foreach ($general_data as $key => $value) {
            $exclude_key_array=array('ID','UPDATED_ON','UPDATED_BY');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="FIR_DATE" || $key==="FIR_TIME" || $key==="CREATED_ON" || $key==="GD_DATE" || $key==="GD_TIME" || $key==="ACC_DATE"){
                if($value==="TO_DATE('','dd/mm/yyyy')"||$value===null||$value===""){
                    $this->db->set($key,null);    
                }
                else{
                    $this->db->set($key,$value,false);        
                }
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }  
            }
        }
        $this->db->insert('ACC_REP');
    }
    public function insert_driver_vehicle($driver_vehicle_item){
        foreach ($driver_vehicle_item as $key => $value) {
                $exclude_key_array=array('MODE_TAG','VD_UPDATED_ON','VD_UPDATED_BY');
                if(in_array($key, $exclude_key_array)) continue;
                if($key==="VD_CREATED_ON"){
                    $this->db->set($key,$value,false);    
                }else{
                    if($value===""){
                        $this->db->set($key,null);
                    }
                    else{
                        $this->db->set($key,$value);
                    }     
                }
            }
            $this->db->insert('DRIVER_VEHICLE_DETAIL');
    }
    private function insert_person($person_data_item){
        foreach ($person_data_item as $key => $value) {
            $exclude_key_array=array('MODE_TAG','UPDATED_ON','UPDATED_BY');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="CREATED_ON"){
                $this->db->set($key,$value,false);    
            }else{
                if($value===""){
                $this->db->set($key,null);
            }
            else{
                $this->db->set($key,$value);
            }     
            }
        }
        $this->db->insert('PERSON_DETAIL'); 
    }
    public function insert_image($image_data_item){
        foreach ($image_data_item as $key => $value) {
            $exclude_key_array=array('MODE_TAG','UPDATED_ON','UPDATED_BY');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="CREATED_ON"){
                $this->db->set($key,$value,false);    
            }else{
                if($value===""){
                $this->db->set($key,null);
            }
            else{
                $this->db->set($key,$value);
            }     
            }
        }
        $this->db->insert('IMAGE_DETAIL');    
    }
    private function insert_kill($kill_data_item){
        foreach ($kill_data_item as $key => $value) {
            $exclude_key_array=array('MODE_TAG','UPDATED_ON','UPDATED_BY');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="FIR_DATE" || $key==="CREATED_ON" || $key==="UPDATED_ON" || $key==="ACC_KIL_DIED_ON"){
                if($value==="TO_DATE('','dd/mm/yyyy')"||$value===null||$value===""){
                    $this->db->set($key,null);    
                }
                else{
                    $this->db->set($key,$value,false);        
                } 
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }   
            }
        }
        $this->db->insert('ACC_REP_KILL');
    }
    private function insert_inj($inj_data_item){
        foreach ($inj_data_item as $key => $value) {
            $exclude_key_array=array('MODE_TAG','UPDATED_ON','UPDATED_BY');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="FIR_DATE" || $key==="CREATED_ON" || $key==="UPDATED_ON" ){
                if($value==="TO_DATE('','dd/mm/yyyy')"||$value===null||$value===""){
                    $this->db->set($key,null);    
                }
                else{
                    $this->db->set($key,$value,false);        
                }    
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }     
            }
        }
        $this->db->insert('ACC_REP_INJ');
    }
    private function update_general($general_data){
        foreach ($general_data as $key => $value) {
            $exclude_key_array=array('MASTER_RECORD_ID','CREATED_ON','CREATED_BY');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="FIR_DATE" || $key==="FIR_TIME" || $key==="UPDATED_ON" || $key==="GD_DATE" || $key==="GD_TIME" || $key==="ACC_DATE"){
                if($value==="TO_DATE('','dd/mm/yyyy')"||$value===null||$value===""){
                    $this->db->set($key,null);    
                }
                else{
                    $this->db->set($key,$value,false);        
                }
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }  
            }
        }
        $this->db->where('ACC_REP.MASTER_RECORD_ID',$general_data['MASTER_RECORD_ID']);
        $this->db->update('ACC_REP');
    }
    public function update_driver_vehicle($driver_vehicle_item,$master_record_id,$updated_by){
        foreach ($driver_vehicle_item as $key => $value) {
            $exclude_key_array=array('ID','VD_CREATED_ON','VD_CREATED_BY','MODE_TAG');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="VD_UPDATED_ON"){
                $this->db->set($key,$value,false);    
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }     
            }
        }
        $this->my_log->write_start($master_record_id,$updated_by);
            $this->db->where('DRIVER_VEHICLE_DETAIL.ID',$driver_vehicle_item['ID']);
            $this->db->update('DRIVER_VEHICLE_DETAIL');
        $this->my_log->write_end($master_record_id,$updated_by);
    }
    private function update_person($person_data_item){
        foreach ($person_data_item as $key => $value) {
            $exclude_key_array=array('ID','CREATED_ON','CREATED_BY','MODE_TAG');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="UPDATED_ON"){
                $this->db->set($key,$value,false);    
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }     
            }
        }
        $this->db->where('PERSON_DETAIL.ID',$person_data_item['ID']);
        $this->db->update('PERSON_DETAIL');
    }
    private function update_image($image_data_item){
        foreach ($image_data_item as $key => $value) {
            $exclude_key_array=array('ID','CREATED_ON','CREATED_BY','MODE_TAG');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="UPDATED_ON"){
                $this->db->set($key,$value,false);    
            }else{
                if($value===""){
                $this->db->set($key,null);
            }
            else{
                $this->db->set($key,$value);
            }     
            }
        }
        $this->db->where('IMAGE_DETAIL.ID',$image_data_item['ID']);
        $this->db->update('IMAGE_DETAIL'); 
    }
    private function update_kill($kill_data_item){
        foreach ($kill_data_item as $key => $value) {
            $exclude_key_array=array('ID','CREATED_ON','CREATED_BY','MODE_TAG');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="FIR_DATE" || $key==="UPDATED_ON" || $key==="ACC_KIL_DIED_ON"){
                if($value==="TO_DATE('','dd/mm/yyyy')"||$value===null||$value===""){
                    $this->db->set($key,null);    
                }
                else{
                    $this->db->set($key,$value,false);        
                } 
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }   
            }
        }
        $this->db->where('ACC_REP_KILL.ID',$kill_data_item['ID']);
        $this->db->update('ACC_REP_KILL');
    }
    private function update_inj($inj_data_item){
        foreach ($inj_data_item as $key => $value) {
            $exclude_key_array=array('ID','CREATED_ON','CREATED_BY','MODE_TAG');
            if(in_array($key, $exclude_key_array)) continue;
            if($key==="FIR_DATE" || $key==="UPDATED_ON"){
                if($value==="TO_DATE('','dd/mm/yyyy')"||$value===null||$value===""){
                    $this->db->set($key,null);    
                }
                else{
                    $this->db->set($key,$value,false);        
                }    
            }else{
                if($value===""){
                    $this->db->set($key,null);
                }
                else{
                    $this->db->set($key,$value);
                }     
            }
        }
        $this->db->where('ACC_REP_INJ.ID',$inj_data_item['ID']);
        $this->db->update('ACC_REP_INJ');
    }
    private function delete_on_update($deleted_driver_vehicle_data,$deleted_person_data,$deleted_image_data,$deleted_kill_data,$deleted_inj_data,$deleted_by){
        foreach($deleted_driver_vehicle_data as $value){
            $this->common_model->delete_data('DRIVER_VEHICLE_DETAIL','IS_DELETE','ID',$value['ID'],$deleted_by);    
        }
        foreach($deleted_person_data as $value){
            $this->common_model->delete_data('PERSON_DETAIL','IS_DELETE','ID',$value['ID'],$deleted_by);    
        }
        foreach($deleted_image_data as $value){
            $this->common_model->delete_data('IMAGE_DETAIL','IS_DELETE','ID',$value['ID'],$deleted_by);    
        }
        foreach($deleted_kill_data as $value){
            $this->common_model->delete_data('ACC_REP_KILL','IS_DELETE','ID',$value['ID'],$deleted_by);    
        }
        foreach($deleted_inj_data as $value){
            $this->common_model->delete_data('ACC_REP_INJ','IS_DELETE','ID',$value['ID'],$deleted_by);    
        }
    }
    public function delete_person($person_id,$master_record_id,$deleted_by){
        $this->my_log->write_start($master_record_id,$deleted_by);
        $this->db->trans_start();
            $this->common_model->delete_data_complete('ACC_REP_INJ','PARENT_TABLE_ID',$person_id);
            $this->common_model->delete_data_complete('ACC_REP_KILL','PARENT_TABLE_ID',$person_id);
            $this->common_model->delete_data_complete('PERSON_DETAIL','ID',$person_id); 
        $this->db->trans_complete();
        $this->my_log->write_end($master_record_id,$deleted_by);
        return $this->db->trans_status();
    }
    public function update_person_on_edit($person_id,$person,$kill,$inj,$master_record_id,$updated_by){
        $this->my_log->write_start($master_record_id,$updated_by);
        $this->db->trans_start();
            $this->common_model->delete_data_complete('ACC_REP_INJ','PARENT_TABLE_ID',$person_id);
            $this->common_model->delete_data_complete('ACC_REP_KILL','PARENT_TABLE_ID',$person_id);
            $this->update_person($person[0]);
			//pre($inj);pre($kill); exit();
			if(count($kill)>0){
				$this->insert_kill($kill[0]);
			}
			if(count($inj)>0){
				$this->insert_inj($inj[0]);
			}
        $this->db->trans_complete();
        $this->my_log->write_end($master_record_id,$updated_by);
        return $this->db->trans_status();
    }
    public function insert_person_on_edit($person_id,$person,$kill,$inj,$master_record_id,$updated_by){
        $this->db->trans_start();
            $this->insert_person($person[0]);
			if(count($kill)>0){
				$this->insert_kill($kill[0]);
			}
			if(count($inj)>0){
				$this->insert_inj($inj[0]);
			}
        $this->db->trans_complete();
        return $this->db->trans_status();
    }
    public function delete_driver_vehicle($driver_vehicle_id,$master_record_id,$deleted_by){
        $this->my_log->write_start($master_record_id,$deleted_by);
		
        $updated_date=date('d/m/Y H:i:s');
		$this->db->set('VD_IS_DELETE','1');
        $this->db->set('VD_UPDATED_BY',$deleted_by);
        $this->db->set('VD_UPDATED_ON',"TO_DATE('$updated_date','dd/mm/yyyy HH24:mi:ss')",false);
		$this->db->where('ID',$driver_vehicle_id);
		$status= $this->db->update('DRIVER_VEHICLE_DETAIL');
		
        $this->my_log->write_end($master_record_id,$deleted_by);
        return $status;
    }
}

?>
