<?php

class test_model extends CI_Model {
	
	private function get_order_list_query($column,$table,$search_value,$column_search,$from_date,$to_date){
		$this->db->select($column)->from($table);
		
		if(!empty($from_date) && !empty($to_date)){
			$this->db->where("LOGIN_TIME BETWEEN to_timestamp('" . date('Y-m-d',strtotime($from_date)) . "', 'yyyy-mm-dd') AND to_timestamp('" . date('Y-m-d',strtotime($to_date)) . "', 'yyyy-mm-dd')");
        }
        $this->db->group_start();
		if (!empty($search_value)) {
			foreach ($column_search as $key => $item) {
				$this->db->or_like($item,$search_value);
			}
		}
		$this->db->group_end();
		$query = $this->db->get();
		return $query;
	}
	
	public function count_all($table){
		return $this->db->from($table)->count_all_results();
	}
	
	public function count_filtered($column,$table,$search_value,$column_search,$from_date,$to_date){
		$query=$this->get_order_list_query($column,$table,$search_value,$column_search,$from_date,$to_date);
		return $query->num_rows();
	}
	public function get_filtered_data($column,$table,$search_value,$column_search,$from_date,$to_date,$order="",$length,$start){
		$query=$this->get_order_list($column,$table,$search_value,$column_search,$from_date,$to_date,$order,$length,$start);
		return $query->result();
	}
	private function get_order_list($column,$table,$search_value,$column_search,$from_date,$to_date,$order="",$length,$start){
		$this->db->select($column)->from($table);
		
		if(!empty($from_date) && !empty($to_date)){
			$this->db->where("LOGIN_TIME BETWEEN to_timestamp('" . date('Y-m-d',strtotime($from_date)) . "', 'yyyy-mm-dd') AND to_timestamp('" . date('Y-m-d',strtotime($to_date)) . "', 'yyyy-mm-dd')");
        }
        $this->db->group_start();
		if (!empty($search_value)) {
			foreach ($column_search as $key => $item) {
				$this->db->or_like($item,$search_value);
			}
		}
		$this->db->group_end();
		if(is_array($order)){
			foreach($order as $key=>$value){
				$this->db->order_by($value);
			}	
		}
		if ($length != -1) {
            $this->db->limit($length,$start);
        }
		$query = $this->db->get();
		return $query;
	}
}

?>