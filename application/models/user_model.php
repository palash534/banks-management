<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
        $this->db->select('TBL_USERS.USERID, TBL_USERS.EMAIL, TBL_USERS.NAME, TBL_USERS.MOBILE, TBL_ROLES.ROLE');
        $this->db->from('TBL_USERS');
        $this->db->join('TBL_ROLES', 'TBL_ROLES.ROLEID = TBL_USERS.ROLEID','left');
        if(!empty($searchText)) {
            $likeCriteria = "(TBL_USERS.EMAIL  LIKE '%".$searchText."%'
                            OR  TBL_USERS.NAME  LIKE '%".$searchText."%'
                            OR  TBL_USERS.MOBILE  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('TBL_USERS.ISDELETED', 0);
        $this->db->where('TBL_USERS.ROLEID !=', 1);
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '' /*, $page, $segment*/)
    {
        $this->db->select('TBL_USERS.USERID, TBL_USERS.EMAIL, TBL_USERS.NAME, TBL_USERS.MOBILE, TBL_ROLES.ROLE');
        $this->db->from('TBL_USERS');
        $this->db->join('TBL_ROLES', 'TBL_ROLES.ROLEID = TBL_USERS.ROLEID','left');
        if(!empty($searchText)) {
            $likeCriteria = "(TBL_USERS.EMAIL  LIKE '%".$searchText."%'
                            OR  TBL_USERS.NAME  LIKE '%".$searchText."%'
                            OR  TBL_USERS.MOBILE  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('TBL_USERS.ISDELETED', 0);
        $this->db->where('TBL_USERS.ROLEID !=', 1);
    /*	$this->db->limit($page, $segment);	*/		// FOR CI PAGINATION
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {
        $this->db->select('ROLEID, ROLE');
        $this->db->from('TBL_ROLES');
        $this->db->where('ROLEID !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("EMAIL");
        $this->db->from("TBL_USERS");
        $this->db->where("EMAIL", $email);   
        $this->db->where("ISDELETED", 0);
        if($userId != 0){
            $this->db->where("USERID !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
		// Setting permission to default value when new user created
		$permission=array('EDIT'=>null, 'SHOW'=>null, 'REMOVE'=>null, 'MAKE'=>null);
		// Transaction - 
        $this->db->trans_start();
        $this->db->insert('TBL_USERS', $userInfo);
		$this->db->insert('TBL_USER_PERMISSION', $permission);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('USERID, NAME, EMAIL, MOBILE, ROLEID, GUARD_DETAIL.GUARD_NAME, TBL_USERS.TG_CD, M_PS_CODE.PS_NAME, TBL_USERS.PS_CD');
        $this->db->from('TBL_USERS');
		$this->db->join('GUARD_DETAIL', 'GUARD_DETAIL.CODE = TBL_USERS.TG_CD', 'left');
		$this->db->join('M_PS_CODE', 'M_PS_CODE.PS_CD = TBL_USERS.PS_CD', 'left');
        $this->db->where('ISDELETED', 0);
		$this->db->where('ROLEID !=', 1);
        $this->db->where('USERID', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('USERID', $userId);
        $this->db->update('TBL_USERS', $userInfo);
        // echo $this->db->last_query();
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('USERID', $userId);
        $this->db->update('TBL_USERS', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('USERID, PASSWORD');
        $this->db->where('USERID', $userId);        
        $this->db->where('ISDELETED', 0);
        $query = $this->db->get('TBL_USERS');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->PASSWORD)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('USERID', $userId);
        $this->db->where('ISDELETED', 0);
        $this->db->update('TBL_USERS', $userInfo);
        
        return $this->db->affected_rows();
    }
}

  