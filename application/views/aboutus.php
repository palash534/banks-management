i <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url(); ?>assets/images/background/team.jpg)">
    	<div class="auto-container">
        	<h1>About Us</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index.html">Home</a></li>
                <li>About Us</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
     <!--Welcome Section-->
    <section class="welcome-section">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Column-->
                <div class="content-column col-md-7 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <h2>ಕುಂಬಾರರ ಗುಡಿ ಕೈಗಾರಿಕಾ ಸಹಕಾರ ಸಂಘ (ನಿ.)<br><center> ಪುತ್ತೂರು </center></h2>
                        </div>
                        <div class="text">ಗ್ರಾಮೀಣ ಕುಂಬಾರಿಕೆ ಹಾಗೂ ಕುಶಲಕರ್ಮಿಗಳ ಅಭಿವೃದ್ಧಿಗಾಗಿಯೇ ಸ್ಥಾಪಿತವಾದ ನಮ್ಮಸಹಕಾರ ಸಂಘ ಈಗ ಎಲ್ಲಾ ವರ್ಗದ ಸದಸ್ಯರಿಗೂ ಸೇವೆಯನ್ನು ನೀಡುತ್ತಾ ಬಂದಿದೆ. ಕುಂಬಾರ ಸಮಾಜದ ಹಲವಾರು ಸಹಕಾರಿ ಧುರೀಣರು ತಮ್ಮ ನಿಸ್ವಾರ್ಥ, ವಿಶಿಷ್ಟ ಹಾಗೂ ಶ್ರದ್ದಾಪೂರ್ವಕ ಸೇವೆಯನ್ನು ಸಲ್ಲಿಸಿದುದರ ಫಲವಾಗಿ ಸಹಕಾರ ಸಂಘವು ಸರ್ವಾಂಗೀನ ಅಭಿವೃದ್ಧಿಯನ್ನು ಸಾಧಿಸಿ ಜನ ಮಾನಸದಲ್ಲಿ ಪ್ರೀತಿ, ವಿಶ್ವಾಸವನ್ನುಗಳಿಸಲು ಸಾಧ್ಯವಾಗಿದೆ. ಹಿರಿಯ ಸಹಕಾರಿ ಬಂಧುಗಳ ಸಹಕಾರ,ಸದಸ್ಯರ,ಗ್ರಾಹಕ ಬಂಧುಗಳ ಪ್ರೀತಿ,ವಿಶ್ವಾಸವೇ ನಮಗೆ ಶ್ರೀರಕ್ಷೆ.</div>
                        <div class="clearfix">
                            
                            <div class="pull-right">
                                <div class="phone"><span class="icon fa fa-phone"></span>08251-233274</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Image Column-->
                <div class="image-column col-md-5 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/welcome.jpg" alt="" />
                            <a href="https://www.youtube.com/watch?v=7W5CgCnO9Iw" class="lightbox-image play-btn">
                                <span class="icon fa fa-play"></span>
                            </a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Welcome Section-->