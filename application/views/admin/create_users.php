
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
						<div class="box-header with-border">
							<div class="container">
								<div class="row" >
									<div class="col-md-8">
										<h3 class="widget-user-username" id="report_name"><b>User Management</b></h3>
										<h5 class="widget-user-desc">Add New Users</h5>
									</div>
								</div>
							</div>
						</div>
					<!-- form start -->
					<form role="form" id="addUser" action="<?php echo base_url() ?>addNewUser" method="post" role="form">
						<div class="box-body">
                            <div class="row">
                                <div class="col-md-4">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control required" id="fname" name="fname" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="text" class="form-control required email" id="email"  name="email" maxlength="128">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control required digits" id="mobile" name="mobile" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control required" id="password"  name="password" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control required equalTo" id="cpassword" name="cpassword" maxlength="10">
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <select class="form-control required" id="role" name="role">
                                            <option value="0">Select Role</option>
                                            <?php
                                            if(!empty($ROLES))
                                            {
                                                foreach ($ROLES as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->ROLEID ?>"><?php echo $rl->ROLE ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>  
                            </div>
							<div class="row">
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Traffic">Traffic Guard</label>
                                        <select class="form-control" id="tg_name" name="tg_name">
										</select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Police">Police Station</label>
                                        <select class="form-control" id="ps_name" name="ps_name">
										</select>
                                    </div>
                                </div>
							</div>
						</div>
						<div class="box-footer pull-right">
                            <input type="submit" class="btn btn-success" value="Add Users" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
						<div class="box-footer">
							<br>
							<div class="row">
								<div class="col-md-12">
								<?php
									$this->load->helper('form');
									$error = $this->session->flashdata('error');
									if($error)
									{
								?>
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<?php echo $this->session->flashdata('error'); ?>                    
								</div>
								<?php } ?>
								<?php  
									$success = $this->session->flashdata('success');
									if($success)
									{
								?>
								<div class="alert alert-success alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<?php echo $this->session->flashdata('success'); ?>
								</div>
								<?php } ?>
								</div>
								<div class="col-md-12">
									<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
								</div>
							</div>
						</div>
					</form>
					<!-- form end -->
				</div>
				
				<div class="box box-primary">
					<div class="box-header with-border">
						<div class="container">
							<div class="row" >
								<div class="col-md-8">
									<h3 class="widget-user-username" id="role_title"><b>Role Management</b></h3>
									<h5 class="widget-user-desc">Add New Roles</h5>
								</div>
							</div>
						</div>
					</div>
					<!-- form start -->
					<form role="form" id="addRole" role="form">
						<div class="box-body">
                            <div class="row">
                                <div class="col-md-4">                                
                                    <div class="form-group">
                                        <label for="rname">Role Name</label>
                                        <input type="text" class="form-control required" id="rname" name="rname" maxlength="128">
                                    </div>
                                    
                                </div>
                            </div>
						</div>
						<div class="box-footer pull-right">
                            <input type="submit" class="btn btn-success" value="Add Roles" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
						<div class="box-footer">
							<br>
							<div class="row">
								<div class="col-md-12">
									<div id="role_status" class="alert alert-dismissable" style="display:none">
										<!-- Code from JS -->
									</div>
								</div>
							</div>
						</div>
					</form>
					<!-- form end -->
				</div>
			</div>
		</div>
    </section>
</div>
<!--script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script-->
<script>
	$.ajax({
		type: 'POST',
		url: '<?php echo base_url(); ?>insert_Data/get_traffic_guard',
		dataType: 'JSON',
		contentType: false,
		cache: false,
		processData: false,
		success: function ( data ) {
			var html= "<option value=''>- Select Traffic Guard -</option>";
			if(data === null || data == "") {
			} else {
				$.each(data,function(key,value) {
					if(data.length==1 ) {
						html = "<option value='" + value.CODE + "' selected >" + value.GUARD_NAME + "</option>";
						set_ps(value.CODE);
					} else {
						html += "<option value='" + value.CODE + "'>" + value.GUARD_NAME + "</option>";
					}
					
				});
			}
			$('#tg_name').append(html);
		}
	});
	var set_ps= function(tg_code, ps_code='') {
		if(tg_code != '') {
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url(); ?>insert_Data/get_ps',
				dataType: 'JSON',
				data :    {traffic_guard : tg_code},
				cache: false,
				success: function ( data ) {
					var html= "<option value=''>- Select Police Station -</option>";
					if(data === null || data == "") {
					} else {
						$.each(data,function(key,value) {
							if(data.length==1 ) {
								html = "<option value='" + value.PS_CD + "' selected readonly>" + value.PS_NAME + "</option>";
							} else {
								html += "<option value='" + value.PS_CD + "'>" + value.PS_NAME + "</option>";
							}
							
						});
					}
				$('#ps_name').html(html);
				// console.log( html );
				}
			});
		}
	}
	$('#tg_name').on('change',function(e) {
		var tg_code = $('#tg_name').val();
		// console.log( tg_code );
		if (tg_code)
			set_ps(tg_code);
		else
			$('#ps_name').empty().append("<option value=''>- Select Police Station -</option>");
	});
	
	$( "#addRole" ).submit(function( event ) {
		var role = $('#rname').val();
		$.ajax({
				type: 'POST',
				url: '<?php echo base_url(); ?>Admin/set_rolename',
				dataType: 'JSON',
				data :    {'ROLE' : role},
				cache: false,
				success: function ( data ) {
					if (data==1) { 
						$('#role_status').addClass('alert-success');
						$('#role_status').show().html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> <span>Role Name successfully added to database</span>");
					}
					else {
						$('#role_status').addClass('alert-danger');
						$('#role_status').show().html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> <span>Some error has occured. Try again.</span>");
					}
				}
			});
		event.preventDefault();
	});
</script>