<?php
if(!empty($USERINFO))
{
    foreach ($USERINFO as $uf)
    {
        $userId = $uf->USERID;
        $name = $uf->NAME;
        $email = $uf->EMAIL;
        $mobile = $uf->MOBILE;
        $roleId = $uf->ROLEID;
		$tg_cd = $uf->TG_CD;
		$ps_cd = $uf->PS_CD;
		$tg_name = $uf->GUARD_NAME;
		$ps_name = $uf->PS_NAME;
		
		echo $tg_cd.$ps_cd.$roleId.$tg_name.$ps_name;
    }
}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
						<div class="box-header with-border">
							<div class="container">
								<div class="row" >
									<div class="col-md-8">
										<h3 class="widget-user-username" id="report_name"><b>User Management</b></h3>
										<h5 class="widget-user-desc">Edit Users</h5>
									</div>
								</div>
							</div>
						</div>
					<!-- form start -->
					<form role="form" action="<?php echo base_url() ?>editUser" method="post" id="editUser" role="form">
						<div class="box-body">
                            <div class="row">
                                <div class="col-md-4">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name" name="fname" value="<?php echo $name; ?>" maxlength="128">
                                        <input type="hidden" value="<?php echo $userId; ?>" name="userId" id="userId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo $email; ?>" maxlength="128">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control" id="mobile" placeholder="Mobile Number" name="mobile" value="<?php echo $mobile; ?>" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-4">
									<div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" maxlength="10">
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <select class="form-control" id="role" name="role">
                                            <option value="0">Select Role</option>
                                            <?php
                                            if(!empty($ROLES))
                                            {
                                                foreach ($ROLES as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->ROLEID; ?>" <?php if($rl->ROLEID == $roleId) {echo "selected=selected";} ?>><?php echo $rl->ROLE ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>  
                            </div>
							<div class="row">
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Traffic">Traffic Guard</label>
                                        <select class="form-control" id="tg_name" name="tg_name">
											<?php
                                            if(!empty($tg_name))
                                            { ?>
                                                <option value="<?= $tg_cd ?>" style="color: #3c8dbc;"><?= $tg_name ?></option>
                                            <?php
                                            }
                                            ?>
										</select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Police">Police Station</label>
                                        <select class="form-control" id="ps_name" name="ps_name">
											<?php
                                            if(!empty($ps_name))
                                            { ?>
                                                <option value="<?= $ps_cd ?>"><?= $ps_name ?></option>
                                            <?php
                                            }
                                            ?>
										</select>
                                    </div>
                                </div>
							</div>
						</div>
						<div class="box-footer pull-right">
                            <input type="submit" class="btn btn-success" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
						<div class="box-footer">
							<br>
							<div class="row">
								<div class="col-md-12">
								<?php
									$this->load->helper('form');
									$error = $this->session->flashdata('error');
									if($error)
									{
								?>
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<?php echo $this->session->flashdata('error'); ?>                    
								</div>
								<?php } ?>
								<?php  
									$success = $this->session->flashdata('success');
									if($success)
									{
								?>
								<div class="alert alert-success alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<?php echo $this->session->flashdata('success'); ?>
								</div>
								<?php } ?>
								</div>
								<div class="col-md-12">
									<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
    </section>
</div>

<!--script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script-->
<script>
$(function () {
	// GET TRAFFIC GUARD LIST
	$.ajax({
		type: 'POST',
		url: '<?php echo base_url(); ?>insert_Data/get_traffic_guard',
		dataType: 'JSON',
		contentType: false,
		cache: false,
		processData: false,
		success: function ( data ) {
			var html= "<option value=''>- Select Traffic Guard -</option>";
			if(data === null || data == "") {
			} else {
				$.each(data,function(key,value) {
					if(data.length==1 ) {
						html = "<option value='" + value.CODE + "' selected >" + value.GUARD_NAME + "</option>";
						set_ps(value.CODE);
					} else {
						html += "<option value='" + value.CODE + "'>" + value.GUARD_NAME + "</option>";
					}
					
				});
			}
			$('#tg_name').append(html);
		}
	});
	// GET POLICE STATION LIST
	var set_ps= function(tg_code, ps_code='') {
		if(tg_code != '') {
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url(); ?>insert_Data/get_ps',
				dataType: 'JSON',
				data :    {traffic_guard : tg_code},
				cache: false,
				success: function ( data ) {
					var html= "<option value=''>- Select Police Station -</option>";
					if(data === null || data == "") {
					} else {
						$.each(data,function(key,value) {
							if(data.length==1 ) {
								html = "<option value='" + value.PS_CD + "' selected readonly>" + value.PS_NAME + "</option>";
							} else {
								html += "<option value='" + value.PS_CD + "'>" + value.PS_NAME + "</option>";
							}
							
						});
					}
				$('#ps_name').html(html);
				// console.log( html );
				}
			});
		}
	}
	// POLICE STATION CHANGE ON TRAFFIC GUARD_NAME CHANGED
	$('#tg_name').on('change',function(e) {
		var tg_code = $('#tg_name').val();
		if (tg_code)
			set_ps(tg_code);
		else
			$('#ps_name').empty().append("<option value=''>- Select Police Station -</option>");
	});
});
</script>