<style>
.m-dialog {
	padding-top:15%;
	position: relative;
	width: auto;
	margin: auto 30% auto;
}
</style>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
						<div class="container">
							<div class="row" >
								<div class="col-md-8">
									<h3 class="widget-user-username" id="report_name"><b>User Permissions</b></h3>
									<h5 class="widget-user-desc">Set Permissions for Users (Create / Update / Delete)</h5>
								</div>
							</div>
						</div>
                    </div>
                    <div class="box-body">
					<form id="con_scope" action="">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-bordered" style="width:100%">
                                <thead> 
                                    <tr>
                                        <th>No.</th>
                                        <th>User ID</th>
                                        <th>Name</th>
										<th>Role</th>
										<th>Permissions</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
						<!--input type="submit" class="btn btn-success pull-right" value="Update"-->
					</form>
                    </div>
                </div>
            </div>
        </div>
		
		<!-- The User Permissions Modal -->
		<div class="modal fade" id="permissions_Modal">
			<div class="m-dialog modal-dialog-centered">
				<div class="modal-content">
					<!-- Modal body -->
					<div class="modal-body">
						<!-- Accident Details -->
						<div class="panel panel-default">
							<div class="panel-heading" style="background-color: #fcfcfc;">
								<h3 class="panel-title" style="text-align: center; color:#3c8dbc; font-size: 1.5em;" data-toggle="collapse" data-parent="#accordion" href="#accident_details">
									<strong>Permissions</strong>
								</h3>
							</div>
							<div id="div_user_permissions" class="panel-collapse">
								<div class="panel-body">
									<!-- form start -->
									<section class="">
									<form id="user_permissions">
										<!-- title row -->
										<div class="row" >
											<div class="form-group col-sm-3">
												<label class="control-label">User ID</label>
												<input id="role_id" name="role_id" maxlength="100" type="text" class="form-control" disabled />
											</div>
											<div class="form-group col-sm-5">
												<label class="control-label">User Name</label>
												<input id="user_name" name="user_name" maxlength="100" type="text" class="form-control" disabled />
											</div>
											<div class="form-group col-sm-4">
												<label class="control-label">Role Name</label>
												<input id="role_name" name="role_name" maxlength="100" type="text" class="form-control" disabled />
											</div>
											<div class="form-group col-sm-3">
												<label class="control-label">Permissions : </label>
											</div>
											<div class="form-group col-sm-8">
												<h4 style="margin-top: -0.5%;">
												<input type="checkbox" name="show" value="Show"> Show &nbsp;
												<input type="checkbox" name="insert" value="Insert"> Insert &nbsp;
												<input type="checkbox" name="edit" value="Edit"> Edit &nbsp;
												<input type="checkbox" name="remove" value="Remove"> Remove &nbsp;
												</h4>
											</div>
										</div>
									</form>
									<div id="save_status" class="alert" style="display: none"></div>
									</section>
									<!-- /.box-body -->
								</div>
							</div>
						</div>
						<!-- ./End -->
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button id="update_permission" class="btn btn-success">Save</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>

<script>
  $(function(){
	  
	// Populate Data in DataTable
	datatable = $('#datatable1').DataTable({
		"lengthMenu": [ [5,10, 25, 50, -1], [5,10, 25, 50, "All"] ],
		"responsive": true,
		"processing": true, //Feature control the processing indicator.
		"serverSide": false, //Feature control DataTables' 
								 //server-side processing mode.
		// "order": [[ 0, 'asc' ], [ 5, 'asc' ]], 	//Initial no order.
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": '<?=base_url()?>Admin/get_userRecords',
			"type": "POST",
			// You can post any data from here. If you do not need then remove it.
			"data": function ( d ) { 
						 d.from_date = ''	//'01/01/2019 00:00:00'	//startDate.format('MM/DD/YYYY HH:mm:ss'),
						 d.to_date = ''		//'01/01/2020 00:00:00'	//endDate.format('MM/DD/YYYY HH:mm:ss'),
					}
			
		}, 	
	   // Set column definition initialisation properties.
		"columnDefs": [
			{
				"targets": [1,2,3,4], 	// first, fourth & seventh column
				"orderable": false 		// set not orderable
			}
		],		
		"fnCreatedRow": function (row, data, index) {
							var info = datatable.page.info();
							$("td:first", row).html(index+1+info.start);
							// $(row).attr('id', data.MASTER_RECORD_ID); 
							return row;
						},
		"columns": [
			{ "class": "details-control", "data": null, "defaultContent": "", "name": "Sl_No.", "orderData": 0,},
			{ "data": null,
						"render": function(data, type, row) {
							return data.USERID;
						}
			},
			{ "data": null,
						"render": function(data, type, row) {
							return data.NAME;
						}
			},
			{ "data": null,
						"render": function(data, type, row) {
							return data.ROLE;
						}
			},
			{ "data": null,
						"render": function(data, type, row) {
							var status = "";
								if(data.SHOW=="1"){ status += "👁️";}
								if(data.MAKE=="1"){ status +=  "  |  " + "✍🏻";}
								if(data.EDIT=="1"){ status +=  "  |  " + "✏️"; }
								if(data.REMOVE=="1"){ status +=  "  |  " + "🗑️";}
								return status;
						}
			},
			{ "class": "details-control", "orderable": false, "data": null,
						"render": function(data, type, row) {
							name = data.NAME
							role = data.ROLE
							return "<div class='btn-group' style='display: -webkit-inline-box;'>" +
		"<button type='button' id='edit' class='btn btn-warning btn-md' data-toggle='modal' data-target='#permissions_Modal' data-tag='" + data.USERID + "'><i class='fa fa-pencil'></i></button>" + "</div>";
			}
			}
		],
		 
	});
	
	
	$('#datatable1').on( 'click', '#edit', function () {
		var id  = $(this).data('tag');
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>Admin/get_userRecords_id',
			dataType: 'JSON',
			data: {USERID: id},
			cache: false,
			success: function(data) {
				$('#role_id').val(data[0].USERID);
				$('#user_name').val(data[0].NAME);
				$('#role_name').val(data[0].ROLE);
				(data[0].SHOW == 1) ? $("input[name*='show']").prop('checked', true) : $("input[name*='show']").prop('checked', false);
				(data[0].MAKE == 1) ? $("input[name*='insert']").prop('checked', true) : $("input[name*='insert']").prop('checked', false);
				(data[0].EDIT == 1) ? $("input[name*='edit']").prop('checked', true) : $("input[name*='edit']").prop('checked', false);
				(data[0].REMOVE == 1) ? $("input[name*='remove']").prop('checked', true) : $("input[name*='remove']").prop('checked', false);
			}
		});
	});

	$("#update_permission").on( 'click', function() {
		var data= new Array();
		data = {
			'USERID' : $('#role_id').val(),
			'SHOW' : $("input[name*='show']").prop("checked") == true ? data['SHOW']=1 : data['SHOW']=0,
			'MAKE' : $("input[name*='insert']").prop("checked") == true ? data['MAKE']=1 : data['MAKE']=0,
			'EDIT' : $("input[name*='edit']").prop("checked") == true ? data['EDIT']=1 : data['EDIT']=0,
			'REMOVE' : $("input[name*='remove']").prop("checked") == true ? data['REMOVE']=1 : data['REMOVE']=0
		}
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>Admin/set_permission_id',
			dataType: 'JSON',
			data: data,
			cache: false,
			success: function(data) {
				if (data==1) { 
					swal("Permission",  "Successfully Updated", "success").then((value) => {
						$('#permissions_Modal').modal('toggle');
						datatable.ajax.url( '<?=base_url()?>Admin/get_userRecords' ).load();
					}) 
				}
				else {
					swal("Permission",  "Not Updated Successfully", "error")
				}
			}
		});
	})

  
  });
</script>