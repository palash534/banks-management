
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
						<div class="container">
							<div class="row" >
								<div class="col-md-8">
									<h3 class="widget-user-username" id="report_name"><b>User Details</b></h3>
									<h5 class="widget-user-desc">Edit User's Name / Mobile / Email / Password / Roles </h5>
								</div>
							</div>
						</div>
                    </div>
                    <div class="box-body">
					<form id="user_roles" action="">
                        <div class="table-responsive">
                            <table id="userList" class="table table-striped table-bordered" style="width:100%">
                                <thead> 
                                    <tr>
									  <th>Id</th>
									  <th>Name</th>
									  <th>Email</th>
									  <th>Mobile</th>
									  <th>Role</th>
									  <th class="text-center">Actions</th>
									</tr>
                                </thead>
                                <tbody>
									<?php
									if(!empty($userRecords))
									{
										foreach($userRecords as $record)
										{
									?>
									<tr>
									  <td><?php echo $record->USERID ?></td>
									  <td><?php echo $record->NAME ?></td>
									  <td><?php echo $record->EMAIL ?></td>
									  <td><?php echo $record->MOBILE ?></td>
									  <td><?php echo $record->ROLE ?></td>
									  <td class="text-center">
										<div class="btn-group" style="display: -webkit-inline-box;">
										  <a class="btn btn-warning btn-md edit" data-toggle="tooltip" title="Edit" href="<?php echo base_url().'editOld/'.$record->USERID; ?>"><i class="fa fa-pencil"></i></a>
										  <a class="btn btn-md btn-danger del" data-toggle="tooltip" title="Delete" href="#" data-userid="<?php echo $record->USERID; ?>"><i class="fa fa-trash"></i></a>
										</div>
									  </td>
									</tr>
									<?php
										}
									}
									?>
                                </tbody>
                            </table>
                        </div>
						<!--div class="box-footer clearfix"-->
							<!--?php echo $this->pagination->create_links(); ?-->
						<!--/div-->
					</form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!--script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script-->
<!--script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script-->
<script>
  $(function(){
	//Initialize Select2 Elements
    $('.select2').select2()
	$('#userList').DataTable();
	
	// Update Controller Scope
	$("#user_roles").submit(function( event ) {
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>#',
			dataType: 'JSON',
			data :    {},
			cache: false,
			success: function ( data ) {
				swal({
					  title: "Data Updated Successfully!",
					  text: " ",
					  icon: "success",
					  timer: 1500,
					  button: false
				});
			}
		});
		event.preventDefault();
	});
	
	// User Delete
	jQuery(document).on("click", ".del", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "deleteUser",
			currentRow = $(this);
		
		swal({
		  title: "Are you sure?",
		  text: "Once deleted, you will not be able to recover this user!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				if(data.status = true) { 
					currentRow.parents('tr').remove();
					swal("User Successfully Deleted!", {
					  icon: "success",
					});
				}
				else if(data.status = false) { 
					swal("User deletion failed", {
					  icon: "error",
					});
				}
				else {
				swal("Access denied..!", {
				  icon: "error",
				});
				}
			});
		  } else {
			swal("User Not Deleted", {
			  icon: "error",
			});
		  }
		});
	});
	
  });
  
</script>