<style>
.m-dialog {
	padding-top:15%;
	position: relative;
	width: auto;
	margin: auto 30% auto;
}
</style>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
						<div class="container">
							<div class="row" >
								<div class="col-md-8">
									<h3 class="widget-user-username" id="report_name"><b>Controller Scope</b></h3>
									<h5 class="widget-user-desc">Set Controller scope for User Roles</h5>
								</div>
							</div>
						</div>
						<input type="button" class="btn btn-success pull-right insert" value="Insert Controller/Methods">
                    </div>
                    <div class="box-body">
					<form id="tbl_con_scope" action="">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-bordered" style="width:100%">
                                <thead> 
                                    <tr>
                                        <th>No.</th>
                                        <th>Role ID</th>
                                        <th>Name</th>
                                        <th>Controllers</th>
										<th>Methods</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
						<!--input type="submit" class="btn btn-success pull-right" value="Update"-->
					</form>
					<button type="button" class="btn btn-warning btn-md edit"><i data-toggle="tooltip" title="Edit" class="fa fa-pencil"></i></button>
                    </div>
                </div>
            </div>
        </div>
		
		<!-- The Insert / Edit Controller/Methods Modal -->
		<div class="modal fade" id="Con_Meth_Modal">
			<div class="m-dialog modal-dialog-centered">
				<div class="modal-content">
					<!-- Modal body -->
					<div class="modal-body">
					<form id="C_Methods_form" >
						<!-- Controller/Methods Details -->
						<div class="panel panel-default">
							<div class="panel-heading" style="background-color: #fcfcfc;">
								<h3 class="panel-title" style="text-align: center; color:#3c8dbc; font-size: 1.5em;" data-toggle="collapse" data-parent="#accordion" href="#accident_details">
									<strong>Controllers & Methods</strong>
								</h3>
							</div>
							<div id="C_Methods_details" class="panel-collapse">
								<div class="panel-body">
									<!-- form start -->
									<section class="">
										<!-- title row -->
										<div class="row" >
											<div class="form-group col-sm-6 hide">
												<label class="control-label">Role ID</label>
												<input id="role_id" name="role_id" maxlength="100" type="text" class="form-control" disabled />
											</div>
											<div class="form-group col-sm-6">
												<label class="control-label">Role Name</label>
												<select id="role_name" name="role_name" class="form-control" data-placeholder="Select User" aria-hidden="true" data-live-search="true" data-validation="required">
													<option value="" selected>- -Select Roles --</option>
												</select>
											</div>
											<div class="form-group col-sm-6">
												<label class="control-label">Controllers</label>
												<select id="controllers" name="controllers" class="form-control" data-placeholder="Select Controller" aria-hidden="true" data-live-search="true" data-validation="required">
													<option value="" selected>-- Select Controller --</option>
												</select>
											</div>
											<div class="form-group col-sm-6">
												<label class="control-label">Methods</label>
												<select id="methods" name="methods" class="form-control" data-placeholder="Select Methods" aria-hidden="true" data-live-search="true" data-validation="required">
													<option value="" selected>-- Select Methods --</option>
												</select>
											</div>
										</div>
									<div id="save_status" class="alert" style="display: none"></div>
									</section>
									<!-- /.box-body -->
								</div>
							</div>
						</div>
						<!-- ./End -->
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button id="save_con_meth" class="btn btn-success">Save</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
					</form>
				</div>
			</div>
		</div>
    </section>
</div>

<script>
  $(function(){
	//VALID
	$.validate();
	
	// Get all roles
	const get_roles = () => {
		$.ajax({
			type: 'POST',
			url: baseURL + '/Admin/get_roles',
			dataType: 'JSON',
			data :    {},
			cache: false,
			success: function ( data ) {
				data.map((el) => {
					$('#role_name').append('<option value="'+el.ROLEID+'">'+el.ROLE+'</option>');
				});
			}
		});
	}
	
	// Get all controllers
	const get_all_controller = () => {
  		$.ajax({
			type: 'POST',
			url: baseURL + '/Admin/get_all_controller',
			dataType: 'JSON',
			data :    {},
			cache: false,
			success: function ( data ) {
				data.map((el) => {
					$('#controllers').append('<option value="'+el+'">'+el+'</option>');
				});
			}
		});	
  	}
	
	// Get all methods of respective controllers
  	const get_methods = (props) => {
  		$.ajax({
			type: 'POST',
			url: baseURL + '/Admin/get_all_methods',
			dataType: 'JSON',
			data :    {'controller_name' : props},
			cache: false,
			success: function ( data ) {
				data.map((el) => {
					console.log(el);
					$('#methods').append('<option value="'+el.METHOD_NAME+'">'+el.METHOD_NAME+'</option>');
				});
			}
		});	
  	}
	
	// Open Insert Modal with ROLEID disable
	$('.edit').on("click", function(e){
		$('#role_name').attr("disabled", 'disabled');
		$('#Con_Meth_Modal').modal(); 
	});
	// Open Insert Modal with ROLEID enable
	$('.insert').on("click", function(e){
		$('#role_name').removeAttr("disabled");
		$('#Con_Meth_Modal').modal(); 
	});
	
	// Populate Data in DataTable
	datatable = $('#datatable1').DataTable({
		"lengthMenu": [ [5,10, 25, 50, -1], [5,10, 25, 50, "All"] ],
		"responsive": true,
		"processing": false, //Feature control the processing indicator.
		"serverSide": false, //Feature control DataTables' 
								 //server-side processing mode.
		// "order": [[ 0, 'asc' ], [ 5, 'asc' ]], 	//Initial no order.
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": '<?=base_url()?>Admin/get_all_role_controller_scope',
			"type": "POST",
			// You can post any data from here. If you do not need then remove it.
			"data": function ( d ) { 
						 d.from_date = ''	//'01/01/2019 00:00:00'	//startDate.format('MM/DD/YYYY HH:mm:ss'),
						 d.to_date = ''		//'01/01/2020 00:00:00'	//endDate.format('MM/DD/YYYY HH:mm:ss'),
					}
		}, 	
	   // Set column definition initialisation properties.
		"columnDefs": [
			{
				"targets": [1,2,3,4], 	// first, fourth & seventh column
				"orderable": false 		// set not orderable
			}
		],		
		"fnCreatedRow": function (row, data, index) {
							var info = datatable.page.info();
							$("td:first", row).html(index+1+info.start);
							// $(row).attr('id', data.MASTER_RECORD_ID); 
							return row;
						},
		"columns": [
			{ "class": "details-control", "data": null, "defaultContent": "", "name": "Sl_No.", "orderData": 0,},
			{ "data": null,
						"render": function(data, type, row) {
							return data.ROLEID;
						}
			},
			{ "data": null,
						"render": function(data, type, row) {
							return data.ROLE;
						}
			},
			{ "class": "details-control", "orderable": false, "data": null,
						"render": function(data, type, row) {
								return data.CONTROLLER;
			}
			},
			{ "class": "details-control", "orderable": false, "data": null,
						"render": function(data, type, row) {
								return data.METHOD;
			}
			}
		],
		 
	});
	
	
	// Insert Controller Scope
	$("#C_Methods_form").submit(function( event ) {
		$.ajax({
			type: 'POST',
			url: baseURL + '/Admin/save_ControllerMethod',
			dataType: 'JSON',
			data : {
				'ROLEID': $('#role_name').val(),
				'CONTROLLER': $('#controllers').val(),
				'METHOD': $('#methods').val()
			},
			cache: false,
			success: function ( data ) {
				if (data==1) { 
					$('#Con_Meth_Modal').modal('toggle');
					swal("Controller & Method",  "Successfully Updated", "success").then((value) => {
						datatable.ajax.url( '<?=base_url()?>Admin/get_all_role_controller_scope' ).load();
					}) 
				}
				else {
					swal("Controller & Method",  "Not Updated - Something goes wrong ", "error")
				}
			}
		});
		event.preventDefault();
	});

	get_roles();
	get_all_controller();

	$('#controllers').change(() => {
		const controller_name = $('#controllers option:selected').val();
		get_methods(controller_name);
	});



  });
</script>