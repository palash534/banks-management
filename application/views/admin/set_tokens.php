
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
						<div class="container">
							<div class="row" >
								<div class="col-md-8">
									<h3 class="widget-user-username" id="report_name"><b>Google Map API</b></h3>
									<h5 class="widget-user-desc">Set API Tokens here</h5>
								</div>
							</div>
						</div> 
					</div>
                    <div class="box-body">
						<form id="set_gmap_key" action="">
							<div class="col-md-8">
								<label for="key" class="btn">Set Key :</label>
								<input id="map_key" class="btn btn-default" type="text" name="map_key" value="<?=$GMAP_API_KEY?>" style="width:50%">
								&nbsp;
								<input type="submit" class="btn btn-success" value="Update">
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
  $(function(){
	// Update Google Map API Key
	$("#set_gmap_key").submit(function( event ) {
		var map_key=$('#map_key').val();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>Admin/set_api_tokens',
			dataType: 'JSON',
			data :    {'map_key' : map_key},
			cache: false,
			complete: function ( data ) {
				swal({
					  title: "Data Updated Successfully!",
					  text: " ",
					  icon: "success",
					  timer: 1500,
					  button: false
				});
			}
		});
		event.preventDefault();
	});
	
  });
  
</script>