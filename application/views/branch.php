  <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url(); ?>assets/images/background/11.jpg)">
        <div class="auto-container">
            <h1><?= $title; ?></h1>
            <ul class="page-breadcrumb">
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li><?= $title; ?></li>
            </ul>
        </div>
    </section>
     <section class="report-section">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Column-->
                <div class="content-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                           
                            <?= $link; ?>
                    </div>
                    
                </div>
                <!--Graph Column-->
                <div class="graph-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/branch/<?= $img; ?>.jpg" alt="branch" width="580" height="200" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Report Section-->

    
    <!--Contact Info Section-->
    <section class="contact-info-section">
        <div class="auto-container">
            <div class="row clearfix">
                
                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <span class="icon flaticon-placeholder"></span><b>
                                <?= $address ?>
                       </li>
                    </ul>
                </div>
                
                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <span class="icon flaticon-technology-2"></span>
                            <?= $contact; ?>

                        </li>
                    </ul>
                </div>
                
                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <span class="icon flaticon-clock-2"></span>
                           Mon To Fri 9:15 am-1:30 pm <br>
                           2:15 pm - 5:30 pm <br>
                           2nd Sat 09-15 am To 01-30 pm
                          <br> Sunday Closed
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Contact Info Section-->
    
  