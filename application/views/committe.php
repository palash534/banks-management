 <!--Page Title-->
    <section class="page-title img-responsive" style="background-image:url(<?php echo base_url(); ?>assets/images/background/team.jpg)">
    	<div class="auto-container">
        	<h1>Our Committee Members </h1>
            <ul class="page-breadcrumb">
            	<li><a href="index.html">Home</a></li>
                <li>Our Committee</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
     <!--Welcome Section-->
    <section class="team-page-section style-two">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="<?php echo base_url(); ?>assets/images/resource/team-1.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                        	<h3>ಶ್ರೀ  ಭಾಸ್ಕರ ಎಂ.ಪೆರುವಾಯಿ <small> B.A(Law)LLB.</small></h3>
                            <div class="designation">ಅಧ್ಯಕ್ಷರು</div>
                            
                        </div>
                    </div>
                </div>
                 <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="<?php echo base_url(); ?>assets/images/resource/team-3.jpg" alt="sri sachidhananda" />
                        </div>
                        <div class="lower-box">
                        	<h3>ಶ್ರೀಸಚ್ಚಿದಾನಂದ<br> <small>B.Arch. MCA,IVA</h3>
                            <div class="designation">ಉಪಾಧ್ಯಕ್ಷರು</div>
                           
                        </div>
                    </div>
                </div>

               


                 <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/team-4.jpg" alt="b.s kulal" />
                        </div>
                        <div class="lower-box">
                            <h3>ಶ್ರೀ  ಬಿ.ಎಸ್  ಕುಲಾಲ್<br><small>ಮಾಜಿ  ತಹಶೀಲ್ದಾರರು </small> </h3>
							<div class="designation">ಸದಸ್ಯರು </div>
                           
                        </div>
                    </div>
                </div>
				
				 <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="<?php echo base_url(); ?>assets/images/resource/team-2.jpg" alt="s.janardhan moolya" />
                        </div>
                        <div class="lower-box">
                        	<h3>ಶ್ರೀ ಎಸ್.ಜನಾರ್ಧನ ಮೂಲ್ಯ<br><small>B.Sc, HDCM</small></h3>
                            <div class="designation">ಮುಖ್ಯಕಾರ್ಯ ನಿರ್ವಹಣಾ  ಅಧಿಕಾರಿ</div>
                           
                        </div>
                    </div>
                </div>
                
               
                
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/team-5.jpg" alt="harish k" />
                        </div>
                        <div class="lower-box">
                            <h3>ಶ್ರೀ ಹರೀಶ್ ಕೆ</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                            
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/team-6.jpg" alt="narayan kulal" />
                        </div>
                        <div class="lower-box">
                            <h3>ಶ್ರೀ ನಾರಾಯಣ ಕುಲಾಲ್</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                           
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="<?php echo base_url(); ?>assets/images/resource/team-7.jpg" alt="dhamodar v" />
                        </div>
                        <div class="lower-box">
                        	<h3>ಶ್ರೀ .ದಾಮೋದರ  ವಿ.</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                            
                        </div>
                    </div>
                </div>

                 <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/team-8.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                            <h3>ಶ್ರೀ ಪಿ.ಧರ್ಣಪ್ಪ ಮೂಲ್ಯ</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                           
                        </div>
                    </div>
                </div>
                 <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/team-9.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                            <h3>ಶ್ರೀ ಗಣೇಶ್ ಪಿ</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                           
                        </div>
                    </div>
                </div>
                 <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/team-10.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                            <h3>ಶ್ರೀ ನಾಗೇಶ್ ಕುಲಾಲ್</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                            
                        </div>
                    </div>
                </div>
                 <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/team-11.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                            <h3>ಶ್ರೀ ಯಚ್. ಪದ್ಮಕುಮಾರ್</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                            
                        </div>
                    </div>
                </div>
				
				<!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/SHIVAPPA.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                            <h3>ಶ್ರೀ ಶಿವಪ್ಪ ಮೂಲ್ಯ</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                            
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="<?php echo base_url(); ?>assets/images/resource/team-12.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                        	<h3>ಶ್ರೀಮತಿ ಯಶೋಧ ಬಿ</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                           
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="<?php echo base_url(); ?>assets/images/resource/team-13.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                        	<h3>ಶ್ರೀಮತಿ  ಜಯಶ್ರೀ  ಯಸ್</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                            
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="<?php echo base_url(); ?>assets/images/resource/team-14.jpg" alt="" />
                        </div>
                        <div class="lower-box">
                        	<h3>ಶ್ರೀಮತಿ ವಿ ರೋಹಿಣಿ</h3>
                            <div class="designation">ಸದಸ್ಯರು</div>
                            
                        </div>
                    </div>
                </div>
                
               
                
            </div>
        </div>
    </section>
    <!--End Welcome Section-->
    
   