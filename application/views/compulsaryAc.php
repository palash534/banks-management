        <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<div class="services-single">
						<div class="inner-box">
							<h3><u>COMPULSARY DEPOSIT</u></h3>
                            <div class="text">
                                <div class="two-column row clearfix">
                                	<div class="column col-md-8 col-sm-8 col-xs-12">
                                    	
                                        <b>Interest rate 5%.*</b>
                                        <p>Compulsory Deposit Account should be opened by the customer who borrows money from us. This amount is deducted at a certain rate of the loan amount. Specific rate of interest will be given to this deposit. This will be refunded only on the full repayment of the loan because this deposit is kept with us as a security deposit.</p>
                                    </div>
                                   
                                </div>
                               
                            </div>
             
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->
           <!-- Call To Action Section -->
     <section class="call-to-action">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h3>If you have any query for related business... Just touch on this button
Contact Us</h3>
                <a href="<?php echo base_url();?>home/contact" class="contact-btn">Contact Us</a>
            </div>
        </div>
    </section>
    <!-- End Call To Action Section -->