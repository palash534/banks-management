 <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url(); ?>assets/images/backtop/headoffice.jpg)">
        <div class="auto-container">
            <h1>Contact Us</h1>
            <ul class="page-breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li>Contact Us</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Default Form Section-->
    <section class="default-form-section">
        <div class="auto-container">
            <!--Title Box-->
            <div class="title-box">
                <div class="title">Write a Message</div>
                <h2>Have Any Questions?</h2>
                <div class="text">Thank you very much for your interest in our co-operative society and our services and if you have any questions, please write us a message now!</div>
            </div>
            
            <!--Default Form-->
            <div class="default-form style-two contact-form">
                <form method="post" action="sendemail.php" id="contact-form">
                    <div class="row clearfix">
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="firstname" value="" placeholder="Your name" required>
                        </div>
                        
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <input type="email" name="email" value="" placeholder="Your Email" required>
                        </div>
                        
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <textarea name="message" placeholder="Your Massage"></textarea>
                        </div>
                        
                        <div class="form-group text-center col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="theme-btn message-btn">Send Massage</button>
                        </div>                                        
                    </div>
                </form>
            </div>
            <!--End Default Form-->

            
        </div>
    </section>
    <!--End Default Form Section-->
    
    <!--Contact Info Section-->
    <section class="contact-info-section">
        <div class="auto-container">
            <div class="row clearfix">
                
                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <span class="icon flaticon-placeholder"></span>
                               KULALA SAHAKARI BHAVANA SAINIKA BHAVAN ROAD, opp. BEO OFFICE, Puttur, Karnataka 574201
                       </li>
                    </ul>
                </div>
                
                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <span class="icon flaticon-technology-2"></span>
              
                              08251-236274 / 08251-233274
                             <br> pcicsputtur@gmail.com
                        </li>
                    </ul>
                </div>
                
                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <span class="icon flaticon-clock-2"></span>
                           Mon To Fri 9:15 am-1:30 pm <br>
                           2:15 pm - 5:30 pm <br>
                           2nd Sat 09-15 am To 01-30 pm
                          <br> Sunday Closed
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Contact Info Section-->
    
    <!--Map Section-->
   
        <!--Map Outer-->
       <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11006.264920307915!2d75.19689232343275!3d12.758870947129356!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe69abe4267e9666!2sPOTTERS%20COTTAGE%20INDUSTRIAL%20CO%20OPERATIVE%20SOCIETY%20LTD%20PUTTUR!5e0!3m2!1sen!2sin!4v1568448360715!5m2!1sen!2sin" width="1500" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
   
    <!--End Map Section-->
    