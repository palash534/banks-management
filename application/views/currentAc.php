        <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<div class="services-single">
						<div class="inner-box">
							<h3><u>CURRENT ACCOUNT </u></h3>
                            <div class="text">
                                <div class="two-column row clearfix">
                                	<div class="column col-md-8 col-sm-8 col-xs-12">
                                    	
                                        <p>Current account is opened by businessmen who have a higher number of regular transactions. It includes deposits, withdrawals, and contra transactions.</p>
                                        <li>•   The applicant should be a member.</li>
<li>•   An Individual/Firm/Institutions can open a current account and shall furnish passport size photograph, Address proof, ID proof and Permanent Account Number and other documents required.</li>
<li>•   No interest will be paid to the Current Account holders.</li>
<li>•   Current Account can be opened with a minimum balance of Rs.500/.</li>
<li>•   Cheque Book facility available for inter branch transactions only.</li>
<li>•   Passbook will be issued free of cost. For duplicate passbook service charges levied.</li>
<li>•   NEFT / RTGS facilities available.</li>
<li>•   SMS service available for the account holders.</li>
                                    </div>
                                     <div class="column col-md-4 col-sm-4 col-xs-12">
                                        <div class="service-image">
                                            <img src="<?php echo base_url(); ?>assets/images/clients/currentaccount.jpg" alt="" />
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
             
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->
           <!-- Call To Action Section -->
    <section class="call-to-action">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h3>If you have any query for related business... Just touch on this button
Contact Us</h3>
                <a href="<?php echo base_url();?>home/contact" class="contact-btn">Contact Us</a>
            </div>
        </div>
    </section>
    <!-- End Call To Action Section -->