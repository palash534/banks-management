<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
        <div class="col-md-8">
          <h3>
            <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
            <small>Control panel</small>
            <div class="form-group pull-right">
              <div class="input-group">
              
                <!-- <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                  <span>May 1, 2019 - May 31, 2019</span>
                  <i class="fa fa-caret-down"></i>
                </button> -->
              </div>
            </div>
          </h3>
        </div>
        <div class="col-md-3 pull-right">
          <label> &nbsp;</label>
          <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; margin-top: 7%;" data-toggle="tooltip" data-placement="left" title="Select from Date Range">
                <i class="fa fa-calendar"></i>&nbsp;
                <span></span> <i class="fa fa-caret-down"></i>
            </div>
        </div>
      </div>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#009FD4">
                <div class="inner">
                  <span class="info-box-text">Total Accidents</span>
                  <span class="info-box-number tot_acc"></span>
                  <div class="overlay-1">
                    <div class="text">
                      <h4 class="tot_acc" ></h4>
                      <p><strong>Total Accident</strong></p>
                    </div>
                  </div>
                  <!--<h6>150</h6>
                  <p>Total Accidents</p>-->
                </div>
                <div class="icon tile-icon">
                  <i class="fa fa-car tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer total-accident">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color: #00A4A6">
                <div class="inner">
                  <span class="info-box-text">Crime Babu Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-2">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Crime Babu Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="fa fa-legal tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer crime-babu">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#5C97BF">
                <div class="inner">
                  <span class="info-box-text">Drunken Driving</span>
                  <span class="info-box-number drunk_drv"></span>
                  <div class="overlay-3">
                    <div class="text">
                      <h4 class="drunk_drv" ></h4>
                      <p><strong>Drunken Driving</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="fa fa-glass tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer drunken-driving ">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#3498DB">
                <div class="inner">
                  <span class="info-box-text">Month wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-4">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Month wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="fa fa-calendar-o tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#1E90FF">
                <div class="inner">
                  <span class="info-box-text">Gender wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-5">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Gender wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="fa fa-venus-mars tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#638BB3">
                <div class="inner">
                  <span class="info-box-text">Age wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-6">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Age wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="ion ion-bag tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#4183D7">
                <div class="inner">
                  <span class="info-box-text">Collission wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-7">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Collission wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="ion ion-bag tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#1E8BC3">
                <div class="inner">
                  <span class="info-box-text">Road User wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-8">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Road User wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="ion ion-bag tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#007FAA">
                <div class="inner">
                  <span class="info-box-text">Offending Vehicle wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-9">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Offending Vehicle wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="ion ion-bag tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#2A7AB0">
                <div class="inner">
                  <span class="info-box-text">Traffic Guard wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-10">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Traffic Guard wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="ion ion-bag tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#3455dbd9">
                <div class="inner">
                  <span class="info-box-text">Time Frame wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-11">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>Time Frame wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="ion ion-bag tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-sm-12">
              <!-- small box -->
              <div class="small-box" style="background-color:#3a739b">
                <div class="inner">
                  <span class="info-box-text">PS wise Fatal Report</span>
                  <span class="info-box-number">1,410</span>
                  <div class="overlay-12">
                    <div class="text">
                      <h4>150</h4>
                      <p><strong>PS wise Fatal Report</strong></p>
                    </div>
                  </div>
                </div>
                <div class="icon tile-icon">
                  <i class="ion ion-bag tile-icon-small"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>
          <div class="row">
            <div class="col-md-12">
			<!-- Table -->
				<div id="collapse_table" class="box" style="display:none;">
					<div class="box-header with-border">
					  <h3 class="box-title slideTitle"></h3>
					</div>
					<div class="box-body">
						<div class="table-responsive">
								<table id="datatable1" class="table table-striped table-bordered" style="width: 100%;">
									<thead> 
										<tr>
											<th>No.</th>
											<th>FIR Details</th>
											<th>GD Details</th>
											<th>Police Station</th>
											<th>Accident Date Time</th>
											<th>Accident Place</th>
											<th>Accident Type</th>
											<!--th>Action</th-->
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
						</div>
					</div>
				</div>
			  <!-- ./Table -->
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="collapse" class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Comparison Graph</h3>
                </div>
                <div class="box-body" style=""> 
                  
                  <div class="chart-container">
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="well">
                          <div id="chartdiv"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div id="collapse" class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title">Graph - 2</h3>
                </div>
                <div class="box-body" style="">
                    <div id="chartdiv-2" style="height:300px"></div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div id="collapse" class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title">Graph - 3</h3>
                </div>
                <div class="box-body" style="">
                <div id="chartdiv-3" style="height:300px"></div>
                </div>
              </div>
            </div>
          </div>
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/plugins/amcharts/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/amcharts/serial.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/amcharts/themes/light.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/amcharts/amstock.js"></script>
<script src="<?php echo base_url(); ?>assets/custom/js/chart-1.js"></script>
<script src="<?php echo base_url(); ?>assets/custom/js/chart-2.js"></script>
<script src="<?php echo base_url(); ?>assets/custom/js/chart-3.js"></script>
<script>
  $(function(){
	var cat;
	//3RD TRY
	//Collapsible panel for total accident 
    $('.total-accident').click(function(){
		if ($( "#collapse_table" ).is( ":hidden" ) && cat != "TOTACC") {
			$("#collapse_table").slideToggle("slow");
			$(".slideTitle").html("Total Accidents");
			datatable.ajax.url( '<?=base_url()?>Dashboard/get_all_accident' ).load();
			cat = "TOTACC"
			console.log('open')
		}
		else if ($( "#collapse_table" ).is( ":visible" ) && cat == "TOTACC") {
			$("#collapse_table").slideToggle("slow");
			cat = ""
			console.log('close')
		}
		else if ($( "#collapse_table" ).is( ":visible" ) && cat != "TOTACC") {
			$("#collapse_table").slideToggle("slow");
			$(".slideTitle").html("Total Accidents");
			datatable.ajax.url( '<?=base_url()?>Dashboard/get_all_accident' ).load();
			$("#collapse_table").slideToggle("slow");
			cat = "TOTACC"
			console.log('close & open')
		}
    });
	//Collapsible panel for drunken driving 
    $('.drunken-driving').click(function(){
		if ($( "#collapse_table" ).is( ":hidden" ) && cat != "DRUDRI") {
			$("#collapse_table").slideToggle("slow");
			$(".slideTitle").html("Total Drunken Driving");
			datatable.ajax.url( '<?=base_url()?>Dashboard/get_drunken_driving_accidents' ).load();
			cat = "DRUDRI"
			console.log('open')
		}
		else if ($( "#collapse_table" ).is( ":visible" ) && cat == "DRUDRI") {
			$("#collapse_table").slideToggle("slow");
			cat = ""
			console.log('close')
		}
		else if ($( "#collapse_table" ).is( ":visible" ) && cat != "DRUDRI") {
			$("#collapse_table").slideToggle("slow");
			$(".slideTitle").html("Total Drunken Driving");
			datatable.ajax.url( '<?=base_url()?>Dashboard/get_drunken_driving_accidents' ).load();
			$("#collapse_table").slideToggle("slow");
			cat = "DRUDRI"
			console.log('close & open')
		}
    });
	
	//Get dates from DateRangePicker
	var startDate=moment().subtract('days', 2000);
	var endDate=moment();

	$('#reportrange span').html(moment().subtract('days', 2000).format('DD/MM/YYYY') + ' - ' + moment().format('DD/MM/YYYY'));
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(2000, 'days'),
        endDate: moment(),
        opens: 'left',
        format: 'DD/MM/YYYY',
        ranges: {
            'Current Year':[moment().startOf('year'),moment().endOf('year')],
            '2 Year':[moment().subtract('years', 2).startOf('year'),moment().endOf('year')],
            '3 Year':[moment().subtract('years', 3).startOf('year'),moment().endOf('year')],
            '4 Year':[moment().subtract('years', 4).startOf('year'),moment().endOf('year')],
            '5 Year':[moment().subtract('years', 5).startOf('year'),moment().endOf('year')]
        }
    }, function(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        startDate = start;
         endDate = end;  
    }).on('apply.daterangepicker', function(e) {
		datatable.ajax.reload();
		total_acc();
		drunk_drv();
		console.log(startDate.format('DD/MM/YYYY'));
	});
	// Get Data Count
	var total_acc = function() {
		var dataset = { 
				'from_date' : startDate.format('MM/DD/YYYY HH:mm:ss'),
				'to_date' : endDate.format('MM/DD/YYYY HH:mm:ss')
			};
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>Dashboard/get_total_accident',
			dataType: 'JSON',
			data: dataset,
			cache: false,
			success: function (data) {
				$('.tot_acc').html(data);
			}
		});
	}
	total_acc();
	var drunk_drv = function() {
		var dataset = { 
				'from_date' : startDate.format('MM/DD/YYYY HH:mm:ss'),
				'to_date' : endDate.format('MM/DD/YYYY HH:mm:ss')
			};
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>Dashboard/get_drunken_driving',
			dataType: 'JSON',
			data: dataset,
			cache: false,
			success: function (data) {
				$('.drunk_drv').html(data);
			}
		});
	}
	drunk_drv();
	
	
	// Populate Data in DataTable
	datatable = $('#datatable1').DataTable({
		"lengthMenu": [ [5,10, 25, 50, -1], [5,10, 25, 50, "All"] ],
		"responsive": true,
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' 
								 //server-side processing mode.
//		"order": [[ 0, 'asc' ], [ 5, 'asc' ]], //Initial no order.
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": "<?=base_url()?>insert_Data/get_all_accident",
			"type": "POST",
			//You can post any data from here. If you do not need then remove it.
			"data": function ( d ) { 
						 d.from_date = startDate.format('MM/DD/YYYY HH:mm:ss'),
						 d.to_date = endDate.format('MM/DD/YYYY HH:mm:ss')
					}
		}, 
	   //Set column definition initialisation properties.
		"columnDefs": [
			{
				"targets": [1,2,3,4,6], //first, fourth & seventh column
				"orderable": false //set not orderable
			}
		],			
		"fnCreatedRow": function (row, data, index) {
							var info = datatable.page.info();
							$("td:first", row).html(index+1+info.start);
							$(row).attr('id', data.MASTER_RECORD_ID); 
							return row;
						},
		"columns": [
			{ "class": "details-control", "data": null, "defaultContent": "", "name": "Sl_No.", "orderData": 0,},
			{ "data": null,
						"render": function(data, type, row) {
							return data.FIR_NO != null ? data.FIR_NO + " Dated " + data.FIR_DATE : "";
						}
			},
			{ "data": null,
						"render": function(data, type, row) {
							return data.GD_NO != null ? data.GD_NO + " Dated " + data.GD_DATE : "";
						}
			},
			{ "data": null,
						"render": function(data, type, row) {
							return data.PS_NAME + " of " + data.GUARD_NAME;
						}
			},
			{ "data": null,
						"render": function(data, type, row) {
							return data.ACC_DATE!= null ? data.ACC_DATE + " at " + data.ACC_TIME : "";
						}
			},
			{ "data": "ACC_PLACE","orderData": [ 5,0 ] },
			{ "data": "NATURE_OF_ACCIDENT" },
	/*		{ "class": "details-control", "orderable": false, "data": null,
						"render": function(data, type, row) {
								return "<div class='btn-group'>" +
	"<button class='btn btn-success btn-md show fa fa-eye' data-tag='" + data.MASTER_RECORD_ID + "' data-toggle='modal' data-target='#myModal'></button>" +
	"<button class='btn btn-warning btn-md fa fa-pencil' data-tag='" + data.MASTER_RECORD_ID + "'></button>" +
	"<button class='btn btn btn-danger del btn-md fa fa-trash' data-tag='" + data.MASTER_RECORD_ID + "'></button>" +
	"<button id='assign_IO_button' class='btn btn-primary fa fa-user' data-toggle='modal' data-target='#assign_IO' data-tag='" + data.MASTER_RECORD_ID + "' ></button>" + "</div>";
			}
			}		*/
		],
		 
	});
	
	
	
  });
	
</script>