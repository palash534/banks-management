        <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<div class="services-single">
						<div class="inner-box">
							<h3><u>FIXED DEPOSIT </u></h3>
                            <div class="text">
                                <div class="two-column row clearfix">
                                	<div class="column col-md-8 col-sm-8 col-xs-12">
                                    	
                                        <p>Fixed deposits are a high-interest -yielding Term deposit. We offers  attractive rate of interest.. Fixed deposit can be opened by Individuals individually or jointly. It can be also opened by an individual guardian on behalf of a minor. A Firm, Trust, Co Operative societies, partnership Firms and other institutions also can open fixed deposit account with us.</p>

                            <div class="table-outer">
                                <table class="cart-table" cellspacing="0" cellpadding="0">
                                   <thead class="cart-header">
                                      <tr>
                                          <th class="price">No. Days</th>
                                          <th>Intrest</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                   <tr>
                                     <td><h4 class="prod-title">30 DAYS TO 45 DAYS </h4></td>
                                     <td class="sub-total">6%</td>
                                   </tr>
                                   <tr>
                                     <td><h4 class="prod-title">46 DAYS TO 90 DAYS  </h4></td>
                                     <td class="sub-total">6.5%</td>
                                   </tr>
                                   <tr>
                                     <td><h4 class="prod-title">91 DAYS TO 180 DAYS  </h4></td>
                                     <td class="sub-total">7.5%</td>
                                   </tr>
                                   <tr>
                                     <td><h4 class="prod-title">181 DAYS TO WITHIN  A YEAR  </h4></td>
                                     <td class="sub-total">8.75%</td>
                                   </tr>
                                   <tr>
                                     <td><h4 class="prod-title">1 YEAR TO WITHIN  2 YEAR  </h4></td>
                                     <td class="sub-total">9%</td>
                                   </tr>
                                   <tr>
                                     <td><h4 class="prod-title">2 YEARS AND ABOVE </h4></td>
                                     <td class="sub-total">9.25%</td>
                                   </tr>
                                   <tr>
                                     <td><h4 class="prod-title">520 DAYS SPECIAL DEPOSIT SCHEME  </h4></td>
                                     <td class="sub-total">9.5%</td>
                                   </tr>
                                 </tbody>
                                </table>
                            </div>
                                    </div>
                            
                                  
                                </div>
                               
                            </div>
             
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->
           <!-- Call To Action Section -->
    <section class="call-to-action">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h3>ಸಂಬಂಧಿತ ವ್ಯವಹಾರಕ್ಕಾಗಿ ನೀವು ಯಾವುದೇ ಪ್ರಶ್ನೆಯನ್ನು ಹೊಂದಿದ್ದರೆ ... ಈ ಗುಂಡಿಯನ್ನು ಸ್ಪರ್ಶಿಸಿ</h3>
                <a href="<?php echo base_url();?>home/contact" class="contact-btn">Contact Us</a>
            </div>
        </div>
    </section>
    <!-- End Call To Action Section -->