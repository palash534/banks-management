  <!--Project Page Section-->
    <section class="project-page-section">
    	<div class="auto-container">
        	
            <!--MixitUp Galery-->
            <div class="mixitup-gallery">
                
                <!--Filter-->
               
                <div class="filter-list row clearfix">
					
                    <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                            	<img src="<?php echo base_url(); ?>assets/images/gallery/1.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/1.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/28.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/28.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Item-->
                   
                   
                   
                     <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/4.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/4.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="filter-list row clearfix">

                    <!--Gallery Item-->
                     <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/3.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/3.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Gallery Item-->
                    <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/5.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/5.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Item-->
                   <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/6.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/6.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="filter-list row clearfix">

                     <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/7.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/7.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Gallery Item-->
                    <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/8.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/8.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Item-->
                   <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/9.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/9.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="filter-list row clearfix">

                     <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/14.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/14.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Gallery Item-->
                    <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/11.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/11.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Item-->
                   <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/12.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/12.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="filter-list row clearfix">

                     <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/16.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/16.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Gallery Item-->
                    <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/27.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/27.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Item-->
                   <!--Gallery Item-->
                    <div class="gallery-item mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/18.jpg" alt="" />
                                   <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo base_url(); ?>assets/images/gallery/18.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
       
            
        </div>
    </section>
    <!--End Project Page Section-->