<!-- Main Footer / Style Two-->
    <footer class="main-footer style-two" style="background-image: url(<?php echo base_url(); ?>assets/images/background/4.jpg);">
        <div class="auto-container">
        
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    <!--Footer Column-->
                    <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                        <!--Logo Widget-->
                        <div class="footer-widget logo-widget">
                            <div class="logo">
                                <a href="index.html"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" /></a>
                            </div>
                            <ul class="list-style-one">
                                <li><span class="icon fa fa-map-marker"></span>Head Office: KULALA SAHAKARI BHAVANA SAINIKA BHAVAN ROAD, opp. BEO OFFICE, Puttur, Karnataka 574201 </li>
                                <li><span class="icon fa fa-envelope"></span> pcicsputtur@gmail.com </li>
                                <li><span class="icon fa fa-headphones"></span>support@pcics.in</li>
                                <li><span class="icon fa fa-phone"></span> 08251-236274 / 08251-233274 </li>
                            </ul>
                        </div>
                    </div>
                    
                    <!--Footer Column-->
                    <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                        <!--Hours Widget-->
                        <div class="footer-widget hours-widget">
                            <h2>Working Days</h2>
                            <div class="widget-content">
                                <div class="text">  Monday to saturday <br>Please Contact us for any inquiry</div>
                                <ul>
                                    <li class="clearfix">Monday to Friday<span>09:15AM    to  01:30PM and 02:15PM to 05:30PM</span></li>
                                    <li class="clearfix">2nd Saturday<span>09:15AM   to  01:30PM</span></li>
                                    <li class="clearfix">Sunday<span>Close</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <!--Footer Column-->
                    <div class="footer-column col-md-4 col-sm-6 col-xs-12 ">
                        <!--News Widget-->
                        <div class="footer-widget news-widget">
                            <h2>Branches</h2>
                            <div class="widget-content">
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>home/branches/1">PUTTUR (Head office)</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/9">B C ROAD</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/2">BELLARE</a></li>
                                       <li><a href="<?php echo base_url(); ?>home/branches/3">GURUVAYANAKERE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/4">KAWDICHARU</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/5">MANI</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/6">UPPINANGADY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/7">VITTAL</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/8">KUDTHAMUGERU</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
            
        </div>
        
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="row clearfix">
                    
                    <div class="column col-md-6 col-sm-12 col-xs-12">
                        <div class="copyright">Copyrights 2019. All Rights are Reserved by <a class="theme_color" href="#">pcics.in</a></div>
                    </div>
                    
                    <div class="social-column col-md-6 col-sm-12 col-xs-12">
                        <div class="copyright"><a class="theme_color" href="#">Andel softech</a></div>
                        <ul class="social-icon-one">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            
                           
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </footer>
    <!-- Main Footer --> 
</div>
<!--End pagewrapper-->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d7cf8689f6b7a4457e1ac78/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>

<script src="js/jquery.js"></script> 
<!--Revolution Slider-->
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main-slider-script.js"></script>

<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mixitup.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/owl.js"></script>
<script src="<?php echo base_url(); ?>assets/js/wow.js"></script>
<script src="<?php echo base_url(); ?>assets/js/appear.js"></script>
<script src="<?php echo base_url(); ?>assets/js/script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/color-settings.js"></script>
<!--Google Map APi Key-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<script src="<?php echo base_url(); ?>js/map-script.js"></script>
<!--End Google Map APi-->

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
</body>
</html>