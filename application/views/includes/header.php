<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8">
<title>POTTERS COTTAGE INDUSTRIAL CO OPERATIVE  SOCIETY LTD PUTTUR | kumbara society| puttur   </title>
<!-- Stylesheets -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugins/revolution/css/settings.css" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="<?php echo base_url(); ?>assets/plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="<?php echo base_url(); ?>assets/plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
<!--Color Switcher Mockup-->
<link href="<?php echo base_url(); ?>assets/css/color-switcher-design.css" rel="stylesheet">
<!--Color Themes-->
<meta property="og:title" content="POTTERS COTTAGE INDUSTRIAL CO OPERATIVE  SOCIETY LTD PUTTUR" />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.pcics.in/">
<meta property="og:image" content="<?php echo base_url(); ?>assets/images/favicon.png" />
<link id="theme-color-file" href="<?php echo base_url(); ?>assets/css/color-themes/brown-theme.css" rel="stylesheet">

<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
<link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta name="google-site-verification" content="J5byQIJM-rOR7Cv3QXSt6zAjj6wQNeLdiNPRm6-k3dA" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="potters society puttur, kumbara bank, putture society, kumbara society, kulal world, kulal, kumbar, loan" />
<meta name="description" content="ಗ್ರಾಮೀಣ ಕುಂಬಾರಿಕೆ ಹಾಗೂ ಕುಶಲಕರ್ಮಿಗಳ ಅಭಿವ್ರದ್ಧಿಗಾಗಿಯೇ ಸ್ಥಾಪಿತವಾದ ನಮ್ಮಸಹಕಾರ ಸಂಘ ಈಗ ಎಲ್ಲಾ ವರ್ಗದ ಸದಸ್ಯರಿಗೂ ಸೇವೆಯನ್ನು ನೀಡುತ್ತಾ ಬಂದಿದೆ.ಕುಂಬಾರ ಸಮಾಜದ ಹಲವಾರು ಸಹಕಾರಿ ಧುರೀಣರು ತಮ್ಮ ನಿಸ್ವಾರ್ಥ, ವಿಶಿಷ್ಟ ಹಾಗೂ ಶ್ರದ್ದಾಪೂರ್ವಕ ಸೇವೆಯನ್ನು ಸಲ್ಲಿಸಿದುದರ ಫಲವಾಗಿ ಸಹಕಾರ ಸಂಘವು ಸರ್ವಾಂಗೀನ ಅಭಿವೃದ್ಧಿಯನ್ನು ಸಾದಿಸಿ ಜನ ಮಾನಸದಲ್ಲಿ ಪ್ರೀತಿ, ವಿಸ್ವಾಸವನ್ನುಗಳಿಸಲು ಸಾಧ್ಯವಾಗಿದೆ.ಹಿರಿಯ ಸಹಕಾರಿ ಬಂಧುಗಳ ಸಹಕಾರ,ಸದಸ್ಯರ,ಗ್ರಾಹಕ ಬಂಧುಗಳ ಪ್ರೀತಿ,ವಿಸ್ವಾಸವೇ ನಮಗೆ ಶ್ರೀರಕ್ಷೆ."/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script> 
</head>

<body>

<body>

<div class="page-wrapper">
    
    <!-- Preloader 
    <div class="preloader"></div>-->
    
    <!-- Main Header-->
    <header class="main-header header-style-three">
        
        <!--Header-Upper-->
        <div class="header-upper">
             <div class="auto-container clearfix">
                
                <div class="pull-left logo-inner">
                    <div class="logo"><a href="<?php echo base_url(); ?>home/"><img src="<?php echo base_url(); ?>assets/images/logoss.png" alt="" title="logo"></a></div>
                </div>
                    
                <div class="pull-right upper-right">
                    <div class="info-columns clearfix">
                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <div class="icon-box"><span class="flaticon-time"></span></div>
                            <ul>
                                <li><strong>Mon - Fri 9:15am To 5:30pm</strong></li>
                                <li><strong>2nd Sat 09:15am To 01:30Pm</strong></li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <div class="icon-box"><span class="flaticon-technology-1"></span></div>
                            <ul>
                                <li><strong>08251-236274 / 08251-233274 </strong></li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <div class="icon-box"><span class="flaticon-timer"></span></div>
                            <ul>
                                <li><strong>pcicsputtur@gmail.com</strong></li>
                            </ul>
                        </div>
                    </div>
                    <ul class="header-info-list">
                        <li>ಸಹಕಾರದಿಂದ ಪ್ರಗತಿ- ಪ್ರಗತಿಗಾಗಿ ಸಹಕಾರ </li>
                        
                        
                        <li class="social">
                            <span><a href="https://www.facebook.com/Potters-Cottage-Industrial-Co-Op-Society-ltd-Puttur-112832513432636/" class="fa fa-facebook"></a></span>
                           
                        </li>
                        
                    </ul>
                </div>
                
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Header Lower-->
        <div class="header-lower clearfix">
            <div class="auto-container">
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="dropdown"><a href="<?php echo base_url(); ?>home/">Home</a> </li>
                                <li class="dropdown"><a href="#">ABOUT </a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>home/aboutus">SOCIETY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/committe">COMMITTEE</a></li>
                                        
                                    </ul>
                                </li>
                                <li ><a href="<?php echo base_url(); ?>home/service">SERVICES</a>                                  
                                </li>
                                <li ><a href="<?php echo base_url(); ?>home/gallery">GALLERY</a>                                  
                                </li>
                              
                                <li ><a href="<?php echo base_url(); ?>news/public_news">NEWS</a>
                                   
                                </li>
                                <li><a href="<?php echo base_url(); ?>home/products">POTTERY PRODUCTS</a>
                                 <li class="dropdown"><a href="#">BRANCHES</a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>home/branches/1">PUTTUR (Head office)</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/9">B C ROAD</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/2">BELLARE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/3">GURUVAYANAKERE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/4">KAWDICHARU</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/5">MANI</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/6">UPPINANGADY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/7">VITTAL</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/8">KUDTHAMUGERU</a></li>


                                        
                                    </ul>
                                </li>  
                                </li>
                                <li><a href="<?php echo base_url();?>home/contact">CONTACT</a></li>
                             </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <!--End Header Lower-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container">
                <div class="sticky-inner-container clearfix">
                    <!--Logo-->
                    <div class="logo pull-left">
                        <a href="<?php echo base_url(); ?>home/" class="img-responsive"><img src="<?php echo base_url(); ?>assets/images/logo-small.png" alt="" title=""></a>
                    </div>
                    
                    <!--Right Col-->
                    <div class="right-col pull-right">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                   <li class="dropdown"><a href="<?php echo base_url(); ?>home/">Home</a> </li>
                                <li class="dropdown"><a href="#">ABOUT </a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>home/aboutus">SOCIETY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/committe">COMMITTEE</a></li>
                                        
                                    </ul>
                                </li>
                                <li ><a href="<?php echo base_url();?>home/service">SERVICES</a><li>
                                 <li ><a href="<?php echo base_url(); ?>home/gallery">GALLERY</a>                                  
                                </li>
                                <li ><a href="<?php echo base_url(); ?>news/public_news">NEWS</a>
                                   
                                </li>
                                <li><a href="<?php echo base_url(); ?>home/products">POTTERY PRODUCTS</a>
                                 <li class="dropdown"><a href="#">BRANCHES</a>
                                    <ul>
                                        
                                        <li><a href="<?php echo base_url(); ?>home/branches/1">PUTTUR (Head office)</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/9">B C ROAD</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/2">BELLARE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/3">GURUVAYANAKERE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/4">KAWDICHARU</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/5">MANI</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/6">UPPINANGADY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/7">VITTAL</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/8">KUDTHAMUGERU</a></li>
        
                                    </ul>
                                </li>  
                                </li>
                                <li><a href="<?php echo base_url();?>home/contact">CONTACT</a></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->
                        
                    </div>
                    
                </div> 
            </div>
        </div>
        <!--End Sticky Header-->
        
    </header>
    <!--End Main Header --> 