<section class="page-title" style="background-image:url(<?php echo base_url(); ?>assets/images/backtop/<?= $image ?>.jpg)">
        <div class="auto-container">
            <h1><?= $pageTitle ?></h1>
            <ul class="page-breadcrumb">
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li><?= $pageTitle ?></li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar no-padd">
                        <!--Blog Category Widget-->
                        <div class="sidebar-widget sidebar-blog-category">
                            <ul class="blog-cat">
                                <li ><a href="<?php echo base_url();?>home/loan/1"><span class="icon glyphicon glyphicon-file"></span> SURITY LOAN</a></li>
                                    <li ><a href="<?php echo base_url();?>home/loan/2"><span class="icon glyphicon glyphicon-grain"></span>JEWEL LOAN</a></li>
                                    <li ><a href="<?php echo base_url();?>home/loan/3"><span class="icon glyphicon glyphicon-piggy-bank"></span> DEPOSIT LOAN</a></li>
                                    <li ><a href="<?php echo base_url();?>home/loan/4"><span class="icon flaticon-meeting"></span> CASH CREDIT LOAN</a></li>
                                    <li ><a href="<?php echo base_url();?>home/loan/5"><span class="icon glyphicon glyphicon-home"></span>HOME LOAN </a></li>
                                    <li ><a href="<?php echo base_url();?>home/loan/6"><span class="icon glyphicon glyphicon-globe"></span>LAND LOANS</a></li>
                                    <li ><a href="<?php echo base_url();?>home/loan/7"><span class="icon glyphicon glyphicon-bed"></span> VEHICLE LOAN</a></li>
                                    <li><a href="<?php echo base_url();?>home/loan/8"><span class="icon glyphicon glyphicon-education"></span> MEMBERS CHILDREN EDUCATION LOANS</a></li>
                                    <li ><a href="<?php echo base_url();?>home/loan/9"><span class="icon glyphicon glyphicon-registration-mark"></span>SELF HELP GROUP LOANS</a></li>
                            </ul>
                        </div>
                  </aside>
                </div>
                