
    <!--End Main Header -->
        <section class="page-title" style="background-image:url(<?php echo base_url(); ?>assets/images/backtop/<?=$img;?>.jpg)">
        <div class="auto-container">
            <h1><?= $pageTitle ?></h1>
            <ul class="page-breadcrumb">
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li><?= $pageTitle ?></li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar no-padd">
                        
                        <!--Blog Category Widget-->
                        <div class="sidebar-widget sidebar-blog-category">
                            <ul class="blog-cat">
                                <li ><a href="<?php echo base_url();?>home/saving"><span class="icon flaticon-employee"></span>SAVINGS ACCOUNT </a></li>
                                <li ><a href="<?php echo base_url();?>home/currentAc"><span class="icon flaticon-money"></span> CURRENT ACCOUNT</a></li>
                                <li><a href="<?php echo base_url();?>home/fixedAc"><span class="icon flaticon-diagram"></span> FIXED DEPOSIT</a></li>
                                <li><a href="<?php echo base_url();?>home/reccuringAc"><span class="icon flaticon-pen"></span> RECCURING DEPOSIT </a></li>
                                <li><a href="<?php echo base_url();?>home/compulsaryAc"><span class="icon flaticon-meeting"></span> COMPULSARY DEPOSIT</a></li>
                                <li><a href="<?php echo base_url();?>home/dailyAc"><span class="icon flaticon-calendar"></span> DAILY DEPOSITES</a></li>
                            </ul>
                        </div>
                        
                     
                        
                       
                    </aside>
                </div>
                