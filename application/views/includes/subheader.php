<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title> <?=$pageTitle ?> </title>
<!-- Stylesheets -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugins/revolution/css/settings.css" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="<?php echo base_url(); ?>assets/plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="<?php echo base_url(); ?>assets/plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
<!--Color Switcher Mockup-->
<link href="<?php echo base_url(); ?>assets/css/color-switcher-design.css" rel="stylesheet">
<!--Color Themes-->
<link id="theme-color-file" href="<?php echo base_url(); ?>assets/css/color-themes/brown-theme.css" rel="stylesheet">

<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
<link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script> 
</head>

<body>

<body>


<div class="page-wrapper">
    
    <!-- Preloader 
    <div class="preloader"></div>-->
    
    <!-- Main Header / Header Style Two-->
    <header class="main-header header-style-two">
    
        <!-- Header Top -->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    
                    <!--Top Left-->
                    <div class="top-left">
                        <ul class="links clearfix">
                            <li><a href="#"><span class="icon fa fa-phone"></span> 08251-236274 / 08251-233274</a></li>
                            <li><a href="#"><span class="icon fa fa-envelope"></span>pcicsputtur@gmail.com</a></li>
                            <li><a href="#"><span class="icon fa fa-map-marker"></span>
                            Potters Cottage Industrial Co-Operative Society Ltd Puttur Head Office</a>
                            </li>
                        </ul>
                    </div>
                    
                    <!--Top Right-->
                    <div class="top-right clearfix">
                        <!--social-icon-->
                        <div class="social-icon">
                            <ul class="clearfix">
                                <li><a href="https://www.facebook.com/Potters-Cottage-Industrial-Co-Op-Society-ltd-Puttur-112832513432636/"><span class="fa fa-facebook"></span></a></li>
                                
                            </ul>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        <!-- Header Top End -->
        
        <!-- Main Box -->
        <div class="main-box">
            <div class="auto-container">
                <div class="outer-container clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <div class="logo"><a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt=""></a></div>
                    </div>
                    
                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                    
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                     <li class=" "><a href="<?php echo base_url();?>">Home</a> </li>
                                <li class="dropdown"><a href="#">ABOUT </a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>home/aboutus">SOCIETY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/committe">COMMITTEE</a></li>
                                        
                                    </ul>
                                </li>
                                <li ><a href="<?php echo base_url(); ?>home/service">SERVICES</a>                                  
                                </li>
                                 <li ><a href="<?php echo base_url(); ?>home/gallery">GALLERY</a>                                  
                                </li>
                              
                                <li ><a href="<?php echo base_url(); ?>news/public_news">NEWS</a>
                                   
                                </li>
                                <li><a href="<?php echo base_url(); ?>home/products">POTTERY PRODUCTS</a>
                                 <li class="dropdown"><a href="#">BRANCHES</a>
                                    <ul>
                                        
                                         <li><a href="<?php echo base_url(); ?>home/branches/1">PUTTUR (Head office)</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/9">B C ROAD</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/2">BELLARE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/3">GURUVAYANAKERE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/4">KAWDICHARU</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/5">MANI</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/6">UPPINANGADY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/7">VITTAL</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/8">KUDTHAMUGERU</a></li>


                                        
                                    </ul>
                                </li>  
                                </li>
                                <li><a href="<?php echo base_url();?>home/contact">CONTACT</a></li>

                                 </ul>
                            </div>
                        </nav>
                        
                        <!-- Main Menu End-->
                       
                        
                    </div>
                    <!--Nav Outer End-->
                    
                </div>    
            </div>
        </div>
    
        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container">
                <div class="sticky-inner-container clearfix">
                    <!--Logo-->
                    <div class="logo pull-left">
                        <a href="<?php echo base_url(); ?>home/" class="img-responsive"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" title=""></a>
                    </div>
                    
                    <!--Right Col-->
                    <div class="right-col pull-right">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                      <li class=" "><a href="<?php echo base_url(); ?>">Home</a> </li>
                                <li class="dropdown"><a href="#">ABOUT </a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>home/aboutus">SOCIETY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/committe">COMMITTEE</a></li>
                                        
                                    </ul>
                                </li>
                                <li ><a href="<?php echo base_url(); ?>home/service">SERVICES</a>                                  
                                </li>
                                 <li ><a href="<?php echo base_url(); ?>home/gallery">GALLERY</a>                                  
                                </li>
                              
                                <li ><a href="<?php echo base_url(); ?>news/public_news">NEWS</a>
                                   
                                </li>
                                <li><a href="<?php echo base_url(); ?>home/products">POTTERY PRODUCTS</a>
                                 <li class="dropdown"><a href="#">BRANCHES</a>
                                    <ul>
                                        
                                         <li><a href="<?php echo base_url(); ?>home/branches/1">PUTTUR (Head office)</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/9">B C ROAD</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/2">BELLARE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/3">GURUVAYANAKERE</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/4">KAWDICHARU</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/5">MANI</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/6">UPPINANGADY</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/7">VITTAL</a></li>
                                        <li><a href="<?php echo base_url(); ?>home/branches/8">KUDTHAMUGERU</a></li>

                                    </ul>
                                </li>  
                                </li>
                                <li><a href="<?php echo base_url();?>home/contact">CONTACT</a></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->
                        
                       
                    </div>
                    
                </div>
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->
    