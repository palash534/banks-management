 <!--Main Slider-->
    <section class="main-slider">
        
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
                <ul>
                    
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1687" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-4.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?php echo base_url(); ?>assets/images/main-slider/image-1.jpg"> 
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['550','650','650','450']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-190','-180','-150','-120']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":100,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="title-two text-center"></div>
                    </div>
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['750','750','700','450']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-60','-60','-50','-30']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":100,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <h2 class="style-two text-center"><span>ಕುಂಬಾರರ ಗುಡಿ ಕೈಗಾರಿಕಾ ಸಹಕಾರ ಸಂಘ(ನಿ.) ಪುತ್ತೂರು    </span></h2>
                    </div>
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['775','550','650','450']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-25','-25','0','0']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="border-image"><img src="<?php echo base_url(); ?>assets/images/main-slider/border.png" alt="" /></div>
                    </div>
                    
                    <div class="tp-caption tp-resizeme"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['550','550','650','450']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['130','100','70','90']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        
                    </div>
                    
                    </li>
                    
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?php echo base_url(); ?>assets/images/main-slider/image-2.jpg"> 
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['550','650','650','450']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-190','-180','-150','-120']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="title-two text-center"></div>
                    </div>
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['750','750','700','450']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-60','-60','-50','-30']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <h2 class="style-two text-center"><font style="color:#f09139;">GREAT BEGINING OF POTTERS COTTAGE INDUSTRY </font> </h2>
                    </div>
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['775','550','650','450']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-25','-25','0','0']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="border-image"><img src="<?php echo base_url(); ?>assets/images/main-slider/border.png" alt="" /></div>
                    </div>
                    
                    <div class="tp-caption tp-resizeme"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['550','550','650','450']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['130','100','70','90']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        
                    </div>
                    
                    </li>
                    
                </ul>
                
            </div>
        </div>
        <!--Scroll Dwwn Btn-->
        <div class="mouse-btn-down scroll-to-target" data-target=".experts-section"></div>
    </section>
    <!--End Main Slider-->
    
    <!--Experts Section-->
     <!--Welcome Section-->
    <section class="welcome-section">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Column-->
                <div class="content-column col-md-7 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <h2>ಕುಂಬಾರರ ಗುಡಿ ಕೈಗಾರಿಕಾ ಸಹಕಾರ ಸಂಘ (ನಿ.)<br><center> ಪುತ್ತೂರು </center></h2>
                        </div>
                        <div class="text">ಗ್ರಾಮೀಣ ಕುಂಬಾರಿಕೆ ಹಾಗೂ ಕುಶಲಕರ್ಮಿಗಳ ಅಭಿವೃದ್ಧಿಗಾಗಿಯೇ ಸ್ಥಾಪಿತವಾದ ನಮ್ಮಸಹಕಾರ ಸಂಘ ಈಗ ಎಲ್ಲಾ ವರ್ಗದ ಸದಸ್ಯರಿಗೂ ಸೇವೆಯನ್ನು ನೀಡುತ್ತಾ ಬಂದಿದೆ. ಕುಂಬಾರ ಸಮಾಜದ ಹಲವಾರು ಸಹಕಾರಿ ಧುರೀಣರು ತಮ್ಮ ನಿಸ್ವಾರ್ಥ, ವಿಶಿಷ್ಟ ಹಾಗೂ ಶ್ರದ್ದಾಪೂರ್ವಕ ಸೇವೆಯನ್ನು ಸಲ್ಲಿಸಿದುದರ ಫಲವಾಗಿ ಸಹಕಾರ ಸಂಘವು ಸರ್ವಾಂಗೀನ ಅಭಿವೃದ್ಧಿಯನ್ನು ಸಾಧಿಸಿ ಜನ ಮಾನಸದಲ್ಲಿ ಪ್ರೀತಿ, ವಿಶ್ವಾಸವನ್ನುಗಳಿಸಲು ಸಾಧ್ಯವಾಗಿದೆ. ಹಿರಿಯ ಸಹಕಾರಿ ಬಂಧುಗಳ ಸಹಕಾರ,ಸದಸ್ಯರ,ಗ್ರಾಹಕ ಬಂಧುಗಳ ಪ್ರೀತಿ,ವಿಶ್ವಾಸವೇ ನಮಗೆ ಶ್ರೀರಕ್ಷೆ.</div>
                        <div class="clearfix">
                            
                            <div class="pull-right">
                                <div class="phone"><span class="icon fa fa-phone"></span>08251-233274</div>
                            </div>
                        </div>
                    </div>
                </div>
               
                <!--Image Column-->
                <div class="image-column col-md-5 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/resource/welcome.jpg" alt="" />
                            <a href="https://www.youtube.com/watch?v=7W5CgCnO9Iw" class="lightbox-image play-btn">
                                <span class="icon fa fa-play"></span>
                            </a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Welcome Section-->
      <!--Services Section-->
    <section class="services-section">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title">
                <div class="title">OUR SERVICE </div>
                <h2>Save your money</h2>
            </div>
            <div class="row clearfix">
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/saving.jpg" alt="SAVINGS ACCOUNT" />
                            <div class="icon-box">
                                <span class="icon flaticon-employee"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-employee"></span>
                                             <h3><a href="<?php echo base_url();?>home/saving">SAVINGS ACCOUNT </a></h3>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/saving">SAVINGS ACCOUNT </a></h3>
                            <a href="<?php echo base_url();?>home/saving" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/currentaccount.jpg" alt="CURRENT ACCOUNT" />
                            <div class="icon-box">
                                <span class="icon flaticon-money"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-money"></span>
                                             <h3><a href="<?php echo base_url();?>home/currentAc">CURRENT ACCOUNT</a></h3>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/currentAc">CURRENT ACCOUNT</a></h3>
                            <a href="<?php echo base_url();?>home/currentAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/fixed.jpg" alt="FIXED DEPOSITS" />
                            <div class="icon-box">
                                <span class="icon flaticon-diagram"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-diagram"></span>
                                            <h3><a href="<?php echo base_url();?>home/fixedAc">FIXED DEPOSIT</a></h3>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/fixedAc">FIXED DEPOSIT</a></h3>
                            <a href="<?php echo base_url();?>home/fixedAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/recurring.jpg" alt="RECCURRING DEPOSITS" />
                            <div class="icon-box">
                                <span class="icon flaticon-pen"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-pen"> </span>
                                            <h3><a href="<?php echo base_url();?>home/reccuringAc">RECCURING DEPOSIT</a></h3>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/reccuringAc">RECCURING DEPOSIT</a></h3>
                            <a href="<?php echo base_url();?>home/reccuringAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/compolsary.jpg" alt="COMPULSARY DEPOSIT" />
                            <div class="icon-box">
                                <span class="icon flaticon-meeting"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-meeting"></span>
                                             <h3><a href="<?php echo base_url();?>home/compulsaryAc">COMPULSARY DEPOSIT</a></h3>

                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/compulsaryAc">COMPULSARY DEPOSIT</a></h3>
                            <a href="<?php echo base_url();?>home/compulsaryAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
           <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/dailydeposite.jpg" alt="daily deposite " />
                            <div class="icon-box">
                                <span class="icon flaticon-calendar"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-calendar"></span>
                                             <h3><a href="<?php echo base_url();?>home/dailyAc">DAILY DEPOSITES</a></h3>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/dailyAc">DAILY DEPOSITES</a></h3>
                            <a href="<?php echo base_url();?>home/dailyAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Services Section-->
               <div class="skills">
                            <div class="skill-item">
                               
                                <div class="skill-bar">
                                    <div class="bar-inner"><div class="bar progress-line" data-width="100"></div></div>
                                </div>
                            </div>
                </div> 
     <!--Report Section-->
    <section class="report-section">
        <div class="auto-container">

            <div class="row clearfix">
                 
                <!--Content Column-->
                <div class="content-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h2>E STAMP FACILITY </h2>
                            <div class="bold-text">
                                This service are available only at head office puttur and B.C road branch only.   
                             </div>
                        </div>
                        <div class="sec-title">
                             <h2>KUMBHA LAKSHMI CASH CERTIFICATE</h2>
                                <div class="bold-text">
                                    Get double of your investment in 90 months
                               </div> 
                          </div>
                          <div class="sec-title">
                             <h2>SPECIAL 0.5% </h2>
                                <div class="bold-text">
                               For senior citizens, widows and institutions special 0.5% extra rate of interest is available for the deposits over the period of 180 days.
                               </div> 
                          </div>
                    </div>
                         
                   
                </div>
                <!--Graph Column-->
                <div class="graph-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                         <div class="sec-title">
                             <h2>UNITED INDIA INSURANCE CO LTD </h2>
                                <div class="bold-text">
                              All kinds of vehicles insurance, housing insurance, shop and personal accidental insurance is available at a discounted rate.
                               </div> 
                          </div>
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/clients/insurence.jpg" alt="insurence" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Report Section-->
    <!--Approach Section-->
    <section class="approach-section" style="background-image:url(<?php echo base_url(); ?>assetsimages/background/2.jpg)">
        <div class="upper-box" style="background-image:url(<?php echo base_url(); ?>assets/images/background/1.jpg)">
            <div class="auto-container">
                <div class="title">  </div>
                <h2>Loans   </h2>
                <div class="quality"> We help you reach your dream</div>
            </div>
        </div>
        
        <div class="auto-container">
            <div class="blocks-section">
                <div class="row clearfix">
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-file"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/1">SURITY LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-file"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/1">SURITY LOAN</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-grain"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/2">JEWEL LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-grain"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/2">JEWEL LOAN</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-piggy-bank"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/3">DEPOSIT LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-piggy-bank"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/3">DEPOSIT LOAN </a></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon flaticon-meeting"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/4">CASH CREDIT LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon flaticon-meeting"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/4">CASH CREDIT LOAN</a></h4>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-home"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/5">HOUSING LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-home"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/5">HOUSING LOAN</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                      <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon  glyphicon glyphicon-globe"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/6">LAND PURCHASE LOAN<BR>LAND MORTGAGE LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon  glyphicon glyphicon-globe"></span>
                                    </div>
                                    <h4>
                                        <a href="<?php echo base_url();?>home/loan/6">LAND LOAN</a></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-bed"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/7">VEHICLE LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-bed"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/7">VEHICLE LOAN</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                       <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-education"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/8">MEMBERS CHILDREN EDUCATION LOANS  </a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-education"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/8">8.    MEMBERS CHILDREN EDUCATION LOANS</a></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                 
                       <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-registration-mark"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/9">SELF HELP GROUP LOANS</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-registration-mark"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/9">SELF HELP GROUP LOANS</a></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        


<!--president Section-->
    <section class="app-section-section">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Image Column-->
                <div class="image-column col-md-7 col-sm-12 col-xs-12">
                    <div class="image">
                        <img src="<?php echo base_url(); ?>assets/images/main-slider/president.jpg" alt="" />
                    </div>
                </div>
                <!--Content Column-->
                <div class="content-column col-md-5 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <div class="title">PRESIDENT</div>
                            <h2>ಶ್ರೀ ಭಾಸ್ಕರ ಎಂ.ಪೆರುವಾಯಿ </h2>
                        </div>
                        <div class="text">ಐದು ವರ್ಷದಲ್ಲಿ 1333 ಎ ದರ್ಜೆ ಸದಸ್ಯರು ಹಾಗು 20261 ಡಿ ದರ್ಜೆ ಸದಸ್ಯರನ್ನು ನೋಂದಾಯಿಸಲಾಗಿದೆ  ಹಾಗು ಪಾಲು ಬಂಡವಾಳ ಸಂಗ್ರಹಣೆಯಲ್ಲಿ ಗಮನಾರ್ಹ ಏರಿಕೆ ಕಂಡು ಬಂದಿದ್ದು ರೂ. 49 ಲಕ್ಷ ಇದ್ದ ಪಾಲು ಬಂಡವಾಳವನ್ನು ರೂ. 2.68 ಕೋಟಿಗೆ ಹೆಚ್ಚಿಸಲಾಗಿದೆ. ರೂ. 11 ಕೋಟಿ ಇದ್ದ ಠೇವಣಿಯನ್ನು ರೂ. 31 ಕೋಟಿ 61 ಲಕ್ಷಕ್ಕೆ ಹೆಚ್ಚಿಸಲಾಗಿದೆ. ಸಾಲ ನೀಡುವಿಕೆಯಲ್ಲಿಯೂ ಗಮನಾರ್ಹ ಸಾಧನೆ ಮಾಡಲಾಗಿದ್ದು  ರೂ. 13 ಕೋಟಿ 62 ಲಕ್ಷ ಇದ್ದ ಸಾಲವನ್ನು ರೂ. 29 ಕೋಟಿ 79 ಲಕ್ಷಕ್ಕೆ ಏರಿಸಲಾಗಿದೆ. ನಾವು ಅಧಿಕಾರ ವಹಿಸಿಕೊಳ್ಳುವಾಗ ಸಹಕಾರ ಸಂಘವು 7  ಶಾಖೆಗಳನ್ನುಹೊಂದಿದ್ದು ಮತ್ತು 2  ಶಾಖೆಗಳನ್ನು ತೆರೆದು ಶಾಖೆಗಳ ಸಂಖ್ಯೆ 9ಕ್ಕೆ ಏರಿಸಲಾಗಿದೆ.  </div>
                        <a href="#" class="theme-btn btn-style-four" data-toggle="modal" data-target="#myModal1">Read more</a>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End App Section-->
     <div class="skills">
                            <div class="skill-item">
                               
                                <div class="skill-bar">
                                    <div class="bar-inner"><div class="bar progress-line" data-width="100"></div></div>
                                </div>
                            </div>
                </div> 
     <!--Clients Section-->
    <section class="clients-section">
        <div class="auto-container">
            <div class="inner-column">
                        <div class="sec-title">
                            <div class="title">ACHIVEMENTS</div>
                            
                        </div>
                    </div>
            <div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li class="slide-item"><figure class="image-box"><a href="<?php echo base_url(); ?>assets/images/clients/001.jpg"><img src="<?php echo base_url(); ?>assets/images/clients/001.jpg" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="<?php echo base_url(); ?>assets/images/clients/002.jpg"><img src="<?php echo base_url(); ?>assets/images/clients/002.jpg" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="<?php echo base_url(); ?>assets/images/clients/003.jpg"><img src="<?php echo base_url(); ?>assets/images/clients/003.jpg" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="<?php echo base_url(); ?>assets/images/clients/004.jpg"><img src="<?php echo base_url(); ?>assets/images/clients/004.jpg" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="<?php echo base_url(); ?>assets/images/clients/005.jpg"><img src="<?php echo base_url(); ?>assets/images/clients/005.jpg" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="<?php echo base_url(); ?>assets/images/clients/006.jpg"><img src="<?php echo base_url(); ?>assets/images/clients/006.jpg" alt=""></a></figure></li>
                   
                </ul>
            </div>
            
        </div>
    </section>
    <!--End Clients Section-->
    
    
    
    
  
    <!--Message Section-->
    <section class="message-section">
        <div class="image-layer" style="background-image:url(<?php echo base_url(); ?>assets/images/background/8.jpg)"></div>
        <div class="auto-container">
            <div class="form-container" style="background-image:url(<?php echo base_url(); ?>assets/images/background/9.jpg)">
                
                <h2>Have any questions?</h2>
                <div class="title">Thank you very much for your interest in our co-operative society and our services and
if you have any questions, please write us a message now!</div>
                
                <!--Default Form-->
                <div class="default-form">
                    <form method="post" action="contact.html">
                        <div class="row clearfix">
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="firstname" value="" placeholder="Your name" required>
                            </div>
                            
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <input type="email" name="email" value="" placeholder="Your Email" required>
                            </div>
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <textarea name="message" placeholder="Your Massage"></textarea>
                            </div>
                            
                            <div class="form-group text-center col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="theme-btn message-btn">Send Massage</button>
                            </div>                                        
                        </div>
                    </form>
                </div>
                <!--End Default Form-->
                
            </div>
        </div>
    </section>
    <!--End Message Section-->
    


<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <!--   <button type="button" class="pull-right" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title"> PRESIDENT</h4>
        </div>
        <div class="modal-body">
          <div style="overflow: auto; width:100%; height:300px;">
          <p align="justify"><b><u>ಶ್ರೀ ಭಾಸ್ಕರ ಎಂ.ಪೆರುವಾಯಿ </u></b>
            <br>ಐದು ವರ್ಷದಲ್ಲಿ 1333 ಎ ದರ್ಜೆ ಸದಸ್ಯರು ಹಾಗು 20261 ಡಿ ದರ್ಜೆ ಸದಸ್ಯರನ್ನು ನೋಂದಾಯಿಸಲಾಗಿದೆ
 ಹಾಗು ಪಾಲು ಬಂಡವಾಳ ಸಂಗ್ರಹಣೆಯಲ್ಲಿ ಗಮನಾರ್ಹ ಏರಿಕೆ ಕಂಡು ಬಂದಿದ್ದು  ರೂ. 49 ಲಕ್ಷ ಇದ್ದ ಪಾಲು ಬಂಡವಾಳವನ್ನು ರೂ. 2.68 ಕೋಟಿಗೆ ಹೆಚ್ಚಿಸಲಾಗಿದೆ. ರೂ. 11 ಕೋಟಿ ಇದ್ದ ಠೇವಣಿಯನ್ನು ರೂ. 31 ಕೋಟಿ 61 ಲಕ್ಷಕ್ಕೆ ಹೆಚ್ಚಿಸಲಾಗಿದೆ. ಸಾಲ ನೀಡುವಿಕೆಯಲ್ಲಿಯೂ ಗಮನಾರ್ಹ ಸಾಧನೆ ಮಾಡಲಾಗಿದ್ದು  ರೂ. 13 ಕೋಟಿ 62 ಲಕ್ಷ ಇದ್ದ ಸಾಲವನ್ನು ರೂ. 29 ಕೋಟಿ 79 ಲಕ್ಷಕ್ಕೆ ಏರಿಸಲಾಗಿದೆ. ನಾವು ಅಧಿಕಾರ ವಹಿಸಿಕೊಳ್ಳುವಾಗ ಸಹಕಾರ ಸಂಘವು 7  ಶಾಖೆಗಳನ್ನುಹೊಂದಿದ್ದು ಮತ್ತು 2  ಶಾಖೆಗಳನ್ನು ತೆರೆದು ಶಾಖೆಗಳ ಸಂಖ್ಯೆ 9ಕ್ಕೆ ಏರಿಸಲಾಗಿದೆ.  ಸಹಕಾರ ಸಂಘದ ಎಲ್ಲಾ ಶಾಖೆಗಳನ್ನು ಸಂಪೂರ್ಣ ಗಣಕೀಕರಣಗೊಳಿಸಲಾಗಿದ್ದು ಎಸ್.ಎಂ.ಎಸ್ ವ್ಯವಸ್ಥೆ, ಆರ್.ಟಿ.ಜಿ. ಎಸ್/ಏನ್.ಈ.ಎಫ್.ಟಿ ಸೌಲಭ್ಯಗಳನ್ನು ಒದಗಿಸಲಾಗಿದೆ ಸಹಕಾರ ಸಂಘದ ಸದಸ್ಯರ ಸಾಲಗಳಿಗೆ ವಿಮ ಭದ್ರತೆಯನ್ನು ಯುನೈಟೆಡ್ ಇಂಡಿಯಾ ಇನ್ಶೂರೆನ್ಸ್  ಮೂಲಕ ಒದಗಿಸಿಕೊಟ್ಟಿರುತ್ತೇವೆ. ಕೇಂದ್ರಶಾಖೆ ಮತ್ತು ಬಿ.ಸಿ.ರೋಡ್ ಶಾಖೆಗಳಲ್ಲಿ ಈ-ಸ್ಟಾಂಪಿಂಗ್ ವ್ಯವಸ್ಥೆ ಕಲ್ಪಿಸಲಾಗಿದೆ. ಸಹಕಾರ ಸಂಘದ ಎಲ್ಲಾ ನಿತ್ಯನಿಧಿ ಸಂಗ್ರಾಹಕರಿಗೆ ನಿತ್ಯನಿಧಿ ಸಂಗ್ರಹಣಾ ಮೆಷಿನ್ ಕೊಡಿಸಲಾಗಿದೆ. ಶಾಖೆಗಳ ಭದ್ರತೆಯ ದ್ರಷ್ಠಿಯಿಂದ ಎಲ್ಲಾ ಶಾಖೆಗಳಲ್ಲಿ ಸಿಸಿ ಕೆಮೆರಾ ಅಳವಡಿಸಲಾಗಿದೆ. ಸಿಬ್ಬಂದಿಗಳಕೊರತೆ ನೀಗಿಸುವನಿಟ್ಟಿನಲ್ಲಿ 12 ಜನ ಹೊಸ ಸಿಬ್ಬಂದಿಗಳನ್ನು ನೇಮಕ ಮಾಡಿ ಖಾಯಂಗೊಳಿಸಲಾಗಿದೆ. ಸಿಬ್ಬಂದಿಗಳಿಗೆ ಉತ್ತಮ ವೇತನ ನೀಡುವ ಮೂಲಕ ಸಂತ್ರಿಪ್ತಿಯ ಭಾವವನ್ನು ಕಂಡಿದ್ದೇವೆ. ಸಹಕಾರ ಸಂಘದ ಎಲ್ಲಾ ಸಿಬ್ಬಂದಿಗಳಿಗೆ ಸಮವಸ್ತ್ರ ನೀಡಲಾಗಿದೆ. ಕೇಂದ್ರ ಶಾಖೆಯನ್ನು ನವೀಕರಗೊಳಿಸಿ ಹವಾನಿಯತ್ರಣಗೊಳಿಸಲಾಗಿದೆ. 40 ಜನಕುಂಬಾರ ಕುಶಲಕರ್ಮಿಗಳಿಗೆ ಆಧುನಿಕ ಕುಂಬಾರಿಕಾ ಚಕ್ರವನ್ನು ಧರ್ಮಾರ್ಥವಾಗಿ ವಿತರಿಸಲಾಗಿದೆ. ಸಹಕಾರ ಸಂಘದ ಸದಸ್ಯರ ಮಕ್ಕಳ ಉನ್ನತ ವಿದ್ಯಾಭ್ಯಾಸಕ್ಕಾಗಿ ಕಡಿಮೆ ಬಡ್ಡಿ ದರದಲ್ಲಿ  ಸಾಲವನ್ನು ವಿತರಿಸಲಾಗಿದೆ. ಸಹಕಾರ ಸಂಘದ ಇತಿಹಾಸದಲ್ಲೇ ಪ್ರಥಮ ಬಾರಿಗೆಸ್ವ-ಸಹಾಯ ಗುಂಪುಗಳನ್ನುರಚಿಸಿ ಆ ಮೂಲಕ ಗ್ರಾಮೀಣ ಜನರ ಅಭಿವೃದ್ದಿಗಾಗಿ ಸಹಕರಿಸಲಾಗಿದೆ. ಸಹಕಾರ ಸಂಘದ ಸಿಬ್ಬಂದಿಗಳಿಗೆಹಾಗೂ ನಿತ್ಯನಿಧಿ ಸಂಗ್ರಾಹಕರಿಗೆ ರೂ. 5 ಲಕ್ಷ ವೆಚ್ಚದ ಅಪಘಾತ ವಿಮಾ ಸೌಲಭ್ಯ ನೀಡಲಾಗಿದೆ. ಸಹಕಾರ ಸಂಘದ ಸದಸ್ಯರ ಅನುಕೂಲಕ್ಕಾಗಿ ಹಾಗೂ  ಸಾರ್ವಜನಿಕರ ಹಿತಕ್ಕಾಗಿ ಉಚಿತ ಆರೋಗ್ಯ ತಪಾಸಣಾ ಶಿಬಿರವನ್ನು ಸತತ 3  ವರ್ಷದಿಂದ
ನಡೆಸಲಾಗಿದೆ. ಮಾಣಿ ಶಾಖೆಗೆ ಸುಸಜ್ಜಿತ ಸ್ವಂತ ಶಾಖಾ ಕಟ್ಟಡವನ್ನು ನಿರ್ಮಿಸಲು ಕಾರ್ಯಾರಂಭಿಸಲಾಗಿದೆ. ಮೆಲ್ಕಾರ್ ಪ್ರದೇಶದ ಜನರಿಗೆ ಅನುಕೂಲವಾಗುವಂತೆ ಶಾಖೆ ತೆರೆಯಲು ಕಾರ್ಯಾರಂಭಿಸಲಾಗಿದೆ.  ತಮ್ಮೆಲ್ಲರ ಪ್ರೀತಿ ವಿಶ್ವಾಸ ಮತ್ತು ನನ್ನ ಆಡಳಿತ ಮಂಡಳಿ ಸದಸ್ಯರ ಸರ್ವ ರೀತಿಯ ಸಹಕಾರದಿಂದಾಗಿ ಈ ಸಾಧನೆ ಮಾಡಲು ಸಾಧ್ಯವಾಗಿದೆ.<br>

 2019 -20ನೇ ಸಾಲೀನ ಕಾರ್ಯಯೋಜನೆಗಳು:
 <ul>
<li>1 .ಕುಂಬಾರಿಕೆ ಕೈಗಾರಿಕೆಯ ಬಗ್ಗೆ ಕುಶಲ ಕರ್ಮಿಗಳಿಗೆ ಹಾಗೂ ಸಿಬ್ಬಂದಿಗಳಿಗೆ ವಿಶೇಷ ತರಬೇತಿಯನ್ನು </li>
    ಆಯೋಜಿಸುವುದು.
<li>2 . ಕುಂಬಾರಿಕೆಯ ಮಾರುಕಟ್ಟೆಯನ್ನು ಅಭಿವೃದ್ಧಿ ಪಡಿಸುವುದು.</li>
<li>3 . ಮಾಣಿ ಶಾಖೆಗೆ ಸುಸಜ್ಜಿತ ಕಟ್ಟಡವನ್ನು ರಚಿಸಿ ಶಾಖೆಯನ್ನು ಸ್ವಂತ ಕಟ್ಟಡಕ್ಕೆ ಸ್ಥಳಾಂತರಿಸುವುದು.
<li>4 .ಸದಸ್ಯರ ಅನುಕೂಲಕ್ಕಾಗಿ ಆರೊಗ್ಯ ಶಿಬಿರವನ್ನು ಏರ್ಪಡಿಸುವುದು . </li>
<li>5.ಕುಂಬಾರಿಕೆ ವೃತ್ತಿ ನಿರತ ಕುಂಬಾರ ಕುಶಲಕರ್ಮಿಗಳಿಗೆ ಇಳಿಸಿದ 9% ಬಡ್ಡಿದರದಲ್ಲಿ ಸಾಲ ನೀಡುವುದು.</li>
<li>6 .ಸದಸ್ಯರ ಮಕ್ಕಳಿಗೆ ಸದಸ್ಯರ ಮಕ್ಕಳ ವಿದ್ಯಾನಿಧಿಯಿಂದ 4% ಬಡ್ಡಿದರದಲ್ಲಿ ಉನ್ನತ ಶಿಕ್ಷಣಕ್ಕಾಗಿ ಸಾಲ ನೀಡುವುದು.</li>
<li>7 . ಮೆಲ್ಕಾರ್ ಪ್ರದೇಶದಲ್ಲಿ ಶಾಖೆ ತೆರೆಯುವುದು.   </li> 
<li>8.  ಕುಂಬಾರಿಕೆಯ ಉಚಿತ ನಿರಂತರ ತರಬೇತಿಗೆ ವ್ಯವಸ್ಥೆ ಮಾಡುವುದು </li>

          </p>
          
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     </div>
    </div>


    
   