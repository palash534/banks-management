<!--Content Side-->
					<div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div class="services-single">
							<div class="inner-box">
								
								<h3><u><?= $pageTitle ?></u></h3>
								<div class="text">
									<p><?= $details;  ?></p>
								
								</div>
							
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!--End Sidebar Page Container-->
	 <section class="call-to-action">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h3>If you have any query for related business... Just touch on this button
Contact Us</h3>
                <a href="<?php echo base_url();?>home/contact" class="contact-btn">Contact Us</a>
            </div>
        </div>
    </section>
    <!-- End Call To Action Section -->