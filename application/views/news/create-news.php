<!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/11.jpg)">
        <div class="auto-container">
            <h1>News</h1>
            <ul class="page-breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li>Create News</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--CheckOut Page-->
    <div class="checkout-page" style="padding: 0px;">
        <div class="auto-container">
            <?php if($this->session->flashdata('flsh_msg')) :?>
                <?php if($this->session->flashdata('flsh_msg')['status'] == 'error'):?>
                    <?php echo implode(" , ", $this->session->flashdata('flsh_msg')['error_desc']); ?>
                <?php endif;?>
                <?php if($this->session->flashdata('flsh_msg')['status'] == 'success'):?>
                    <?php echo $this->session->flashdata('flsh_msg')['success_msg']; ?>
                <?php endif;?>
            <?php endif;?>    
            <!--Checkout Details-->
            <div class="checkout-form">
                <form method="post" action="<?=base_url()?>news/saveNews" enctype="multipart/form-data">
                    <div class="row clearfix">
                        <!--Column-->
                        <div class="column col-md-12 col-sm-12 col-xs-12">
                            <div class="checkout-title">
                                <h2>News Details</h2>
                            </div>
                            <div class="row clearfix">
                                <div class="row clearfix">
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="field-label">Title <sup>*</sup></div>
                                        <input type="text" name="title" value="" placeholder="">
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8 col-sm-8 col-xs-8">
                                        <div class="field-label">News Content </div>
                                        <textarea rows="20" name="content" style=""></textarea>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div style="">
                                            <img style="margin: 15% 15% 15% 17%;" height="500em" width="250em" id="preview" src="<?=base_url()?>/assets/images/background/1.jpg" alt="your image" class="preview-image">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8 col-sm-8 col-xs-8">
                                        <div class="field-label">News Date</div>
                                        <input type="text" name="news_date" value="" placeholder="yyyy-mm-dd">
                                    </div>  
                                    <div class="col-md-4 col-sm-4 col-xs-4 text-center" >
                                        <div style="height:0px;overflow:hidden">
                                           <input type="file" id="fileInput" name="image[]" accept="image/*"/>
                                        </div><br>
                                        <button class="theme-btn btn-style-one" type="button" onclick="chooseFile();">choose file</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group text-center">
                                        <button class=" btn-style-four" type="submit">SUBMIT</button>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--End Checkout Details-->
        </div>
    </div>
    <!--End CheckOut Page-->

    <script>
        function chooseFile() {
          $("#fileInput").click();
        }
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(function(){
            $("#fileInput").change(function() {
                readURL(this);
            });
        });
    </script>
