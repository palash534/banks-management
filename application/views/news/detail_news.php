<!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/11.jpg)">
    	<div class="auto-container">
        	<h1>News Detail</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index.html">Home</a></li>
                <li>News Detail</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
				
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<div class="blog-single">
						<div class="inner-box">
                        	<div class="image">
                            	<img style="width: 770px; height: 500px" src="<?php echo base_url();?>/upload/<?=$data['IMAGE_PATH']?>" alt="" />
                                <div class="post-date"><span><?=date('d',strtotime($data['NEWS_DATE']))?></span><?=date('M',strtotime($data['NEWS_DATE']))?></div>
                            </div>
                            <h2><?=$data['TITLE']?></h2>
                            <ul class="post-meta">
                            	<li><span class="icon fa fa-commenting-o"></span>02</li>
                                <li><span class="icon fa fa-user"></span>Admin</li>
                                <li><span class="icon fa fa-heart-o"></span>03</li>
                            </ul>
                            <div class="text">
                            	<p><?=$data['CONTENT']?></p>
                                <blockquote style="background-image:url(images/background/12.jpg)">
                                	<div class="text">Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource and leveling customer service for state of the art customer service.</div>
                                </blockquote>
                            </div>
                            <!--post-share-options-->
                            <div class="post-share-options">
                                <div class="tags">
                                	<span class="tag">POST TAGS:</span>
                                    <a href="#">Entertanment</a>
                                    <a href="#">Logo</a>
                                    <a href="#">Design</a>
                                    <a href="#">Business</a>
                                    <a href="#">Development</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Author Box-->
                    <div class="author-box">
                        <div class="inner-box">
                        	<div class="content">
                                <div class="image">
                                    <img src="images/resource/author-2.jpg" alt="" />
                                </div>
                                <h3>Willium Ranto</h3>
                                <div class="text">Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </div>
                                <ul class="social-icon-three">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                    <li><a href="#"><span class="fa fa-behance"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--End Author Box-->
                    
                    <!--Comments Area-->
                    <div class="comments-area">
                        
                        <div class="group-title">
                            <h2>Comments 02</h2>
                        </div>
                        
                        <!--Comment Box-->
                        <div class="comment-box">
                            <div class="comment">
                                <div class="author-thumb"><img src="images/resource/author-4.jpg" alt=""></div>
                                <div class="comment-inner">
                                    <div class="comment-info clearfix"><strong>Paul Jones</strong><div class="comment-time">August 29, 2017</div></div>
                                    <div class="text">Cosmic ocean science Tunguska event the only home we’ve ever known Orion’s sword, concept of the one billions upon billions paroxysm of global death.</div>
                                    <a class="comment-reply" href="#">Reply</a>
                                </div>
                            </div>
                        </div>
                        
                        <!--Comment Box-->
                        <div class="comment-box reply-comment">
                            <div class="comment">
                                <div class="author-thumb"><img src="images/resource/author-5.jpg" alt=""></div>
                                <div class="comment-inner">
                                    <div class="comment-info clearfix"><strong>Catherine Brown</strong><div class="comment-time">August 29, 2017</div></div>
                                    <div class="text">Cosmic ocean science Tunguska event the only home we’ve ever known Orion’s sword, concept of the one billions upon billions paroxysm of global death.</div>
                                    <a class="comment-reply" href="#">Reply</a>
                                </div>
                            </div>
                        </div>
                        
					</div>
                    
                    <!-- Comment Form -->
                    <div class="comment-form">
                        <div class="group-title">
                        	<h2>Leave a Reply</h2>
                        </div>
                        <!--Comment Form-->
                        <form method="post" action="blog.html">
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="username" placeholder="Name" required>
                                </div>
                                
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <input type="email" name="email" placeholder="Email" required>
                                </div>
                                
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="text" placeholder="Subject" required>
                                </div>
                                
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <textarea name="message" placeholder="Massage"></textarea>
                                </div>
                                
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form">Post Comments</button>
                                </div>
                                
                            </div>
                        </form>
                            
                    </div>
                    <!--End Comment Form -->
                    
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar">
						
                        <!--search box-->
                        <div class="sidebar-widget search-box">
                            <form method="post" action="blog.html">
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search...." required="">
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>
                        
                        <!-- Categories -->
                        <div class="sidebar-widget categories">
                            <div class="sidebar-title"><h2>Categories</h2></div>
                            <ul class="category-list">
                                <li><a href="#">Business <span>(6)</span></a></li>
                                <li><a href="#">Corporate <span>(9)</span></a></li>
                                <li><a href="#">Portfolio <span>(3)</span></a></li>
                                <li><a href="#">Finince <span>(5)</span></a></li>
                                <li><a href="#">Investment planning <span>(7)</span></a></li>
                            </ul>
                        </div>
                        
                        <!-- Popular Posts -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title"><h2>Popular Posts</h2></div>

                            <article class="post">
                            	<figure class="post-thumb"><img src="images/resource/post-thumb-1.jpg" alt=""><a href="blog-detail.html" class="overlay-box"><span class="icon fa fa-link"></span></a></figure>
                                <div class="text"><a href="blog-detail.html">Our operations worldwide have been neutral.</a></div>
                                <div class="post-info">June 21, 2017</div>
                            </article>

                            <article class="post">
                            	<figure class="post-thumb"><img src="images/resource/post-thumb-2.jpg" alt=""><a href="blog-detail.html" class="overlay-box"><span class="icon fa fa-link"></span></a></figure>
                                <div class="text"><a href="blog-detail.html">8 Pricing strategies for your digital product.</a></div>
                                <div class="post-info">June 21, 2017</div>
                            </article>
                            
                            <article class="post">
                            	<figure class="post-thumb"><img src="images/resource/post-thumb-3.jpg" alt=""><a href="blog-detail.html" class="overlay-box"><span class="icon fa fa-link"></span></a></figure>
                                <div class="text"><a href="blog-detail.html">A digital prescription for the pharma industry.</a></div>
                                <div class="post-info">June 21, 2017</div>
                            </article>
                            
                        </div>
                        
                        <!-- Instagram Post -->
                        <div class="sidebar-widget instagram-widget">
                            <div class="sidebar-title"><h2>Instagram</h2></div>
                           	<div class="images-outer clearfix">
                                <!--Image Box-->
                                <figure class="image-box"><a href="images/gallery/1.jpg" class="lightbox-image" data-caption="" data-fancybox="images" title="Image Title Here" data-fancybox-group="footer-gallery"><span class="overlay-box flaticon-plus"></span></a>
                                <img src="images/gallery/instagram-1.jpg" alt=""></figure>
                                <!--Image Box-->
                                <figure class="image-box"><a href="images/gallery/2.jpg" class="lightbox-image" data-caption="" data-fancybox="images" title="Image Title Here" data-fancybox-group="footer-gallery"><span class="overlay-box flaticon-plus"></span></a>
                                <img src="images/gallery/instagram-2.jpg" alt=""></figure>
                                <!--Image Box-->
                                <figure class="image-box"><a href="images/gallery/3.jpg" class="lightbox-image" data-caption="" data-fancybox="images" title="Image Title Here" data-fancybox-group="footer-gallery"><span class="overlay-box flaticon-plus"></span></a>
                                <img src="images/gallery/instagram-3.jpg" alt=""></figure>
                                <!--Image Box-->
                                <figure class="image-box"><a href="images/gallery/4.jpg" class="lightbox-image" data-caption="" data-fancybox="images" title="Image Title Here" data-fancybox-group="footer-gallery"><span class="overlay-box flaticon-plus"></span></a>
                                <img src="images/gallery/instagram-4.jpg" alt=""></figure>
                                <!--Image Box-->
                                <figure class="image-box"><a href="images/gallery/1.jpg" class="lightbox-image" data-caption="" data-fancybox="images" title="Image Title Here" data-fancybox-group="footer-gallery"><span class="overlay-box flaticon-plus"></span></a>
                                <img src="images/gallery/instagram-5.jpg" alt=""></figure>
                                <!--Image Box-->
                                <figure class="image-box"><a href="images/gallery/2.jpg" class="lightbox-image" data-caption="" data-fancybox="images" title="Image Title Here" data-fancybox-group="footer-gallery"><span class="overlay-box flaticon-plus"></span></a>
                                <img src="images/gallery/instagram-6.jpg" alt=""></figure>
                            </div>
                        </div>
                        
                        <!-- Popular Tags -->
                        <div class="sidebar-widget popular-tags">
                            <div class="sidebar-title">
                            	<h2>Tag Cloud</h2>
                            </div>
                            <a href="#">Design</a>
                            <a href="#">Fashion</a>
                            <a href="#">Money</a>
                            <a href="#">Entertanment</a>
                            <a href="#">Logo</a>
                            <a href="#">Business</a>
                            <a href="#">Development</a>
                        </div>
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    
    <!-- Call To Action Section -->
    <section class="call-to-action">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h3>If you have any query for related business... Just touch on this button</h3>
                <a href="contact.html" class="contact-btn">Contact Us</a>
            </div>
        </div>
    </section>
    <!-- End Call To Action Section -->