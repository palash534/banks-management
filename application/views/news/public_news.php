<!--Page Title
    <section class="page-title" style="background-image:url(images/background/11.jpg)">
    	<div class="auto-container">
        	<h1>Latest News</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index.html">Home</a></li>
                <li>Blog</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Blog Modern Section-->
    <section class="blog-modern-section">
    	<div class="auto-container">
            <?php $i=0;?>
            <?php foreach($results as $value):?>
                <?php $i++;?>
                <?php if($i%2==1):?>
                <!--News Block Three-->
                    <div class="news-block-three">
                    	<div class="clearfix">
                        	<!--Image Column-->
                        	<div class="image-column col-md-6 col-sm-12 col-xs-12">
                            	<div class="image">
                                	<a href="<?=base_url('/news/detail_news/'.$value['ID'])?>"><img style="height:400px ;width: 600px" src="<?php echo base_url();?>/upload/<?=$value['IMAGE_PATH']?>" alt="" /></a>
                                </div>
                            </div>

                            <!--Content Column-->
                        	<div class="content-column col-md-6 col-sm-12 col-xs-12">
                            	<div class="inner-column">
                                	<div class="post-date"><?=date('d',strtotime($value['NEWS_DATE']))?><span><?=date('M',strtotime($value['NEWS_DATE']))?></span></div>
                                	<h2><a href="<?=base_url('/news/detail_news/'.$value['ID'])?>"><?=$value['TITLE']?></a></h2>
                                    <div class="text"><?=$value['CONTENT']?></div>
                                    <ul class="post-meta">
                                    	
                                        <li><a href="blog-detail.html"><span class="icon fa fa-user"></span>Admin</a></li>
                                          
                                        
                                    </ul>
                                    <div class="fb-share-button" data-href="http://pcics.in/news/public_news" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fpcics.in%2Fnews%2Fpublic_news&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else:?>
                <!--News Block Four-->
                    <div class="news-block-four">
                    	<div class="clearfix">
                        	<!--Image Column-->
                        	<div class="image-column pull-right col-md-6 col-sm-12 col-xs-12">
                            	<div class="image">
                                	<a href="<?=base_url('/news/detail_news/'.$value['ID'])?>"><img src="<a href="#"><img style="height:400px ;width: 600px" src="<?php echo base_url();?>/upload/<?=$value['IMAGE_PATH']?>" alt="" /></a>
                                </div>
                            </div>
                            <!--Content Column-->
                        	<div class="content-column pull-left col-md-6 col-sm-12 col-xs-12">
                            	<div class="inner-column">
                                	<div class="post-date"><?=date('d',strtotime($value['NEWS_DATE']))?><span><?=date('M',strtotime($value['NEWS_DATE']))?></span></div>
                                	<h2><a href="<?=base_url('/news/detail_news/'.$value['ID'])?>"><?=$value['TITLE']?></a></h2>
                                    <div class="text"><?=$value['CONTENT']?></div>
                                    <ul class="post-meta">
                                    	
                                        <li><span class="icon fa fa-user"></span>Admin
                                            

                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            <?php endforeach;?>           
            <!--Styled Pagination-->
            <ul class="styled-pagination text-center">
                <?=$links[0]?>
            </ul>                
            <!--End Styled Pagination-->
            
        </div>
    </section>
    <!--End Modern Section Section-->
    
    