<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-dashboard"></i> Create News</h1>
      <p>Create News</p>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">Create News</a></li>
    </ul>
  </div>
  <div class="row">
    
      <?php if($this->session->flashdata('flsh_msg')) :?>
                <?php if($this->session->flashdata('flsh_msg')['status'] == 'error'):?>
                      <div class="alert alert-danger col-md-12 tile" id="danger-alert">
                          <button id="danger-close" type="button" class="close" data-dismiss="alert">x</button>
                          <p><strong>Error! </strong>
                            <?php if(is_array($this->session->flashdata('flsh_msg')['error_desc'])):?>
                              <?php echo implode(" , ", $this->session->flashdata('flsh_msg')['error_desc']); ?>
                            <?php else:?>
                              <?php echo $this->session->flashdata('flsh_msg')['error_desc']?>
                            <?php endif;?>
                          </p>
                      </div>
                <?php endif;?>
                <?php if($this->session->flashdata('flsh_msg')['status'] == 'success'):?>
                    <div class="alert alert-danger col-md-12 tile" id="danger-alert">
                          <button id="danger-close" type="button" class="close" data-dismiss="alert">x</button>
                          <p><strong>Success! </strong>
                            <?php echo $this->session->flashdata('flsh_msg')['success_msg']; ?>
                          </p>
                      </div>
                <?php endif;?>
            <?php endif;?>
    
    <div class="col-md-12">
      <div class="tile">
        <form method="post" action="<?=base_url()?>product/saveProduct" enctype="multipart/form-data">
          <div class="tile-body">
          
            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Product Name</label>
                    <input class="form-control" name="product_name" id="product_name" type="text" aria-describedby="emailHelp" placeholder="Enter title">
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Product Description</label>
                    <textarea class="form-control" id="content" rows="5" name="content"></textarea>
                  </div>
                </div>
                <div class="col-md-4 text-center align-middle">
                  <img style="height:200px; width:300px" id="preview" src="<?=base_url()?>/assets/images/background/1.jpg" alt="your image" class="preview-image">
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Categories</label>
                  <input class="form-control" name="categories" id="categories" type="text" aria-describedby="emailHelp" placeholder="Enter title">
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Price</label>
                  <input class="form-control" name="price" id="price" type="text" aria-describedby="emailHelp" placeholder="Enter title">
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Size</label>
                  <input class="form-control" name="size" id="size" type="text" aria-describedby="emailHelp" placeholder="Enter title">
                </div>
                <div class="col-md-3 text-center align-middle">
                  <div style="height:0px;overflow:hidden">
                     <input type="file" id="fileInput" name="image[]" accept="image/*"/>
                  </div><br>
                  <button class="btn btn-default" type="button" onclick="chooseFile();"><i class="fa fa-fw fa-lg fa-file-image-o"></i>Choose File</button>
                </div>
            </div>
          
        </div>
        <div class="tile-footer text-right">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
            </div>
        </form>
      </div>
    </div>
  </div>
</main>

<script>
        function chooseFile() {
          $("#fileInput").click();
        }
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
            $("#fileInput").change(function() {
                readURL(this);
            });
            $('.datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd',
            });
        });
    </script>
