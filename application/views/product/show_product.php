
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css"><main class="app-content">
    <div class="app-title">
      <div>
        <h1><i class="fa fa-edit"></i> News </h1>
        <p>List of News</p>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">News</li>
      </ul>
    </div>
    <div class="row">
        
              <?php if($this->session->flashdata('flsh_msg')) :?>
                <?php if($this->session->flashdata('flsh_msg')['status'] == 'error'):?>
                      <div class="alert alert-danger col-md-12 tile" id="danger-alert">
                          <button id="danger-close" type="button" class="close" data-dismiss="alert">x</button>
                          <p><strong>Error! </strong>
                            <?php if(is_array($this->session->flashdata('flsh_msg')['error_desc'])):?>
                              <?php echo implode(" , ", $this->session->flashdata('flsh_msg')['error_desc']); ?>
                            <?php else:?>
                              <?php echo $this->session->flashdata('flsh_msg')['error_desc']?>
                            <?php endif;?>
                          </p>
                      </div>
                <?php endif;?>
                <?php if($this->session->flashdata('flsh_msg')['status'] == 'success'):?>
                    <div class="alert alert-danger col-md-12 tile" id="danger-alert">
                          <button id="danger-close" type="button" class="close" data-dismiss="alert">x</button>
                          <p><strong>Success! </strong>
                            <?php echo $this->session->flashdata('flsh_msg')['success_msg']; ?>
                          </p>
                      </div>
                <?php endif;?>
            <?php endif;?>
      <div class="col-md-12">
        <div class="tile">
          <div style="width:auto;overflow-x:scroll;">
            <table class="table table-hover table-bordered" id="memberList" width="100%">
            <thead>
              <tr width="100%">
                <th class="text-center" width="2%">Sl.No.</th>
                <th class="text-center" width="26%">Title</th>
                <th class="text-center" width="12%">Date</th>
                <th class="text-center" width="30%">Contnt</th>
                <th class="text-center" width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
                <?php $i=1;?>
                <?php foreach ($data as $value) : ?>
                  <tr>
                    <td><?=$i?></td>
                    <td><?=$value['TITLE']?></td>
                    <td><?=$value['NEWS_DATE']?></td>
                    <td><?=$value['CONTENT']?></td>
                    <td>
                      <a href="<?=base_url()?>news/edit/<?=$value['ID']?>"><button class="btn btn-secondary"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>
                      <button class="btn btn-danger" type="button" onclick="doDelete('<?=$value["ID"]?>')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </td>
                  </tr>
                  <?php $i++;?>
                <?php endforeach;?>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>
  </main>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript">
    $(function(){
      $('#memberList').dataTable();
    });
    function doDelete(encrypted_string){
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            var form_string="";
            form_string+='<form method="post" action="<?=base_url()?>news/deleteNews"> <input type="hidden" name="id" value="';
            form_string+=encrypted_string+'"></form>';
            $(form_string).appendTo('body').submit();
          } else {
            
          }
        });
      }

  </script>