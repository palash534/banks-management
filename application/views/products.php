 <!--Default Form Section-->
    <section class="default-form-section">
    	<div class="auto-container">
        	<!--Title Box-->
        	<div class="title-box">
                <h2>Pottery  Products</h2>
				<div class="image">
				<img src="<?php echo base_url(); ?>assets/images/pottery-service/products.jpg" alt="">
				</div>
                <div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li class="slide-item"><figure class=""><a href="<?php echo base_url(); ?>home/shop"><img src="<?php echo base_url(); ?>assets/images/resource/products/1.jpg" alt=""></a></figure></li>
                    <li class="slide-item"><figure class=""><a href="<?php echo base_url(); ?>home/shop"><img src="<?php echo base_url(); ?>assets/images/resource/products/2.jpg" alt=""></a></figure></li>
                     <li class="slide-item"><figure class=""><a href="<?php echo base_url(); ?>home/shop"><img src="<?php echo base_url(); ?>assets/images/resource/products/3.jpg" alt=""></a></figure></li>
                      <li class="slide-item"><figure class=""><a href="<?php echo base_url(); ?>home/shop"><img src="<?php echo base_url(); ?>assets/images/resource/products/4.jpg" alt=""></a></figure></li>
                      
                   
                </ul>
            </div>
                
                                <a href="<?php echo base_url(); ?>home/shop" class="theme-btn btn-style-four">Buy Now</a>

		    </div>
			 
         
        </div>
    </section>
      
    <!--Contact Info Section-->
    <section class="contact-info-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <div class="column col-md-4 col-sm-6 col-xs-12">
                	<ul>
                    	<li>
                            <span class="icon flaticon-placeholder"></span>
                               Pottery society Co-operative bank Putture (Head office)

                       </li>
                    </ul>
                </div>
                
                <div class="column col-md-4 col-sm-6 col-xs-12">
                	<ul>
                    	<li>
                            <span class="icon flaticon-technology-2"></span>
              
                              08251-236274 / 08251-233274
                             <br> pcicsputtur@gmail.com
                        </li>
                    </ul>
                </div>
                
                <div class="column col-md-4 col-sm-6 col-xs-12">
                	<ul>
                    	<li>
                            <span class="icon flaticon-clock-2"></span>
                           Mon To Fri 9:15 am-1:30 pm <br>
						   2:15 pm - 5:30 pm <br>
                           2nd Sat 09-15 am To 01-30 pm
                          <br> Sunday Closed
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Contact Info Section-->
    
   
    