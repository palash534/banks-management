        <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<div class="services-single">
						<div class="inner-box">
							
                            <div class="text">
                                <div class="two-column row clearfix">
                                	<div class="column col-md-8 col-sm-8 col-xs-12">
                                    	<h3><u>RECCURING DEPOSIT</u></h3>
                                        <b>A recurring deposit is a special kind of term deposit offered by us which help people with regular incomes to deposit a fixed amount every month into their recurring deposit account and earn interest at the rate applicable. This deposit matures on a specific date in the future along with all the deposits made every month.     </p>
                                           <ul> <li> •  The applicant should be a member.</li>
                                            <li>•   The person desiring to open RD account shall furnish passport size photograph and Address proof.</li>
                                            <li>•   RD Accounts can be opened in multiple of Rs.100 per month.</li>
                                            <li>•   7%  rate of interest will be paid to the deposit.</li>
                                            <li>•   RD account holders will be given RD cards.</li></ul>
                               
                                    </div>
                                   
                                </div>
                               
                            </div>
             
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->
           <!-- Call To Action Section -->
   <section class="call-to-action">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h3>If you have any query for related business... Just touch on this button
Contact Us</h3>
                <a href="<?php echo base_url();?>home/contact" class="contact-btn">Contact Us</a>
            </div>
        </div>
    </section>
    <!-- End Call To Action Section -->