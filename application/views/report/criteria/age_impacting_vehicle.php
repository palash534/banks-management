<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-8">
                                <h3 class="widget-user-username" id="report_name"><b>According To Age of Impacting Vehicles</b></h3>
                                <h5 class="widget-user-desc">List All According To Age of Impacting Vehicles</h5>
                            </div>
                            <div class="col-md-4" style="padding-right:0px">
                                <div class="row" style="margin-top:60px;">
                                    <div class="col-md-12">
                                        <div id="reportrange" style="width:65%;float:left;background: #fff; cursor: pointer; padding: 6px 10px; border: 1px solid #ccc;">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                        <button type="button" class="btn btn-info" style="background-color:#3c8dbc;float:left"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive" style="overflow-x:auto">
                            <table id="excel_export" class="table table-bordered table-striped dataTable" style="border:2px solid #3c8dbc;overflow-y: scroll; height:400px;">
                                <thead>
                                    <tr>
                                        <th rowspan="3"></th>
                                        <th rowspan="3">Age of Vehicles <br> (Year)</th>
                                        <th colspan="5">Number of Accidents</th>
                                        <th colspan="3">Number of persons</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">Fatal</th>
                                        <th rowspan="2">Grievous <br> (need hospitalisation)</th>
                                        <th rowspan="2">Minor<br> (not needing hospitalisation)</th>
                                        <th rowspan="2">Non</th>
                                        <th rowspan="2">Total</th>
                                        <th rowspan="2">Killed</th>
                                        <th colspan="2">Injured</th>
                                    </tr>
                                    <tr>
                                        <th>Greviously<br> Injured</th>
                                        <th>Minor <br> Injured</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_posts8">
                                    <tr>
                                        <td></td>
                                        <td>Less than 5 years</td>            
                                        <td>0</td>
                                        <td>2</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>2</td>
                                        <td>2</td> 
                                        <td>2</td> 
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>5 – 10 years</td>            
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td> 
                                        <td>0</td> 
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>10.1 - 15 years</td>            
                                        <td>0</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>2</td>
                                        <td>0</td> 
                                        <td>0</td> 
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&gt; 15 years</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td> 
                                        <td>0</td> 
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Age Not Known</td>            
                                        <td>6</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>0</td>
                                        <td>12</td>
                                        <td>2</td> 
                                        <td>2</td> 
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><b>Total</b></td>
                                        <td>6</td>
                                        <td>6</td>
                                        <td>5</td>
                                        <td>0</td>  
                                        <td><b>17</b></td>
                                        <td>4</td>
                                        <td>4</td>
                                        <td>0</td>
                                    </tr>        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-8">
                                <h3 class="widget-user-username" id="report_name"><b>Yearly Accident Report</b></h3>
                                <h5 class="widget-user-desc">Fatal, Grievous, Minor, Non: Year <span>2019</span></h5>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div style="width: 100%">
                            <canvas id="canvas"></canvas>
                        </div>
                        <button id="randomizeData">Randomize Data</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>-->
<!--Not Needed Actually. Only for demo data-->
<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" type="text/javascript"></script>

<script src="https://www.chartjs.org/samples/latest/utils.js" type="text/javascript"></script>

<script>
  $(function(){
    $('.small-box-footer').click(function(){
      $("#collapse").slideToggle("slow");
    });

    //Date range as a button
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
            '1 Day':[moment(),moment()],
            '1 Week':[moment().subtract('week',1),moment()],
            '1 Month':[moment().subtract('month',1),moment()],
            '6 Month':[moment().subtract('month',6),moment()],
            '1 Year':[moment().subtract('year',1),moment()],
            '5 Year':[moment().subtract('year',5),moment()]
        }
    }, cb);

    cb(start, end);
  });
  
  var barChartData = {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'Dataset 1',
				backgroundColor: window.chartColors.red,
				stack: 'Stack 0',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 2',
				backgroundColor: window.chartColors.blue,
				stack: 'Stack 0',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 3',
				backgroundColor: window.chartColors.green,
				stack: 'Stack 1',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};
		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Chart.js Bar Chart - Stacked'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			barChartData.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});
			});
			window.myBar.update();
		});
  
</script>