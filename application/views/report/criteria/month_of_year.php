<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-8">
                                <h3 class="widget-user-username" id="report_name"><b>According To Month Of The Year of Year 2019</b></h3>
                                <h5 class="widget-user-desc">List According To Month Of The Year </h5>
                            </div>
                            <div class="col-md-4" style="padding-right:0px">
                                <div class="row" style="margin-top:60px;">
                                    <div class="col-md-12">
                                        <div id="reportrange" style="width:65%;float:left;background: #fff; cursor: pointer; padding: 6px 10px; border: 1px solid #ccc;">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                        <button type="button" class="btn btn-info" style="background-color:#3c8dbc;float:left"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive" style="overflow-x:auto">
                            <table id="" class="table table-bordered table-striped dataTable" style="border:2px solid #3c8dbc;overflow-y: scroll; height:400px;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th colspan="5">Number Of Accident</th>
                                        <th colspan="4">Number of Persons involved by<br>type of Accidents</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>Month</th>
                                        <th>Fatal Accident</th>
                                        <th>Grevious Accident <br> (need hospitalisation)</th>
                                        <th>Minor Accident <br> (not needing hospitalisation)</th>
                                        <th>No-Injury Accident</th>
                                        <th>Total</th>
                                        <th>Killed</th>
                                        <th> Grevious Injury<br> (needhospitalisation)</th>
                                        <th>Minor Injury<br> (not needing hospitalisation)</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_posts">
                                    <tr>
                                        <td></td>            
                                        <td>January</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>February</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>March</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>April</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>May</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>June</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>July </td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>August</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->

                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>September</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>October</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>November</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>            
                                        <td>December</td>            
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    <!--  End -->
                                    </tr>
                                    <tr>
                                        <td></td>  
                                        <td><b>TOTAL</b></td>  
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>  
                                        <!-- Please go through this new section of code used for the injured people purpose -->
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <!-- End  -->
                                    </tr>     
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-8">
                                <h3 class="widget-user-username" id="report_name"><b>Yearly Accident Report</b></h3>
                                <h5 class="widget-user-desc">Fatal, Grievous, Minor, Non: Year <span>2019</span></h5>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div style="width: 100%">
                            <canvas id="canvas"></canvas>
                        </div>
                        <button id="randomizeData">Randomize Data</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>-->
<!--Not Needed Actually. Only for demo data-->
<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" type="text/javascript"></script>

<script src="https://www.chartjs.org/samples/latest/utils.js" type="text/javascript"></script>

<script>
  $(function(){
    $('.small-box-footer').click(function(){
      $("#collapse").slideToggle("slow");
    });

    //Date range as a button
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
            'Current Year':[moment().startOf('year'),moment().endOf('year')],
            '2 Year':[moment().subtract('years', 2).startOf('year'),moment().endOf('year')],
            '3 Year':[moment().subtract('years', 3).startOf('year'),moment().endOf('year')],
            '4 Year':[moment().subtract('years', 4).startOf('year'),moment().endOf('year')],
            '5 Year':[moment().subtract('years', 5).startOf('year'),moment().endOf('year')]
        }
    }, cb);

    cb(start, end);
  });
  
  var barChartData = {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'Dataset 1',
				backgroundColor: window.chartColors.red,
				stack: 'Stack 0',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 2',
				backgroundColor: window.chartColors.blue,
				stack: 'Stack 0',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 3',
				backgroundColor: window.chartColors.green,
				stack: 'Stack 1',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};
		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Chart.js Bar Chart - Stacked'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			barChartData.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});
			});
			window.myBar.update();
		});
  
</script>