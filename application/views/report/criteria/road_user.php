<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-8">
                                <h3 class="widget-user-username" id="report_name"><b>According To Type of Road User</b></h3>
                                <h5 class="widget-user-desc">List All According To Type of Road User</h5>
                            </div>
                            <div class="col-md-4" style="padding-right:0px">
                                <div class="row" style="margin-top:60px;">
                                    <div class="col-md-12">
                                        <div id="reportrange" style="width:65%;float:left;background: #fff; cursor: pointer; padding: 6px 10px; border: 1px solid #ccc;">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                        <button type="button" class="btn btn-info" style="background-color:#3c8dbc;float:left"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive" style="overflow-x:auto">
                            <table id="" class="table table-bordered table-striped dataTable" style="border:2px solid #3c8dbc;">
                                <thead>
                                    <tr>
                                        <th rowspan="3" colspan="2">Persons</th>
                                        <th colspan="10">Number of Persons</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Fatal</th>
                                        <th colspan="2">Grievous <br> (need hospitalisation)</th>
                                        <th colspan="2">Minor<br> (not needing hospitalisation)</th>
                                        <th colspan="2">No Injury</th>
                                        <th colspan="2">Total</th>
                                    </tr>
                                    <tr>
                                        <th>Male</th>
                                        <th>Female </th>
                                        <th>Male </th>
                                        <th>Female </th>
                                        <th>Male </th>
                                        <th>Female </th>
                                        <th>Male</th>
                                        <th>Female</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_posts17">
                                    <tr>
                                        <td colspan="2">1. Pedestrians</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">2. Bicycles</td>
                                        <td>a. Drivers</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers </td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">3. Two Wheelers</td>
                                        <td>a. Drivers</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers </td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">4. Auto Rickshaws</td>
                                        <td>a. Drivers</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers </td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">5. Cars, Taxis,<br> Vans &amp; LMV</td>
                                        <td>a. Drivers</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers </td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">6. Trucks/Lorries</td>
                                        <td>a. Drivers</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers </td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">7. Buses</td>
                                        <td>a. Drivers</td>
                                        <td>2</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>5</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers </td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">8. Other <br>Motor Vehicles</td>
                                        <td>a. Drivers</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers </td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">9. Others Persons</td>
                                        <td>a. Drivers</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers </td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">Total</td>
                                        <td>a. Drivers</td>
                                        <td>3</td>
                                        <td>0</td>
                                        <td>6</td>
                                        <td>0</td>
                                        <td>2</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>11</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>b. Passengers <br>Passengers+<br>Other Persons</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>2</td>
                                        <td>0</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-8">
                                <h3 class="widget-user-username" id="report_name"><b>Yearly Accident Report</b></h3>
                                <h5 class="widget-user-desc">Fatal, Grievous, Minor, Non: Year <span>2019</span></h5>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div style="width: 100%">
                            <canvas id="canvas"></canvas>
                        </div>
                        <button id="randomizeData">Randomize Data</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>-->
<!--Not Needed Actually. Only for demo data-->
<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" type="text/javascript"></script>

<script src="https://www.chartjs.org/samples/latest/utils.js" type="text/javascript"></script>

<script>
  $(function(){
    $('.small-box-footer').click(function(){
      $("#collapse").slideToggle("slow");
    });

    //Date range as a button
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
            '1 Day':[moment(),moment()],
            '1 Week':[moment().subtract('week',1),moment()],
            '1 Month':[moment().subtract('month',1),moment()],
            '6 Month':[moment().subtract('month',6),moment()],
            '1 Year':[moment().subtract('year',1),moment()],
            '5 Year':[moment().subtract('year',5),moment()]
        }
    }, cb);

    cb(start, end);
  });
  
  var barChartData = {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'Dataset 1',
				backgroundColor: window.chartColors.red,
				stack: 'Stack 0',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 2',
				backgroundColor: window.chartColors.blue,
				stack: 'Stack 0',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 3',
				backgroundColor: window.chartColors.green,
				stack: 'Stack 1',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};
		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Chart.js Bar Chart - Stacked'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			barChartData.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});
			});
			window.myBar.update();
		});
  
</script>