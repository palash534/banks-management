<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-8">
                                <h3 class="widget-user-username" id="report_name"><b>Accidents Classified According to Type of Victims, Age and Sex</b></h3>
                                <h5 class="widget-user-desc">List All Accidents Classified According to Type of Victims, Age and Sex</h5>
                            </div>
                            <div class="col-md-4" style="padding-right:0px">
                                <div class="row" style="margin-top:60px;">
                                    <div class="col-md-12">
                                        <div id="reportrange" style="width:65%;float:left;background: #fff; cursor: pointer; padding: 6px 10px; border: 1px solid #ccc;">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                        <button type="button" class="btn btn-info" style="background-color:#3c8dbc;float:left"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive" style="overflow-x:auto">
                            <table id="" class="table table-bordered table-striped dataTable" style="border:2px solid #3c8dbc;">
                                <thead>
                                    <tr>
                                        <th rowspan="3"></th>
                                        <th rowspan="3" colspan="2">Victims</th>
                                        <th colspan="10">Number of Accidents</th>
                                        <th colspan="4">Number of Persons</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Fatal </th>
                                        <th colspan="2">Greviously Injured<br>
                                            (need hospitalisation)
                                        </th>
                                        <th colspan="2">Minor Injured <br>
                                            (not needing hospitalisation)
                                        </th>
                                        <th colspan="2">No Injured</th>
                                        <th colspan="2">Total</th>
                                        <th colspan="2">Killed</th>
                                        <th colspan="2">Injured</th>
                                    </tr>
                                    <tr>
                                        <th>Male</th>
                                        <th>Female </th>
                                        <th>Male </th>
                                        <th>Female </th>
                                        <th>Male </th>
                                        <th>Female </th>
                                        <th>Male</th>
                                        <th>Female</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_posts12">
                                    <tr>
                                        <td></td>
                                        <th colspan="2">(A)Drivers</th>
                                        <td><b>3</b></td>
                                        <td><b>0</b></td>
                                        <td><b>6</b></td>
                                        <td><b>0</b></td>
                                        <td><b>2</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>11</b></td>
                                        <td><b>0</b></td>
                                        <td><b>3</b></td>
                                        <td><b>0</b></td>
                                        <td><b>8</b></td>
                                        <td><b>0</b></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">1.Less than 18 years</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>2</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">2. 18-25</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">3. 25-35</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">4. 35-45</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>2</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">5. 45-60</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>4</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>4</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>4</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">6. 60 and Above</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <th colspan="2">(B)Passengers</th>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">1.Less than 18 years</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">2. 18-25</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">3. 25-35</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">4. 35-45</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">5. 45-60</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">6. 60 and Above</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <th colspan="2">(C) Pedestrian</th>
                                        <td><b>1</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>1</b></td>
                                        <td><b>0</b></td>
                                        <td><b>1</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">1.Less than 18 years</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">2. 18-25</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">3. 25-35</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">4. 35-45</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">5. 45-60</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">6. 60 and Above</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <th colspan="2">(D) Cyclist</th>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                        <td><b>0</b></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">1.Less than 18 years</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">2. 18-25</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">3. 25-35</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">4. 35-45</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">5. 45-60</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">6. 60 and Above</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-8">
                                <h3 class="widget-user-username" id="report_name"><b>Yearly Accident Report</b></h3>
                                <h5 class="widget-user-desc">Fatal, Grievous, Minor, Non: Year <span>2019</span></h5>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div style="width: 100%">
                            <canvas id="canvas"></canvas>
                        </div>
                        <button id="randomizeData">Randomize Data</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>-->
<!--Not Needed Actually. Only for demo data-->
<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" type="text/javascript"></script>

<script src="https://www.chartjs.org/samples/latest/utils.js" type="text/javascript"></script>

<script>
  $(function(){
    $('.small-box-footer').click(function(){
      $("#collapse").slideToggle("slow");
    });

    //Date range as a button
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
            '1 Day':[moment(),moment()],
            '1 Week':[moment().subtract('week',1),moment()],
            '1 Month':[moment().subtract('month',1),moment()],
            '6 Month':[moment().subtract('month',6),moment()],
            '1 Year':[moment().subtract('year',1),moment()],
            '5 Year':[moment().subtract('year',5),moment()]
        }
    }, cb);

    cb(start, end);
  });
  
  var barChartData = {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'Dataset 1',
				backgroundColor: window.chartColors.red,
				stack: 'Stack 0',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 2',
				backgroundColor: window.chartColors.blue,
				stack: 'Stack 0',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 3',
				backgroundColor: window.chartColors.green,
				stack: 'Stack 1',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};
		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Chart.js Bar Chart - Stacked'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			barChartData.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});
			});
			window.myBar.update();
		});
  
</script>