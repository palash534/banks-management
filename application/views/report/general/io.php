<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row" >
                            <div class="col-md-10">
                                <h3 class="widget-user-username" id="report_name"><b>Investigation Officer Wise Accident Details</b></h3>
                                <h5 class="widget-user-desc">List All Accidents under Investigation Officer</h5>
                            </div>
                            <div class="col-md-2" style="text-align:right">
                                <button type="button" class="btn btn-info" style="background-color:#3c8dbc;text-align:right"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label></label>
                                <select id="select_io" class="form-control">
                                    <option value="0">Select Investigation Officer</option>
                                </select>
                            </div>
                            <div class="col-md-6"></div>
                            <div class="col-md-3">
                                <div class="row" >
                                    <div class="col-md-12" >
                                        <br>
                                        <div id="reportrange" class="form-control" >
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                    </div>    
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive" style="overflow-x:auto">
                            <table id="io" class="table table-bordered table-striped dataTable display responsive nowrap" width="100%" style="border:2px solid #3c8dbc;overflow-y: scroll; height:400px;">
                                <thead class="bg-light-blue-active">
                                    <tr>
                                        <th>Accident Description</th>
                                        <th>Incharge</th>
                                        <th></th>
                                        <th>Accident Severity</th>
                                        <th>Accident Image</th>
                                        <th>Accident Date<br> & Time</th>
                                        <th>Station</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot class="bg-info">
                                    <tr>
                                        <th>Accident Description</th>
                                        <th>Incharge</th>
                                        <th></th>
                                        <th>Accident Severity</th>
                                        <th>Accident Image</th>
                                        <th>Accident Date<br> & Time</th>
                                        <th>Station</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>-->
<!--Not Needed Actually. Only for demo data-->
<!--Datatable-->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>



<script>
  $(function(){
    //Date range as a button
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
            '1 Day':[moment(),moment()],
            '1 Week':[moment().subtract('week',1),moment()],
            '1 Month':[moment().subtract('month',1),moment()],
            '6 Month':[moment().subtract('month',6),moment()],
            '1 Year':[moment().subtract('year',1),moment()],
            '5 Year':[moment().subtract('year',5),moment()]
        }
    }, cb);

    cb(start, end);

    //Datatable
    $('#io').dataTable();

    //Select2
    //$('#select_guard').select2();
  });
</script>