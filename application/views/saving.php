      <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<div class="services-single">
						<div class="inner-box">
							<h3><u>SAVING ACCOUNT</u></h3>
                            <div class="text">
                                <div class="two-column row clearfix">
                                	<div class="column col-md-8 col-sm-8 col-xs-12">
                                    	<b>Inetrest rate 3.5%.*</b>
                                        <p>A savings account is a basic type of  account that allows you to deposit money, keep it safe, and withdraw funds, all while earning interest. Savings Account encourages depositors to save money and easy to operate.
                                            <ul>
                                             <li>• The applicant should be a member.</li>
                                            <li>• The person desiring to open saving account shall furnish passport size photograph, Address proof, ID proof and Permanent Account Number along with the application. .</li>
                                             <li>• Saving account can be opened by individuals, self help groups, and other institution. .</li>

                                              <li>• Specific rate of interest will be paid to the depositors for their deposits. .</li>
    
                                               <li>• Savings Accounts can be opened with a minimum balance of Rs.100/- and the same to be maintained to keep the account running.</li>
  
                                                 <li>• Cheque Book facility available for inter branch transactions only.</li>
  
                                                  <li>• Passbook will be issued free of cost. For duplicate passbook service charges levied. .</li>
  
                                                 <li>• NEFT / RTGS facilities available. .</li>
  
                                                  <li>• SMS service available for the account holders. .</li>

                                             </ul> 
                                            </p>
                                    </div>
                                   <div class="column col-md-4 col-sm-4 col-xs-12">
                                        <div class="service-image">
                                            <img src="<?php echo base_url(); ?>assets/images/clients/saving.jpg" alt="" />
                                        </div>
                                    </div>

                                </div>
                               
                            </div>
                        
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
 
     <section class="call-to-action">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h3>If you have any query for related business... Just touch on this button
Contact Us</h3>
                <a href="<?php echo base_url();?>home/contact" class="contact-btn">Contact Us</a>
            </div>
        </div>
    </section>
    <!-- End Call To Action Section -->