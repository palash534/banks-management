 <!--Page Title-
    <section class="page-title" style="background-image:url(images/background/11.jpg)">
    	<div class="auto-container">
        	<h1>Services</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index.html">Home</a></li>
                <li>Services</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
       <!--Services Section-->
    <section class="services-section">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title">
                <div class="title">OUR SERVICE </div>
                <h2>Save your money</h2>
            </div>
            <div class="row clearfix">
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/saving.jpg" alt="SAVINGS ACCOUNT" />
                            <div class="icon-box">
                                <span class="icon flaticon-employee"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-employee"></span>
                                             <h3><a href="<?php echo base_url();?>home/saving">SAVINGS ACCOUNT </a></h3>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/saving">SAVINGS ACCOUNT </a></h3>
                            <a href="<?php echo base_url();?>home/saving" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/currentaccount.jpg" alt="CURRENT ACCOUNT" />
                            <div class="icon-box">
                                <span class="icon flaticon-money"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-money"></span>
                                             <h3><a href="<?php echo base_url();?>home/currentAc">CURRENT ACCOUNT</a></h3>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/currentAc">CURRENT ACCOUNT</a></h3>
                            <a href="<?php echo base_url();?>home/currentAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/fixed.jpg" alt="FIXED DEPOSITS" />
                            <div class="icon-box">
                                <span class="icon flaticon-diagram"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-diagram"></span>
                                            <h3><a href="<?php echo base_url();?>home/fixedAc">FIXED DEPOSIT</a></h3>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/fixedAc">FIXED DEPOSIT</a></h3>
                            <a href="<?php echo base_url();?>home/fixedAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/recurring.jpg" alt="RECCURRING DEPOSITS" />
                            <div class="icon-box">
                                <span class="icon flaticon-pen"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-pen"> </span>
                                            <h3><a href="<?php echo base_url();?>home/reccuringAc">RECCURING DEPOSIT</a></h3>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/reccuringAc">RECCURING DEPOSIT</a></h3>
                            <a href="<?php echo base_url();?>home/reccuringAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/compolsary.jpg" alt="COMPULSARY DEPOSIT" />
                            <div class="icon-box">
                                <span class="icon flaticon-meeting"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-meeting"></span>
                                             <h3><a href="<?php echo base_url();?>home/compulsaryAc">COMPULSARY DEPOSIT</a></h3>

                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/compulsaryAc">COMPULSARY DEPOSIT</a></h3>
                            <a href="<?php echo base_url();?>home/compulsaryAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
           <!--Services Block Two-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/pottery-service/dailydeposite.jpg" alt="daily deposite " />
                            <div class="icon-box">
                                <span class="icon flaticon-calendar"></span>
                            </div>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <div class="overlay-icon">
                                            <span class="icon flaticon-calendar"></span>
                                             <h3><a href="<?php echo base_url();?>home/dailyAc">DAILY DEPOSITES</a></h3>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url();?>home/dailyAc">DAILY DEPOSITES</a></h3>
                            <a href="<?php echo base_url();?>home/dailyAc" class="arrow-box"><span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Services Section-->
               <div class="skills">
                            <div class="skill-item">
                               
                                <div class="skill-bar">
                                    <div class="bar-inner"><div class="bar progress-line" data-width="100"></div></div>
                                </div>
                            </div>
                </div> 
     <!--Report Section-->
    <section class="report-section">
        <div class="auto-container">

            <div class="row clearfix">
                 
                <!--Content Column-->
                <div class="content-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h2>E STAMP FACILITY </h2>
                            <div class="bold-text">
                                This setvice is available only at head office puttur and B.C road branch only.   
                             </div>
                        </div>
                        <div class="sec-title">
                             <h2>KUMBHA LAKSHMI CASH CERTIFICATE</h2>
                                <div class="bold-text">
                                    Get double of your investment in 90 months
                               </div> 
                          </div>
                          <div class="sec-title">
                             <h2>SPECIAL 0.5% </h2>
                                <div class="bold-text">
                               For senior citizens, widows and institutions special 0.5% extra rate of interest is available for the deposits over the period of 180 days.
                               </div> 
                          </div>
                    </div>
                         
                   
                </div>
                <!--Graph Column-->
                <div class="graph-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                         <div class="sec-title">
                             <h2>UNITED INDIA INSURANCE CO LTD </h2>
                                <div class="bold-text">
                              All kinds of vehicles insurance, insurance for house, shop and personal accidental insurance is available at a discounted rate.
                               </div> 
                          </div>
                        <div class="image">
                            <img src="<?php echo base_url(); ?>assets/images/clients/insurence.jpg" alt="insurence" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Report Section-->
    <!--Approach Section-->
    <section class="approach-section" style="background-image:url(<?php echo base_url(); ?>assetsimages/background/2.jpg)">
        <div class="upper-box" style="background-image:url(<?php echo base_url(); ?>assets/images/background/1.jpg)">
            <div class="auto-container">
                <div class="title">  </div>
                <h2>Loans   </h2>
                <div class="quality"> We help you reach your dream</div>
            </div>
        </div>
        
        <div class="auto-container">
            <div class="blocks-section">
                <div class="row clearfix">
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-file"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/1">SURITY LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-file"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/1">SURITY LOAN</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-grain"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/2">JEWEL LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-grain"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/2">JEWEL LOAN</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-piggy-bank"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/3">DEPOSIT LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-piggy-bank"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/3">DEPOSIT LOAN </a></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon flaticon-meeting"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/4">CASH CREDIT LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon flaticon-meeting"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/4">CASH CREDIT LOAN</a></h4>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-home"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/5">HOUSING LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-home"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/5">HOUSING LOAN</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                      <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon  glyphicon glyphicon-globe"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/6">LAND PURCHASE LOAN<BR>LAND MORTGAGE LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon  glyphicon glyphicon-globe"></span>
                                    </div>
                                    <h4>
                                        <a href="<?php echo base_url();?>home/loan/6">LAND LOAN</a></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-bed"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/7">VEHICLE LOAN</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-bed"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/7">VEHICLE LOAN</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                       <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-education"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/8">MEMBERS CHILDREN EDUCATION LOANS  </a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-education"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/8">8.    MEMBERS CHILDREN EDUCATION LOANS</a></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                 
                       <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon glyphicon glyphicon-registration-mark"></span>
                            </div>
                            <h3><a href="<?php echo base_url();?>home/loan/9">SELF HELP GROUP LOANS</a></h3>
                            <div class="overlay-box" >
                                <div class="overlay-inner">
                                    <div class="icon-box">
                                        <span class="icon glyphicon glyphicon-registration-mark"></span>
                                    </div>
                                    <h4><a href="<?php echo base_url();?>home/loan/9">SELF HELP GROUP LOANS</a></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        