 <!--Page Title-
    <section class="page-title" style="background-image:url(<?php echo base_url(); ?>assets/images/background/11.jpg)">
    	<div class="auto-container">
        	<h1>Our Products</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index.html">Home</a></li>
                <li>Shop</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="our-shop">
                        
                        <div class="row clearfix">
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets/images/resource/products/1.jpg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="list-style-one">
                                                <!-- <li><a href="shop-detail.html"><span class="fa fa-shopping-cart"></span></a></li>
                                                --> <li><a href="<?php echo base_url(); ?>assets/images/resource/products/1.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3>Deepa</h3>
                                        <div class="price">₹120.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets/images/resource/products/2.jpg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="cart-option">
                                                <!-- <li><a href="shop-detail.html"><span class="fa fa-shopping-cart"></span></a></li>
                                                 --><li><a href="i<?php echo base_url(); ?>assets/images/resource/products/2.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="shop-detail.html">Lord Ganesha</a></h3>
                                        <div class="price">₹250.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets/images/resource/products/3.jpg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="cart-option">
                                               <!--  <li><a href="shop-detail.html"><span class="fa fa-shopping-cart"></span></a></li>
                                               -->  <li><a href="<?php echo base_url(); ?>assets/images/resource/products/3.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3>Whistle Bird</h3>
                                        <div class="price">₹50.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets/images/resource/products/4.jpg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="cart-option">
                                               <!--  <li><a href="shop-detail.html"><span class="fa fa-shopping-cart"></span></a></li>
                                                --> <li><a href="<?php echo base_url(); ?>assets/images/resource/products/4.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="shop-detail.html">Kitchen Toy</a></h3>
                                        <div class="price">₹250.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets/images/resource/products/5.jpg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="cart-option">
                                                 <li><a href="<?php echo base_url(); ?>assets/images/resource/products/5.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="shop-detail.html">Gante</a></h3>
                                        <div class="price">₹100.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets/images/resource/products/6.jpg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="cart-option">
                                                <li><a href="<?php echo base_url(); ?>assets/images/resource/products/6.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="shop-detail.html">Fire Pot</a></h3>
                                        <div class="price">₹150.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets/images/resource/products/10.jpg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="cart-option">
                                                 <li><a href="<?php echo base_url(); ?>assets/images/resource/products/10.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3>Crokery</h3>
                                        <div class="price">₹120.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url();?>assets/images/resource/products/11.jpeg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="cart-option">
                                                 <li><a href="<?php echo base_url(); ?>assets/images/resource/products/11.jpeg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3>Crokery</h3>
                                        <div class="price">₹150.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                            <!--Shop Item-->
                            <div class="shop-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets/images/resource/products/9.jpeg" alt="" />
                                        <div class="overlay-box">
                                            <ul class="cart-option">
                                                 <li><a href="<?php echo base_url(); ?>assets/images/resource/products/9.jpeg" data-fancybox="images" data-caption="" class="link"><span class="icon fa fa-search"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3>Crokery</h3>
                                        <div class="price">₹120.00</div>                                    
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                      
                        
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar no-padd">
                        
                        <!-- Search -->
                        <!-- <div class="sidebar-widget search-box style-two">
                            <form method="post" action="contact.html">
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search Product" required>
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
                        </div> -->
                        
                        <!-- category -->
                        <div class="sidebar-widget category-widget">
                            <div class="sidebar-title">
                                <h1>For Orders<b> Contact</b></h1><ul>
                             <li><span class="icon fa fa-phone"></span>  08251-236274 / 08251-233274 </li></ul>
                        </div>
                       
                    </aside>
                </div>
                
            </div>
            
        </div>
    </div>
    <!--End Sidebar Page Container-->
    