<!-- Modal -->
<div id="vehicle_search" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #357ca5 !important">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:white">Find Vehicle Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                  <label for="license_plate_no">License Plate No</label>
                  <input type="email" class="form-control" id="license_plate_no" placeholder="Enter license plate no">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                  <label for="engine_no">Engine No</label>
                  <input type="email" class="form-control" id="engine_no" placeholder="Enter engine plate no">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                  <label for="chasis_no">Chasis No</label>
                  <input type="email" class="form-control" id="chasis_no" placeholder="Enter chasis no">
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-primary search_vehicle" style="margin-top:25px">Search Vehicle</button>
            </div>
        </div>
        <div class="row vehicle_search_details" style="background: aliceblue;display:none">
            <div class="row col-md-12 ">
                <label class="control-label pad col-md-1">Owner Name </label>
                <div class="col-md-3 pad">
                    <div name="Owner_Name" placeholder=" " id="Owner_Name" class="form-control"> </div>
                </div>
                <label class="control-label pad col-md-1">Guardians Name </label>
                <div class="col-md-3 pad">
                    <div name="Fathers_Name" placeholder=" " id="Fathers_Name" class="form-control"> </div>
                </div>
                <label class="control-label pad col-md-1">Mobile No </label>
                <div class="col-md-3 pad">
                    <div name="Mobile_No" placeholder=" " id="Mobile_No" class="form-control"> </div>
                </div>
            </div>
            <div class="row col-md-12 ">
                <label class="control-label pad col-md-1">Chasis NO </label>
                <div class="col-md-3 pad">
                    <div name="Chasis_NO" placeholder="   " id="Chasis_NO" class="form-control"> </div>
                </div>
                <label class="control-label pad col-md-1">Engine No </label>
                <div class="col-md-3 pad">
                    <div name="Engine_No" placeholder=" " id="Engine_No" class="form-control"> </div>
                </div>
                <label class="control-label pad col-md-1">Vehicle Model </label>
                <div class="col-md-3 pad">
                    <div name="Vehicle_Model" placeholder=" " id="Vehicle_Model" class="form-control"> </div>
                </div>
            </div>
            <div class="row col-md-12 ">
                <label class="control-label pad col-md-1">Details </label>
                <div class="col-md-3 pad">
                    <div name="Description" placeholder=" " id="Description" class="form-control"> </div>
                </div>
                <label class="control-label pad col-md-1">Vehicle Colour </label>
                <div class="col-md-3 pad">
                    <div name="Vehicle_Colour" placeholder="  " id="Vehicle_Colour" class="form-control"> </div>
                </div>
                <label class="control-label pad col-md-2">Registration Date </label>
                <div class="col-md-2 pad">
                    <div name="Registration_Date" placeholder=" " id="Registration_Date" class="form-control"> </div>
                </div>
            </div>
            <div class="row col-md-12 ">
                <label class="control-label pad col-md-2">Address 1 </label>
                <div class="col-md-4 pad">
                    <div name="Address1" placeholder=" " id="Address1" class="form-control"> </div>
                </div>
                <label class="control-label pad col-md-2">Address 2 </label>
                <div class="col-md-4 pad">
                    <div name="Address2" placeholder=" " id="Address2" class="form-control"> </div>
                </div>
            </div>
            <div class="row col-md-12 ">
                <label class="control-label pad col-md-2">Address 3 </label>
                <div class="col-md-4 pad">
                    <div name="Address3" placeholder=" " id="Address3" class="form-control"> </div>
                </div>
                <label class="control-label pad col-md-2">Pincode </label>
                <div class="col-md-4 pad">
                    <div name="Pincode" placeholder=" " id="Pincode" class="form-control"> </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
    $(function(){
        $('.search_vehicle').click(function(){
            $('.vehicle_search_details').slideDown();
        });
    });
</script>